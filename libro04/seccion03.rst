##################################
SECCION TERCERA Sucesión Intestada
##################################

TITULO  I Disposiciones Generales
#################################

Artículo 815.- Casos de sucesión intestada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La herencia corresponde a los herederos legales en los casos siguientes, cuando:

1. El causante muere sin dejar testamento, o el que otorgó fue declarado nulo.

2. El testamento no contiene institución de herederos, o se ha declarado la caducidad de la que contenía.

3. El heredero forzoso muere antes que el testador, renuncia a la herencia o la pierde por indignidad o desheredación y no tiene descendientes.

4. El heredero voluntario o el legatario muere antes que el testador; o por no haberse cumplido la condición puesta por éste; o por renuncia o por haberse declarado indignos a estos sucesores, sin designarse sustitutos.

5. El testador que no tiene herederos forzosos o voluntarios instituidos en testamento, no ha dispuesto de todos sus bienes en legados, en cuyo caso la sucesión legal sólo funciona con respecto a los bienes de que no dispuso.(*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Casos de sucesión intestada

"Artículo 815.-   La herencia corresponde a los herederos legales cuando:

1.- El causante muere sin dejar testamento; el que otorgó ha sido declarado nulo total o parcialmente; ha caducado por falta de comprobación judicial; o se declara inválida la desheredación.

2.- El testamento no contiene institución de heredero, o se ha declarado la caducidad o invalidez de la disposición que lo instituye.

3.- El heredero forzoso muere antes que el testador, renuncia a la herencia o la pierde por indignidad o desheredación y no tiene descendientes.

4.- El heredero voluntario o el legatario muere antes que el testador; o por no haberse cumplido la condición establecida por éste; o por renuncia, o por haberse declarado indignos a estos sucesores sin sustitutos designados.

5.- El testador que no tiene herederos forzosos o voluntarios instituidos en testamento, no ha dispuesto de todos sus bienes en legados, en cuyo caso la sucesión legal sólo funciona con respecto a los bienes de que no dispuso.

La declaración judicial de herederos por sucesión total o parcialmente intestada, no impide al preterido por la declaración haga valer los derechos que le confiere el Artículo 664."

PROCESOS CONSTITUCIONALES

Artículo 816.- Orden sucesorio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son herederos del primer orden, los hijos y demás descendientes; del segundo orden, los padres y demás ascendientes; del tercer orden, el cónyuge; del cuarto, quinto y sexto órdenes, respectivamente, los parientes colaterales del segundo, tercero y cuarto grados de consanguinidad.

El cónyuge también es heredero en concurrencia con los herederos de los dos primeros órdenes indicados en este artículo.(*)

(*) Artículo modificado por el Artículo 6 de la Ley N° 30007, publicada el 17 abril 2013, cuyo texto es el siguiente:

“Artículo 816.- Órdenes sucesorios

Son herederos del primer orden, los hijos y demás descendientes; del segundo orden, los padres y demás ascendientes; del tercer orden, el cónyuge o, en su caso, el integrante sobreviviente de la unión de hecho; del cuarto, quinto y sexto órdenes, respectivamente, los parientes colaterales del segundo, tercer y cuarto grado de consanguinidad.

El cónyuge o, en su caso, el integrante sobreviviente de la unión de hecho también es heredero en concurrencia con los herederos de los dos primeros órdenes indicados en este artículo.”

Artículo 817.- Exclusión sucesoria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los parientes de la línea recta descendente excluyen a los de la ascendente. Los parientes más próximos en grado excluyen a los más remotos, salvo el derecho de representación.

TITULO  II Sucesión de los Descendientes
########################################

Artículo 818.- Igualdad de derechos sucesorios de los hijos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Todos los hijos tienen iguales derechos sucesorios respecto de sus padres. Esta disposición comprende a los hijos matrimoniales, a los extramatrimoniales reconocidos voluntariamente o declarados por sentencia, respecto a la herencia del padre o de la madre y los parientes de éstos, y a los hijos adoptivos.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 819.- Sucesión por cabeza y por estirpe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La misma igualdad de derechos rige la sucesión de los demás descendientes. Estos heredan a sus ascendientes por cabeza, si concurren solos, y por estirpe, cuando concurren con hijos del causante.

TITULO  III Sucesión de los Ascendientes
########################################

(*) De conformidad con la Primera Disposición Complementaria Final del Decreto Supremo N° 009-2019-IN, publicado el 24 abril 2019, para el otorgamiento del monto dispuesto en el artículo 2 de la Ley Nº 30684 a los ascendientes de los bomberos que hayan sido declarados héroes del Cuerpo General de Bomberos Voluntarios del Perú, se aplica lo dispuesto en los artículos 820 y 821 del presente Código según cada caso.

Artículo 820.- Sucesión de los padres
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A falta de hijos y otros descendientes heredan los padres por partes iguales. Si existiera sólo uno de ellos, a éste le corresponde la herencia.

Artículo 821.- Sucesión de los abuelos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si no hubiere padres, heredan los abuelos, en forma que la indicada en el artículo 820.

TITULO  IV Sucesión del Cónyuge
###############################

Artículo 822.- Concurrencia del cónyuge con descendientes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cónyuge que concurre con hijos o con otros descendientes del causante, hereda una parte igual a la de un hijo.

Artículo 823.- Opción usufructuaria del cónyuge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos del artículo 822 el cónyuge puede optar por el usufructo de la tercera parte de la herencia, salvo que hubiere obtenido los derechos que le conceden los artículos 731 y 732.

Artículo 824.- Concurrencia del cónyuge con ascendientes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cónyuge que concurra con los padres o con otros ascendientes del causante, hereda una parte igual a la de uno de ellos.

Artículo 825.- Sucesión exclusiva del cónyuge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el causante no ha dejado descendientes ni ascendientes con derecho a heredar, la herencia corresponde al cónyuge sobreviviente.

Artículo 826.- Improcedencia de la sucesión del cónyuge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La sucesión que corresponde al viudo o a la viuda no procede, cuando hallándose enfermo uno de los cónyuges al celebrarse el matrimonio, muriese de esa enfermedad dentro de los treinta días siguientes, salvo que el matrimonio hubiera sido celebrado para regularizar una situación de hecho.

Artículo 827.- Derecho sucesorio del cónyuge de buena fe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La nulidad del matrimonio por haber sido celebrado con  persona que estaba impedida de contraerlo no afecta los derechos sucesorios del cónyuge que lo contrajo de buena fe, salvo que el primer cónyuge sobreviva al causante.

TITULO  V Sucesión de los Parientes Colaterales
###############################################

Artículo 828.- Sucesión de parientes colaterales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si no hay descendientes, ni ascendientes, ni cónyuge con derecho a heredar, la herencia corresponde a los parientes colaterales hasta el cuarto grado de consanguinidad inclusive, excluyendo los más próximos a los más remotos, salvo el derecho de los sobrinos para concurrir con sus tíos en representación de sus padres, de conformidad con el artículo 683.

Artículo 829.- Concurrencia de medios hermanos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos de concurrencia de hermanos de padre y madre con medio hermanos, aquéllos recibirán doble porción que éstos.

TITULO  VI Sucesión del Estado y de las Beneficencias Públicas
##############################################################

Artículo 830.- Sucesión del Estado y de la Beneficencia Pública
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A falta de sucesores testamentarios o legales, el juez que conoce del procedimiento de declaratoria de herederos, adjudicará los predios rústicos, ganado, maquinaria e instalaciones que los integren al correspondiente organismo del Estado, y los demás bienes a la Beneficencia Pública del lugar del último domicilio que tuvo el causante en el país, o de la capital de la República si estuvo domiciliado en el extranjero o al organismo que haga sus veces. Es obligación de las entidades adjudicatarias pagar las deudas del causante hasta donde alcance el valor de los bienes adjudicados.

Corresponde al gestor de la declaratoria respectiva, el cuarenta por ciento de su valor neto.(*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26680, publicada el 08 noviembre 1996, cuyo texto es el siguiente:

Sucesión del Estado y de la Beneficencia Pública

"Artículo 830.- A falta de sucesores testamentarios o legales el juez o notario que conoce del proceso o trámite de sucesión intestada, adjudicará los bienes que  integran la masa hereditaria, a la Sociedad de Beneficencia o a falta de ésta, a la Junta de  Participación Social del lugar del último domicilio del causante en el país o a la Sociedad de Beneficencia de Lima Metropolitana si estuvo domiciliado en el extranjero.

Es obligación de la entidad adjudicataria pagar las deudas del causante si las hubiera, hasta donde alcance el valor de los bienes adjudicados.

Corresponde al gestor del proceso o trámite de sucesión intestada, el diez por ciento del valor neto de los bienes adjudicados, el mismo que será abonado por la entidad respectiva, con el producto de la venta de dichos bienes u otros, mediante la adjudicación de alguno de ellos."
