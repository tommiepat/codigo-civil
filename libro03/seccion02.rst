=================================
SECCION SEGUNDA Sociedad Conyugal
=================================

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

TITULO I El Matrimonio como Acto
================================

CAPITULO PRIMERO Esponsales
###########################

Artículo 239 Promesa recíproca de matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La promesa recíproca de matrimonio no genera obligación legal de contraerlo, ni de ajustarse a lo estipulado para el caso de incumplimiento de la misma.

Artículo 240 Efectos de la ruptura de promesa matrimonial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la promesa de matrimonio se formaliza indubitablemente entre personas legalmente aptas para casarse y se deja de cumplir por culpa exclusiva de uno de los promitentes, ocasionando con ello daños y perjuicios al otro o a terceros, aquél estará obligado a indemnizarlos.

La acción debe de interponerse dentro del plazo de un año a partir de la ruptura de la promesa.

Dentro del mismo plazo, cada uno de los prometidos puede revocar las donaciones que haya hecho en favor del otro por razón del matrimonio proyectado. Cuando no sea posible la restitución, se observa lo prescrito en el artículo 1635.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

CAPITULO SEGUNDO Impedimentos
#############################

Artículo 241 Impedimentos Absolutos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No pueden contraer matrimonio:

1. Los impúberes. El juez puede dispensar este impedimento por motivos graves, siempre que el varón tenga dieciséis años cumplidos y la mujer catorce.(*)

(*) Inciso modificado por el Artículo 1 de la Ley Nº 27201, publicada el 14 noviembre 1999, cuyo texto es el siguiente:

"1. Los adolescentes. El juez puede dispensar este impedimento por motivos justificados, siempre que los contrayentes tengan, como mínimo, dieciséis años cumplidos y manifiesten expresamente su voluntad de casarse."

2.- Los que adolecieren de enfermedad crónica, contagiosa y trasmisible por herencia, o de vicio que constituya peligro para la prole.(*)

(*) Inciso modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

"2.- Las personas con capacidad de ejercicio restringida contempladas en el artículo 44 numeral 9, en tanto no exista manifestación de la voluntad expresa o tácita sobre esta materia."

3.- Los que padecieren crónicamente de enfermedad mental, aunque tengan intervalos lúcidos.(*)

(*) Numeral derogado por el Literal a) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

4.- Los sordomudos, los ciegosordos y los ciegomudos que no supieren expresar su voluntad de manera indubitable. (*)

(*) Numeral derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 29973, publicada el 24 diciembre 2012.

5.- Los casados.

Artículo 242 Impedimentos relativos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No pueden contraer matrimonio entre sí:

1.- Los consanguíneos en línea recta. El fallo que condena al pago de alimentos en favor del hijo extramatrimonial no reconocido ni declarado judicialmente produce también el impedimento a que se refiere este inciso.

2.- Los consanguíneos en línea colateral dentro del segundo y el tercer grados. Tratándose del tercer grado el juez puede dispensar este impedimento cuando existan motivos graves.

3.- Los afines en línea recta.

4.- Los afines en el segundo grado de la línea colateral cuando el matrimonio que produjo la afinidad se disolvió por divorcio y el ex-cónyuge vive.

5.- El adoptante, el adoptado y sus familiares en las líneas y dentro de los grados señalados en los incisos 1 a 4 para la consanguinidad y la afinidad.

6.- El condenado como partícipe en el homicidio doloso de uno de los cónyuges, ni el procesado por esta causa con el sobreviviente.

7.- El raptor con la raptada o a la inversa, mientras subsista el rapto o haya retención violenta.

Artículo 243 Prohibiciones especiales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No se permite el matrimonio:

1. Del tutor o del curador con el menor o el incapaz, durante el ejercicio del cargo, ni antes de que estén judicialmente aprobadas las cuentas de la administración, salvo que el padre o la madre de la persona sujeta a la tutela o curatela hubiese autorizado el matrimonio por testamento o escritura pública.

El tutor o el curador que infrinja la prohibición pierde la retribución a que tenga derecho, sin perjuicio de la responsabilidad derivada del desempeño del cargo.  (*)

(*) Numeral modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

"1.- Del tutor o del curador con el menor o con la persona con capacidad de ejercicio restringida del artículo 44 numerales 4 al 7 durante el ejercicio del cargo, ni antes de que estén judicialmente aprobadas las cuentas de la administración, salvo que el padre o la madre de la persona sujeta a la tutela hubiese autorizado el matrimonio por testamento o escritura pública.

El tutor que infrinja la prohibición pierde la retribución a que tenga derecho, sin perjuicio de la responsabilidad derivada del desempeño del cargo."

2. Del viudo o de la viuda que no acredite haber hecho inventario judicial, con intervención del Ministerio Público, de los bienes que esté administrando pertenecientes a sus hijos o sin que preceda declaración jurada de que no tiene hijos bajo su patria potestad o de que éstos no tienen bienes.

La infracción de esta norma acarrea la pérdida del usufructo legal sobre los bienes de dichos hijos.

Esta disposición es aplicable al cónyuge cuyo matrimonio hubiese sido invalidado o disuelto por divorcio, así como al padre o a la madre que tenga hijos extramatrimoniales bajo su patria potestad.

3. De la viuda en tanto no trascurran por lo menos trescientos días de la muerte de su marido, salvo que diere a luz. Esta disposición es aplicable a la mujer divorciada o cuyo matrimonio hubiera sido invalidado. (*) RECTIFICADO POR FE DE ERRATAS

La viuda que contravenga la prohibición contenida en este inciso pierde los bienes que hubiese recibido del marido a título gratuito.

El juez puede conceder dispensa del plazo de espera cuando, atendidas las circunstancias, sea imposible que la mujer se halle embarazada por obra del marido.

No rige la prohibición, para el caso del artículo 333, inciso 5.

Es de aplicación a los casos a que se refiere este inciso la presunción de paternidad respecto del nuevo marido. (*)

(*) Inciso modificado por el Artículo 1 de la Ley Nº 27118, publicada el 23 mayo 1999, cuyo texto es el siguiente:

"3. De la viuda, en tanto no transcurran por lo menos trescientos días de la muerte de su marido, salvo que diere a luz. Esta disposición es aplicable a la mujer divorciada o cuyo matrimonio hubiera sido invalidado.

Se dispensa el plazo si la mujer acredita no hallarse embarazada, mediante certificado médico expedido por autoridad competente.

La viuda que contravenga la prohibición contenida en este inciso pierde los bienes que hubiera recibido de su marido a título gratuito.

No rige la prohibición para el caso del Artículo 333 inciso 5.

Es de aplicación a los casos a que se refiere este inciso la presunción de paternidad respecto del nuevo marido."

Artículo 244 Requisitos para matrimonio entre menores de edad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los menores de edad, para contraer matrimonio, necesitan del asentimiento expreso de sus padres. La discrepancia entre los padres equivale al asentimiento.

A falta o por incapacidad absoluta o por destitución de uno de los padres del ejercicio de la patria potestad, basta el asentimiento del otro.

A falta de ambos padres, o si los dos fueran absolutamente incapaces o hubieran sido destituidos del ejercicio de la patria potestad, prestarán asentimiento los abuelos y las abuelas. En igualdad de votos contrarios, la discordancia equivale al asentimiento.

A falta de abuelos y abuelas o si son absolutamente incapaces o han sido removidos de la tutela, corresponde al juez de menores otorgar o negar la licencia supletoria. La misma atribución corresponde al juez de menores, respecto de expósitos o de menores abandonados o que se encuentren bajo jurisdicción especial.

Los hijos extramatrimoniales sólo requieren el asentimiento del padre o, en su caso, de los abuelos paternos, cuando aquél los hubiese reconocido voluntariamente. La misma regla se aplica a la madre y los abuelos en línea materna.

Artículo 245 Negativa de los padres
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La negativa de los padres o ascendientes a otorgar el asentimiento no requiere fundamentación. Contra esta negativa no hay recurso alguno.

Artículo 246 Resolución judicial denegatoria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La resolución judicial denegatoria a que se refiere el artículo 244 debe ser fundamentada y contra ella procede el recurso de apelación en ambos efectos.

Artículo 247 Efectos del matrimonio de menores sin autorización
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El menor que se casa sin el asentimiento a que se refieren los artículos 244 y 245 no goza de la posesión, administración, usufructo ni de la facultad de gravamen o disposición de sus bienes, hasta que alcance la mayoría.

El funcionario del registro del estado civil ante quien se celebró el casamiento sufrirá una multa no menor a diez sueldos mínimos vitales mensuales del lugar que corresponda, sin perjuicio de la responsabilidad penal a que haya lugar.

CAPITULO TERCERO Celebración del Matrimonio
###########################################

Artículo 248 Diligencias para matrimonio civil
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quienes pretendan contraer matrimonio civil lo declararán oralmente o por escrito al alcalde provincial o distrital del domicilio de cualquiera de ellos.

Acompañarán copia certificada de las partidas de nacimiento, la prueba del domicilio y el certificado médico, expedido en fecha no anterior a treinta días, que acredite que no están incursos en el impedimento establecido en el artículo 241, inciso 2, o, si en el lugar no hubiere servicio médico oficial y gratuito, la declaración jurada de no tener tal impedimento.

Acompañarán, también en sus respectivos casos, la dispensa judicial de la impubertad, el instrumento en que conste el asentimiento de los padres o ascendientes o la licencia judicial supletoria, la dispensa del parentesco de consanguinidad colateral en tercer grado, copia certificada de la partida de defunción del cónyuge anterior o la sentencia de divorcio o de invalidación del matrimonio anterior, el certificado consular de soltería o viudez, y todos los demás documentos que fueren necesarios según las circunstancias.

Cada pretendiente presentará, además, a dos testigos mayores de edad que lo conozcan por lo menos desde tres años antes, quienes depondrán, bajo juramento, acerca de si existe o no algún impedimento. Los mismos testigos pueden serlo de ambos pretendientes.

Cuando la declaración sea oral se extenderá un acta que será firmada por el alcalde, los pretendientes, las personas que hubiesen prestado su consentimiento y los testigos. (*)

(*) Artículo modificado por el Artículo 2 de la Ley Nº 27118, publicada el 23 mayo 1999, cuyo texto es el siguiente:

"Artículo 248.- Quienes pretendan contraer matrimonio civil lo declararán oralmente o por escrito al alcalde provincial o distrital del domicilio de cualquiera de ellos.

Acompañarán copia certificada de las partidas de nacimiento, la prueba del domicilio y el certificado médico, expedido en fecha no anterior a treinta días, que acredite que no están incursos en los impedimentos establecidos en el Artículo 241, inciso 2 y 243 inciso 3, o si en el lugar no hubiere servicio médico oficial y gratuito, la declaración jurada de no tener tal impedimento.

Acompañarán también en sus respectivos casos, la dispensa judicial de la impubertad, el instrumento en que conste el asentimiento de los padres o ascendientes o la licencia judicial supletoria, la dispensa del parentesco de consanguinidad colateral en tercer grado, copia certificada de la partida de defunción del cónyuge anterior o la sentencia de divorcio o de invalidación del matrimonio anterior, el certificado consular de soltería o viudez, y todos los demás documentos que fueren necesarios según las circunstancias.

Cada pretendiente presentará, además, a dos testigos mayores de edad que lo conozcan por lo menos desde tres años antes, quienes depondrán, bajo juramento, acerca de si existe o no algún impedimento. Los mismos testigos pueden serlo de ambos pretendientes.

Cuando la declaración sea oral se extenderá un acta que será firmada por el alcalde, los pretendientes, las personas que hubiesen prestado su consentimiento y los testigos."

PROCESOS CONSTITUCIONALES

Artículo 249 Dispensa judicial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El juez de primera instancia puede dispensar a los pretendientes de la obligación de presentar algunos documentos, cuando sean de muy difícil o imposible obtención.

Artículo 250 Publicación de matrimonio proyectado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El alcalde anunciará el matrimonio proyectado, por medio de un aviso que se fijará en la oficina de la municipalidad durante ocho días y que se publicará una vez por periódico, donde lo hubiere.

El aviso consignará el nombre, nacionalidad, edad, profesión, ocupación u oficio, domicilio de los contrayentes, el lugar donde será celebrado el matrimonio y la advertencia de que todo el que conozca la existencia de algún impedimento debe denunciarlo. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 26205, publicada el 02 julio 1993, cuyo texto es el siguiente:

"Artículo 250.- El alcalde anunciará el matrimonio proyectado, por medio de un aviso que se fijará en la oficina de la municipalidad durante ocho días y que se publicará una vez por periódico, donde lo hubiere.

En la circunscripción que no exista periódico, el aviso se efectuará a través de la emisora radial de la respectiva localidad que elijan los contrayentes, o de la más cercana a su localidad; debiendo entregarse el texto publicado, con la firma y libreta electoral del responsable de la emisora radial, al jefe de los Registros Civiles.

El aviso consignará el nombre, nacionalidad, edad, profesión, ocupación u oficio, domicilio de los contrayentes, el lugar donde será celebrado el matrimonio y la advertencia de que todo el que conozca la existencia de algún impedimento debe denunciarlo."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 251 Edicto domiciliar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si fuere diverso el domicilio de los contrayentes, se oficiará al alcalde que corresponda para que ordene también la publicación prescrita en el artículo 250, en su jurisdicción.

Artículo 252 Dispensa de la publicación del edicto matrimonial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El alcalde puede dispensar la publicación de los avisos si median causas razonables y siempre que se presenten todos los documentos exigidos en el artículo 248.

Artículo 253 Oposición de terceros a la celebración del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Todos los que tengan interés legítimo pueden oponerse a la celebración del matrimonio cuando exista algún impedimento. La oposición se formula por escrito ante cualquiera de los alcaldes que haya publicado los avisos.

Si la oposición no se funda en causa legal, el alcalde la rechazará de plano, sin admitir recurso alguno. Si se funda en causa legal y los pretendientes niegan su existencia, el alcalde remitirá lo actuado al juez.

Artículo 254 Oposición del Ministerio Público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Ministerio Público debe oponerse de oficio al matrimonio cuando tenga noticia de la existencia de alguna causa de nulidad.

Artículo 255 Denuncia de impedimento matrimonial por tercero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cualquier persona que conozca la existencia de un impedimento que constituya alguna causal de nulidad, puede denunciarlo.

La denuncia puede hacerse oralmente o por escrito y se remitirá al Ministerio Público, el cual, si la encuentra fundada, formulará la oposición.

Artículo 256 Procedimiento de la Oposición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es competente para conocer de la oposición al matrimonio el juez de primera instancia del lugar donde se haya recibido la declaración a que se refiere el artículo 248.

La oposición se tramita conforme a lo dispuesto en el Código de Procedimientos Civiles para los incidentes, con audiencia del Ministerio Público.

Si se declara infundada la oposición hay lugar al recurso de apelación. Si se declara fundada hay lugar a los recursos de apelación y de nulidad. (*)

(*) Artículo modificado por el Artículo 5 del Decreto Ley N° 25940, publicado el 11 diciembre 1992, cuyo texto es el siguiente:

"Artículo 256.- Es competente para conocer la oposición al matrimonio, el Juez de Paz Letrado del lugar donde éste habría de celebrarse.

Remitido el expediente de oposición por el alcalde, el Juez requerirá al oponente para que interponga demanda dentro de quinto día. El Ministerio Público interpondrá su demanda dentro de diez días contados desde publicado el aviso previsto en el artículo 250 o de formulada la denuncia citada en el artículo anterior.

Vencidos los plazos citados en el párrafo anterior sin que se haya interpuesto demanda, se archivará definitivamente lo actuado.

La oposición se tramita como proceso sumarísimo."(*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Ùnico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 256.- Es competente para conocer la oposición al matrimonio, el Juez de Paz Letrado del lugar donde éste habría de celebrarse.

Remitido el expediente de oposición por el alcalde, el Juez requerirá al oponente para que interponga demanda dentro de quinto día. El Ministerio Público interpondrá su demanda dentro de diez días contados desde publicado el aviso previsto en el Artículo 250 o de formulada la denuncia citada en el Artículo anterior.

Vencidos los plazos citados en el párrafo anterior sin que se haya interpuesto demanda, se archivará definitivamente lo actuado.

La oposición se tramita como proceso sumarísimo."

Artículo 257 Indemnización por oposición infundada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se declara infundada la oposición, quien la formuló queda sujeto al pago de la indemnización de daños y perjuicios. Los ascendientes y el Ministerio Público están exonerados de esta responsabilidad. Si la denuncia hubiera sido maliciosa, es igualmente responsable quien la formula. En ambos casos, la indemnización la fija prudencialmente el juez, teniendo en cuenta el daño moral.

Artículo 258 Declaración de capacidad de los pretendientes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Transcurrido el plazo señalado para la publicación de los avisos sin que se haya producido oposición o desestimada ésta, y no teniendo el alcalde noticia de ningún impedimento, declarará la capacidad de los pretendientes y que pueden contraer matrimonio dentro de los cuatro meses siguientes.

Si el alcalde tuviese noticia de algún impedimento o si de los documentos presentados y de la información producida no resulta acreditada la capacidad de los pretendientes, remitirá lo actuado al juez, quien, con citación del Ministerio Público, resolverá lo conveniente, en el plazo de tres días.

Artículo 259 Celebración del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El matrimonio se celebra en la municipalidad, públicamente, ante el alcalde que ha recibido la declaración, compareciendo los contrayentes en presencia de dos testigos mayores de edad y vecinos del lugar. El alcalde, después de leer los artículos 287, 288, 289, 290, 418 y 419, preguntará a cada uno de los pretendientes si persisten en su voluntad de celebrar el matrimonio y respondiendo ambos afirmativamente, extenderá el acta de casamiento, la que será firmada por el alcalde, los contrayentes y los testigos.

Artículo 260 Persona facultada a celebrar matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El alcalde puede delegar, por escrito, la facultad de celebrar el matrimonio a otros regidores, a los funcionarios municipales, directores o jefes de hospitales o establecimientos análogos.

El matrimonio puede celebrarse también ante el párroco o el Ordinario del lugar por delegación del alcalde respectivo.

En este caso el párroco o el Ordinario remitirá dentro de un plazo no mayor de cuarentiocho horas el certificado del matrimonio a la oficina del registro del estado civil respectivo.

Artículo 261 Celebración del matrimonio en distinta jurisdicción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El matrimonio puede celebrarse ante el alcalde de otro concejo municipal, mediante autorización escrita del alcalde competente.

Artículo 262 Celebración del matrimonio en comunidades campesinas y nativas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El matrimonio civil puede tramitarse y celebrarse también en las comunidades campesinas y nativas, ante un comité especial constituído por la autoridad educativa e integrado por los dos directivos de mayor jerarquía de la respectiva comunidad. La presidencia del comité recae en uno de los directivos de mayor jerarquía de la comunidad.

Artículo 263 Facultad del jefe de registro para celebrar matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En las capitales de provincia donde el registro de estado civil estuviese a cargo de funcionarios especiales, el jefe de aquél ejerce las atribuciones conferidas a los alcaldes por este título.

Artículo 264 Matrimonio por apoderado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El matrimonio puede contraerse por apoderado especialmente autorizado por escritura pública, con identificación de la persona con quien ha de celebrarse, bajo sanción de nulidad. Es indispensable la presencia de esta última en el acto de celebración.

El matrimonio es nulo si el poderdante revoca el poder o deviene incapaz antes de la celebración, aun cuando el apoderado ignore tales hechos. Para que surta efecto la revocatoria debe notificarse al apoderado y al otro contrayente.

El poder caduca a los seis meses de otorgado.

Artículo 265 Matrimonio fuera del local municipal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El alcalde puede, excepcionalmente, celebrar el matrimonio fuera del local de la municipalidad.

Artículo 266 Gratuidad de trámites matrimoniales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ninguno de los funcionarios o servidores públicos que intervienen en la tramitación y celebración del matrimonio cobrará derecho alguno.

Artículo 267 Sanciones al infractor de la gratuidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El infractor del artículo 266 sufrirá destitución del cargo, sin perjuicio de la responsabilidad penal.

Artículo 268 Matrimonio por inminente peligro de muerte
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si alguno de los contrayentes se encuentra en inminente peligro de muerte, el matrimonio puede celebrarse sin observar las formalidades que deben precederle. Este matrimonio se celebrará ante el párroco o cualquier otro sacerdote y no produce efectos civiles si alguno de los contrayentes es incapaz.

La inscripción sólo requiere la presentación de copia certificada de la partida parroquial.

Dicha inscripción, sobreviva o no quien se encontraba en peligro de muerte, debe efectuarse dentro del año siguiente de celebrado el matrimonio, bajo sanción de nulidad.

CAPITULO CUARTO Prueba del Matrimonio
#####################################

Artículo 269 Prueba del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para reclamar los efectos civiles del matrimonio debe presentarse copia certificada de la partida del registro del estado civil.

La posesión constante del estado de matrimonio, conforme a la partida, subsana cualquier defecto puramente formal de ésta.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES

Artículo 270 Prueba supletoria del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Comprobada la falta o pérdida del registro o del acta correspondiente, es admisible cualquier otro medio de prueba.

Artículo 271 Sentencia penal como prueba del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la prueba del matrimonio resulta de un proceso penal, la inscripción de la sentencia en el registro del estado civil tiene la misma fuerza probatoria que la partida.

Artículo 272 Posesión constante de estado de casados
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La posesión constante del estado de casados de los padres, constituye uno de los medios de prueba del matrimonio, si hubiesen muerto o se hallasen en la imposibilidad de expresarse o de proporcionar información.

Artículo 273 Dudas de la celebración del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La duda sobre la celebración del matrimonio se resuelve favorablemente a su preexistencia si los cónyuges viven o hubieran vivido en la posesión constante del estado de casados.

CAPITULO QUINTO Invalidez del Matrimonio
########################################

Artículo 274 Causales de nulidad del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nulo el matrimonio:

1.- Del enfermo mental, aun cuando la enfermedad se manifieste después de celebrado el acto o aquél tenga intervalos lúcidos. No obstante, cuando el enfermo ha recobrado la plenitud de sus facultades, la acción corresponde exclusivamente al cónyuge perjudicado y caduca si no se ejercita dentro del plazo de un año a partir del día en que cesó la incapacidad. (*)

(*) Numeral derogado por el Literal a) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

2. Del sordomudo, del ciegosordo y del ciegomudo que no sepan expresar su voluntad de manera indubitable.

Empero, si aprenden a expresarse sin lugar a duda, es de aplicación lo dispuesto en el inciso 1. (*)

(*) Numeral derogado por el Literal a) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

3. Del casado. No obstante, si el primer cónyuge del bígamo ha muerto o si el primer matrimonio ha sido invalidado o disuelto por divorcio, sólo el segundo cónyuge del bígamo puede demandar la invalidación, siempre que hubiese actuado de buena fe. La acción caduca si no se interpone dentro del plazo de un año desde el día en que tuvo conocimiento del matrimonio anterior.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Tratándose del nuevo matrimonio contraído por el cónyuge de un desaparecido sin que se hubiera declarado la muerte presunta de éste, sólo puede ser impugnado, mientras dure el estado de ausencia, por el nuevo cónyuge y siempre que hubiera procedido de buena fe.

En el caso del matrimonio contraído por el cónyuge de quien fue declarado  presuntamente muerto, es de aplicación el artículo 68.

4. De los consanguíneos o afines en línea recta.

5. De los consanguíneos en segundo y tercer grado de la línea colateral.

Sin embargo, tratándose del tercer grado, el matrimonio se convalida si se obtiene dispensa judicial del parentesco.

6. De los afines en segundo grado de la línea colateral cuando el matrimonio anterior se disolvió por divorcio y el ex-cónyuge vive.

7. Del condenado por homicidio doloso de uno de los cónyuges con el sobreviviente a que se refiere el artículo 242, inciso 6.

8. De quienes lo celebren con prescindencia de los trámites establecidos en los artículos 248 a 268. No obstante, queda convalidado si los contrayentes han actuado de buena fe y se subsana la omisión.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

9. De los contrayentes que, actuando ambos de mala fe, lo celebren ante funcionario incompetente, sin perjuicio de la responsabilidad administrativa, civil o penal de éste. La acción no puede ser planteada por los cónyuges. (*) RECTIFICADO POR FE DE ERRATAS

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 275 Acción de nulidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción de nulidad debe ser interpuesta por el Ministerio Público y puede ser intentada por cuantos tengan en ella un interés legítimo y actual. Si la nulidad es manifiesta, el juez la declara de oficio. Sin embargo, disuelto el matrimonio, el Ministerio Público no puede intentar ni proseguir la nulidad ni el juez declararla de oficio.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 276 Inextinguibilidad de la acción de nulidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción de nulidad no caduca.

Artículo 277 Causales de anulabilidad del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es anulable el matrimonio:

1. Del impúber. La acción puede ser intentada por él, por sus ascendientes si no hubiesen prestado asentimiento para el matrimonio y, a falta de éstos, por el consejo de familia. No puede intentarse la acción después que el menor ha alcanzado la mayoría de edad, ni cuando la mujer ha concebido. Si la anulación hubiese sido obtenida a instancia de los padres, ascendientes o del consejo de familia, los cónyuges al llegar a la mayoría de edad, pueden confirmar su matrimonio ante el juez. La confirmación produce efecto retroactivo. (*)

(*) Inciso modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"1. Del impúber. La pretensión puede ser ejercida por él luego de llegar a la mayoría de edad, por sus ascendientes si no hubiesen prestado asentimiento para el matrimonio y, a falta de éstos, por el consejo de familia. No puede solicitarse la anulación después que el menor ha alcanzado mayoría de edad, ni cuando la mujer ha concebido. Aunque se hubiera declarado la anulación, los cónyuges mayores de edad pueden confirmar su matrimonio. La confirmación se solicita al Juez de Paz Letrado del lugar del domicilio conyugal y se tramita como proceso no contencioso. La resolución que aprueba la confirmación produce efectos retroactivos."

2. De quien está impedido conforme el artículo 241, inciso 2. La acción sólo puede ser intentada por el cónyuge del enfermo y caduca si no se interpone dentro del plazo de un año desde el día en que tuvo conocimiento de la dolencia o del vicio.

3. Del raptor con la raptada o a la inversa o el matrimonio realizado con retención violenta. La acción corresponde exclusivamente a la parte agraviada y sólo será admisible si se plantea dentro del plazo de un año de cesado el rapto o la retención violenta.

4. De quien no se halla en pleno ejercicio de sus facultades mentales por una causa pasajera. La acción sólo puede ser interpuesta por él, dentro de los dos años de la celebración del casamiento y siempre que no haya hecho vida común durante seis meses después de desaparecida la causa.

5. De quien lo contrae por error sobre la identidad física del otro contrayente o por ignorar algún defecto sustancial del mismo que haga insoportable la vida común. Se reputan defectos sustanciales: la vida deshonrosa, la homosexualidad, la toxicomanía, la enfermedad grave de carácter crónico, la condena por delito doloso a más de dos años de pena privativa de la libertad o el ocultamiento de la esterilización o del divorcio. La acción puede ser ejercitada sólo por el cónyuge perjudicado, dentro del plazo de dos años de celebrado.

6. De quien lo contrae bajo amenaza de un mal grave e inminente, capaz de producir en el amenazado un estado de temor, sin el cual no lo hubiera contraído. El juez apreciará las circunstancias, sobre todo si la amenaza hubiera sido dirigida contra terceras personas. La acción corresponde al cónyuge perjudicado y sólo puede ser interpuesta dentro del plazo de dos años de celebrado. El simple temor reverencial no anula el matrimonio.

7. De quien adolece de impotencia absoluta al tiempo de celebrarlo. La acción corresponde a ambos cónyuges y está expedita en tanto subsista la impotencia. No procede la anulación si ninguno de los cónyuges puede realizar la cópula sexual.

8. De quien, de buena fe, lo celebra ante funcionario incompetente, sin perjuicio de la responsabilidad administrativa, civil o penal de dicho funcionario. La acción corresponde únicamente al cónyuge o cónyuges de buena fe y debe interponerse dentro de los seis meses siguientes a la celebración del matrimonio.

Artículo 278 Carácter personal de las acciones de nulidad y anulabilidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción a que se contraen los artículos 274, incisos 1, 2 y 3, y 277 no se trasmite a los herederos, pero éstos pueden continuar la iniciada por el causante.

Artículo 279 Intransmisibilidad de la acción de nulidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción de nulidad que corresponde al cónyuge en los demás casos del artículo 274 tampoco se trasmite a sus herederos, quienes pueden continuar la iniciada por su causante. Sin embargo, esto no afecta el derecho de accionar que dichos herederos tienen por sí mismos como legítimos interesados en la nulidad.

Artículo 280 Petición de invalidez por representación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La invalidez del matrimonio puede ser demandada por apoderado si está facultado expresamente y por escritura pública, bajo sanción de nulidad.

Artículo 281 Procedimiento para invalidez del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Durante el juicio de invalidez del matrimonio, las peticiones sobre separación temporal de los cónyuges, asignación para alimentos y gastos judiciales, oposición a dichas asignaciones, inventario, medidas de seguridad del patrimonio común y guarda de los hijos menores, se sujetarán a las normas pertinentes relativas al juicio de separación de cuerpos y de divorcio. (*)

(*) Artículo modificado por el Artículo 5 del Decreto Ley N° 25940, publicado el 11 diciembre 1992, cuyo texto es el siguiente:

"Artículo 281.- La pretensión de invalidez de matrimonio se tramita como proceso de conocimiento, y le son aplicables, en cuanto sean pertinentes, las disposiciones establecidas para los procesos de separación de cuerpos o divorcio por causal." (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 281.- La pretensión de invalidez del matrimonio se tramita como proceso de conocimiento, y le son aplicables, en cuanto sean pertinentes, las disposiciones establecidas para los procesos de separación de cuerpos o divorcio por causal."

Artículo 282 Invalidez del matrimonio y patria potestad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Al declarar la invalidez del matrimonio, el juez determina lo concerniente al ejercicio de la patria potestad, sujetándose a lo establecido para el divorcio.

Artículo 283 Indemnización por invalidez de matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son aplicables a la invalidez del matrimonio las disposiciones establecidas para el caso del divorcio en lo que se refiere a la indemnización de daños y perjuicios.

Artículo 284 Efectos del matrimonio invalidado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El matrimonio invalidado produce efectos civiles respecto de los cónyuges e hijos si se contrajo de buena fe, como si fuese un matrimonio válido disuelto por divorcio.

Si hubo mala fe en uno de los cónyuges, el matrimonio no produce efectos en su favor, pero sí respecto del otro y de los hijos.

El error de derecho no perjudica la buena fe.

Artículo 285 Efectos de la invalidez matrimonial frente a terceros
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El matrimonio invalidado produce los efectos de un matrimonio válido disuelto por divorcio, frente a los terceros que hubieran actuado de buena fe.

Artículo 286 Casos de validez del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El matrimonio contraído con infracción del artículo 243 es válido.

TITULO II Relaciones Personales entre los Cónyuges
==================================================

CAPITULO UNICO Deberes y derechos que nacen del matrimonio
##########################################################

Artículo 287 Obligaciones comunes de los cónyuges
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los cónyuges se obligan mutuamente por el hecho del matrimonio a alimentar y educar a sus hijos.

CONCORDANCIAS:     Ley N° 28970 (Ley que crea el Registro de Deudores Alimentarios Morosos)
R.A. Nº 136-2007-CE-PJ (Crean el Registro de Deudores Alimentarios Morosos -REDAM y aprueban Directiva)

Artículo 288 Deber de fidelidad y asistencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los cónyuges se deben recíprocamente fidelidad y asistencia.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 289 Deber de cohabitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es deber de ambos cónyuges hacer vida común en el domicilio conyugal. El juez puede suspender este deber cuando su cumplimiento ponga en grave peligro la vida, la salud o el honor de cualquiera de los cónyuges o la actividad económica de la que depende el sostenimiento de la familia.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 290 Igualdad en el hogar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ambos cónyuges tienen el deber y el derecho de participar en el gobierno del hogar y de cooperar al mejor desenvolvimiento del mismo.

A ambos compete, igualmente, fijar y mudar el domicilio conyugal y decidir las cuestiones referentes a la economía del hogar.

Artículo 291 Obligación unilateral de sostener la familia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si uno de los cónyuges se dedica exclusivamente al trabajo del hogar y al cuidado de los hijos, la obligación de sostener a la familia recae sobre el otro, sin perjuicio de la ayuda y colaboración que ambos cónyuges se deben en uno y otro campo.

Cesa la obligación de uno de los cónyuges de alimentar al otro cuando éste abandona la casa conyugal sin justa causa y rehusa volver a ella. En este caso el juez puede, según las circunstancias, ordenar el embargo parcial de las rentas del abandonante en beneficio del cónyuge inocente y de los hijos. El mandamiento de embargo queda sin efecto cuando lo soliciten ambos cónyuges.

Artículo 292 Representación de la sociedad conyugal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Corresponde conjuntamente a los cónyuges la representación legal de la sociedad conyugal. Cualquiera de ellos puede, sin embargo, dar poder al otro para que ejerza solo dicha representación, en todo o en parte.  (*)

(*) Primer párrafo modificado por el Artículo 5 del Decreto Ley N° 25940, publicado el 11 diciembre 1992, cuyo texto es el siguiente:

"Artículo 292.- La representación de la sociedad conyugal es ejercida conjuntamente por los cónyuges, sin perjuicio de lo dispuesto por el Código Procesal Civil. Cualquiera de ellos, sin embargo, puede otorgar poder al otro para que ejerza dicha representación de manera total o parcial."

Para las necesidades ordinarias del hogar, la sociedad es representada indistintamente por el marido o por la mujer. Si cualquiera de ellos abusa de este derecho, el juez puede limitárselo a instancias del otro. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 292.- La representación de la sociedad conyugal es ejercida conjuntamente por los cónyuges, sin perjuicio de lo dispuesto por el Código Procesal Civil. Cualquiera de ellos, sin embargo, puede otorgar poder al otro para que ejerza dicha representación de manera total o parcial.

Para las necesidades ordinarias del hogar y actos de administración y conservación, la sociedad es representada indistintamente por cualquiera de los cónyuges.

Si cualquiera de los cónyuges abusa de los derechos a que se refiere este artículo, el Juez de Paz Letrado puede limitárselos en todo o parte. La pretensión se tramita como proceso abreviado."

Artículo 293 Libertad de trabajo de los cónyuges
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cada cónyuge puede ejercer cualquier profesión o industria permitidos por la ley, así como efectuar cualquier trabajo fuera del hogar, con el asentimiento expreso o tácito del otro. Si éste lo negare, el juez puede autorizarlo, si lo justifica el interés de la familia.

Artículo 294 Representación unilateral de la sociedad conyugal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Uno de los cónyuges asume la dirección y representación de la sociedad:

1.- Si el otro está impedido por interdicción u otra causa.

2.- Si se ignora el paradero del otro o éste se encuentra en lugar remoto.

3.- Si el otro ha abandonado el hogar.

TITULO III Régimen Patrimonial
==============================

CAPITULO PRIMERO Disposiciones Generales
########################################

Artículo 295 Elección del régimen patrimonial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Antes de la celebración del matrimonio, los futuros cónyuges pueden optar libremente por el régimen de sociedad de gananciales o por el de separación de patrimonios, el cual comenzará a regir al celebrarse el casamiento.

Si los futuros cónyuges optan por el régimen de separación de patrimonios, deben otorgar escritura pública, bajo sanción de nulidad.

Para que surta efecto debe inscribirse en el registro personal.

A falta de escritura pública se presume que los interesados han optado por el régimen de sociedad de gananciales.

Artículo 296 Sustitución del Régimen Patrimonial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Durante el matrimonio, los cónyuges pueden sustituir un régimen por el otro. Para la validez del convenio son necesarios el otorgamiento de escritura pública y la inscripción en el registro personal. El nuevo régimen tiene vigencia desde la fecha de su inscripción.

Artículo 297 Sustitución judicial del régimen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el caso de hallarse en vigencia el régimen de sociedad de gananciales, cualquiera de los cónyuges puede recurrir al juez para que dicho régimen se sustituya por el de separación, en los casos a que se refiere el artículo 329.

Artículo 298 Liquidación del régimen patrimonial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Al terminar la vigencia de un régimen patrimonial se procederá necesariamente a su liquidación.

Artículo 299 Bienes del régimen patrimonial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El régimen patrimonial comprende tanto los bienes que los cónyuges tenían antes de entrar aquél en vigor como los adquiridos por cualquier título durante su vigencia.

Artículo 300 Obligación mutua de sostener el hogar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cualquiera que sea el régimen en vigor, ambos cónyuges están obligados a contribuir al sostenimiento del hogar según sus respectivas posibilidades y rentas.

En  caso necesario, el juez reglará la contribución de cada uno.

CAPITULO SEGUNDO Sociedad de Gananciales
########################################

Artículo 301 Bienes de la sociedad de gananciales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el régimen de sociedad de gananciales puede haber bienes propios de cada cónyuge y bienes de la sociedad.

PROCESOS CONSTITUCIONALES

Artículo 302 Bienes de la sociedad de gananciales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son bienes propios de cada cónyuge:

1.- Los que aporte al iniciarse el régimen de sociedad de gananciales.

2.- Los que adquiera durante la vigencia de dicho régimen a título oneroso, cuando la causa de adquisición ha precedido a aquélla.

3.- Los que adquiera durante la vigencia del régimen a título gratuito.

4.- La indemnización por accidentes o por seguros de vida, de daños personales o de enfermedades, deducidas las primas pagadas con bienes de la sociedad.

5.- Los derechos de autor e inventor.

6.- Los libros, instrumentos y útiles para el ejercicio de la profesión o trabajo, salvo que sean accesorios de una empresa que no tenga la calidad de bien propio.

7.- Las acciones y las participaciones de sociedades que se distribuyan gratuitamente entre los socios por revaluación del patrimonio social, cuando esas acciones o participaciones sean bien propio.

8.- La renta vitalicia a título gratuito y la convenida a título oneroso cuando la contraprestación constituye bien propio.

9.- Los vestidos y objetos de uso personal, así como los diplomas, condecoraciones, correspondencia y recuerdos de familia.

Artículo 303 Administración de bienes propios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cada cónyuge conserva la libre administración de sus bienes propios y puede disponer de ellos o gravarlos.

Artículo 304 Irrenunciabilidad de actos de liberalidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ninguno de los cónyuges puede renunciar a una herencia o legado o dejar de aceptar una donación sin el consentimiento del otro.

Artículo 305 Administración de bienes propios del otro cónyuge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si uno de los cónyuges no contribuye con los frutos o productos de sus bienes propios al sostenimiento del hogar, el otro puede pedir que pasen a su administración, en todo o en parte. En este  caso, está obligado a constituir hipoteca y, si carece de bienes propios, otra garantía, si es posible, según el prudente arbitrio del juez, por el valor de los bienes que reciba.

Artículo 306 Atribución del cónyuge administrador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando uno de los cónyuges permite que sus bienes propios sean administrados en todo o en parte por el otro, no tiene éste sino las facultades inherentes a la mera administración y queda obligado a devolverlos en cualquier momento a requerimiento del propietario.

Artículo 307 Pago de deudas anteriores al régimen de gananciales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las deudas de cada cónyuge anteriores a la vigencia del régimen de gananciales son pagadas con sus bienes propios, a menos que hayan sido contraídas en beneficio del futuro hogar, en cuyo caso se pagan con bienes sociales a falta de bienes propios del deudor.

Artículo 308 Deudas personales del otro cónyuge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los bienes propios de uno de los cónyuges, no responden de las deudas personales del otro, a menos que se pruebe que se contrajeron en provecho de la familia.

Artículo 309 Responsabilidad extracontractual del cónyuge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La responsabilidad civil por acto ilícito de un cónyuge no perjudica al otro en sus bienes propios ni en la parte de los de la sociedad que le corresponderían en caso de liquidación. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 309.- La responsabilidad extracontractual de un cónyuge no perjudica al otro en sus bienes propios ni en la parte de los de la sociedad que le corresponderían en caso de liquidación."

Artículo 310 Bienes sociales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son bienes sociales todos los no comprendidos en el artículo 302, incluso los que cualquiera de los cónyuges adquiera por su trabajo, industria o profesión, así como los frutos y productos de todos los bienes propios y de la sociedad y las rentas de los derechos de autor e inventor.

También tienen la calidad de bienes sociales los edificios construídos a costa del caudal social en suelo propio de uno de los cónyuges, abonándose a éste el valor del suelo al momento del reembolso.

CONCORDANCIAS:     R.N° 298-2019-SUNARP-PT (Disponen la publicación de los precedentes de observancia obligatoria aprobados en sesión ordinaria del 220º Pleno del Tribunal Registral, modalidad presencial)

Artículo 311 Reglas para calificación de los bienes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para la calificación de los bienes, rigen las reglas siguientes:

1.- Todos los bienes se presumen sociales, salvo prueba en contrario.

2.- Los bienes sustituídos o subrogados a otros se reputan de la misma condición de los que sustituyeron o subrogaron.

3.- Si vendidos algunos bienes, cuyo precio no consta haberse invertido, se compran después otros equivalentes, se presume, mientras no se pruebe lo contrario, que la adquisición posterior es hecha con el producto de la enajenación anterior.

Artículo 312 Prohibición de contratos entre cónyuges
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los cónyuges no pueden celebrar contratos entre sí respecto de los bienes de la sociedad.

Artículo 313 Administración común del patrimonio social
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Corresponde a ambos cónyuges la administración del patrimonio social. Sin embargo, cualquiera de ellos puede facultar al otro para que asuma exclusivamente dicha administración respecto de todos o de algunos de los bienes. En este caso, el cónyuge administrador indemnizará al otro por los daños y perjuicios que sufra a consecuencia de actos dolosos o culposos.

Artículo 314 Administración de bienes sociales y propios por el otro cónyuge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La administración de los bienes de la sociedad y de los propios de uno de los cónyuges corresponde al otro en los casos del artículo 294, incisos 1 y 2.

Si uno de los cónyuges ha abandonado el hogar, corresponde al otro la administración de los bienes sociales.

Artículo 315 Disposición de los bienes sociales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para disponer de los bienes sociales o gravarlos, se requiere la intervención del marido y la mujer. Empero, cualquiera de ellos puede ejercitar tal facultad, si tiene poder especial del otro.

Lo dispuesto en el párrafo anterior no rige para los actos de adquisición de bienes muebles, los cuales pueden ser efectuados por cualquiera de los cónyuges. Tampoco rige en los casos considerados en las leyes especiales.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 316 Cargas de la sociedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son de cargo de la sociedad:

1.- El sostenimiento de la familia y la educación de los hijos comunes.

2.- Los alimentos que uno de los cónyuges esté obligado por ley a dar a otras personas.

3.- El importe de lo donado o prometido a los hijos comunes por ambos cónyuges.

4.- Las mejoras necesarias y las reparaciones de mera conservación o mantenimiento hechas en los predios propios, así como las retribuciones y tributos que los afecten.

5.- Las mejoras útiles y de recreo que la sociedad decida introducir en bienes propios de uno de los cónyuges con consentimiento de éste.

6.- Las mejoras y reparaciones realizadas en los bienes sociales, así como los tributos y retribuciones que los afecten.

7.- Los atrasos o réditos devengados de las obligaciones a que estuviesen afectos tanto los bienes propios como los sociales, cualquiera que sea la época a que correspondan. (*) RECTIFICADO POR FE DE ERRATAS

8.- Las cargas que pesan sobre los usufructuarios respecto de los bienes propios de cada cónyuge.

9.- Los gastos que cause la administración de la sociedad.

Artículo 317 Responsabilidad por deudas de la sociedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los bienes sociales y, a falta o por insuficiencia de éstos, los propios de ambos cónyuges, responden a prorrata de las deudas que son de cargo de la sociedad.

Artículo 318 Fin de la sociedad de gananciales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Fenece el régimen de la sociedad de gananciales:

1.-  Por invalidación del matrimonio.
2.-  Por separación de cuerpos.
3.-  Por divorcio.
4.-  Por declaración de ausencia.
5.-  Por muerte de uno de los cónyuges.
6.-  Por cambio de régimen patrimonial.

Artículo 319 Fin de la Sociedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para las relaciones entre los cónyuges se considera que el fenecimiento de la sociedad de gananciales se produce en la fecha de la muerte o de la declaración de muerte presunta o de ausencia; en la de notificación con la demanda de invalidez del matrimonio, de divorcio, de separación de cuerpos o de separación judicial de bienes; y en la fecha de la escritura pública, cuando la separación de bienes se establece de común acuerdo.

Respecto a terceros, el régimen de sociedad de gananciales se considera fenecido en la fecha de la inscripción correspondiente en el registro personal. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27495, publicada el 07 julio 2001, cuyo texto es el siguiente:

“Artículo 319.- 
Para las relaciones entre los cónyuges se considera que el fenecimiento de la sociedad de gananciales se produce en la fecha de la muerte o de la declaración de muerte presunta o de ausencia; en la de notificación con la demanda de invalidez del matrimonio, de divorcio, de separación de cuerpos o de separación judicial de bienes; y en la fecha de la escritura pública, cuando la separación de bienes se establece de común acuerdo. En los casos previstos en los incisos 5 y 12 del Artículo 333, la sociedad de gananciales fenece desde el momento en que se produce la separación de hecho.

Respecto a terceros, el régimen de sociedad de gananciales se considera fenecido en la fecha de la inscripción correspondiente en el registro personal.”

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 320 Inventario valorizado de bienes sociales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Fenecida la sociedad de gananciales, se procede de inmediato a la formación del inventario valorizado de todos los bienes. El inventario puede formularse en documento privado con firmas legalizadas, si ambos cónyuges o sus herederos están de acuerdo. En caso contrario el inventario se hace judicialmente.

No se incluye en el inventario el menaje ordinario del hogar en los casos del artículo 318, incisos 4 y 5, en que corresponde al cónyuge del ausente o al sobreviviente.

Artículo 321 Bienes excluídos del menaje
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El menaje ordinario del hogar no comprende:

1.- Los vestidos y objetos de uso personal.

2.- El dinero.

3.- Los títulos valores y otros documentos de carácter patrimonial.

4.- Las joyas.

5.- Las medallas, condecoraciones, diplomas y otras distinciones.

6.- Las armas.

7.- Los instrumentos de uso profesional u ocupacional.

8.- Las colecciones científicas o artísticas.

9.- Los bienes culturales-históricos.

10.- Los libros, archivos y sus contenedores.

11.- Los vehículos motorizados.

12.- En general, los objetos que no son de uso doméstico.

Artículo 322 Liquidación de la sociedad de gananciales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Realizado el inventario, se pagan las obligaciones sociales y las cargas y después se reintegra a cada cónyuge los bienes propios que quedaren.

Artículo 323 Gananciales
~~~~~~~~~~~~~~~~~~~~~~~~

Son gananciales los bienes remanentes después de efectuados los actos indicados en el artículo 322.

Los gananciales se dividen por mitad entre ambos cónyuges o sus respectivos herederos.

Cuando la sociedad de gananciales ha fenecido por muerte o declaración de ausencia de uno de los cónyuges, el otro tiene preferencia para la adjudicación de la casa en que habita la familia y del establecimiento agrícola, artesanal, industrial o comercial de carácter familiar, con la obligación de reintegrar el exceso de valor, si lo hubiera.

Artículo 324 Pérdida de gananciales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de separación de hecho, el cónyuge culpable pierde el derecho a gananciales proporcionalmente a la duración de la separación.

Artículo 325 Liquidación de varias sociedades de gananciales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Siempre que haya de ejecutarse simultáneamente la liquidación de gananciales de dos o más matrimonios contraídos sucesivamente por una misma persona, se admitirá, en defecto de inventarios previos a cada matrimonio, toda clase de pruebas para determinar los bienes de cada sociedad; y, en caso de duda, se dividirán los gananciales entre las diferentes sociedades, teniendo en cuenta el tiempo de su duración y las pruebas que se haya podido actuar acerca de los bienes propios de los respectivos cónyuges.

Artículo 326 Unión de hecho
~~~~~~~~~~~~~~~~~~~~~~~~~~~

La unión de hecho, voluntariamente realizada y mantenida por un varón y una mujer, libres de impedimento matrimonial, para alcanzar finalidades y cumplir deberes semejantes a los del matrimonio, origina una sociedad de bienes que se sujeta al régimen de sociedad de gananciales, en cuanto le fuere aplicable, siempre que dicha unión haya durado por lo menos dos años contínuos.

La posesión constante de estado a partir de fecha aproximada puede probarse con cualquiera de los medios admitidos por la ley procesal, siempre que exista un principio de prueba escrita.

La unión de hecho termina por muerte, ausencia, mutuo acuerdo o decisión unilateral. En este último caso, el juez puede conceder, a elección del abandonado, una cantidad de dinero por concepto de indemnización o una pensión de alimentos, además de los derechos que le correspondan de conformidad con el régimen de sociedad de gananciales.

Tratándose de la unión de hecho que no reúna las condiciones señaladas en este artículo, el interesado tiene expedita, en su caso, la acción de enriquecimiento indebido.

“Las uniones de hecho que reúnan las condiciones señaladas en el presente artículo producen, respecto de sus miembros, derechos y deberes sucesorios, similares a los del matrimonio, por lo que las disposiciones contenidas en los artículos 725, 727, 730, 731, 732, 822, 823, 824 y 825 del Código Civil se aplican al integrante sobreviviente de la unión de hecho en los términos en que se aplicarían al cónyuge.” (*)

(*) Párrafo incorporado por el Artículo 4 de la Ley N° 30007, publicada el 17 abril 2013.

(*) De conformidad con la Única Disposición Complementaria Final de la Ley N° 30311, publicada el 18 marzo 2015, se dispone que la calidad de convivientes conforme a lo señalado en el presente artículo, se acredita con la inscripción del reconocimiento de la unión de hecho en el Registro Personal de la Oficina Registral que corresponda al domicilio de los convivientes.

(*) De conformidad con el Artículo 1 de la Ley N° 30907, publicada el 11 enero 2019, el objeto de la citada ley es establecer la equivalencia de la unión de hecho con el matrimonio para acceder a la pensión de sobrevivencia siempre que se cumpla con los requisitos establecidos en el presente artículo.

CONCORDANCIAS:     R. Nº 088-2011-SUNARP-SA (Aprueban Directiva que establece los criterios registrales para la inscripción de las Uniones de Hecho,
su Cese y otros actos inscribibles directamente vinculados)

CAPITULO TERCERO Separación de Patrimonios
##########################################

Artículo 327 Separación del patrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el régimen de separación de patrimonios, cada cónyuge conserva a plenitud la propiedad, administración y disposición de sus bienes presentes y futuros y le corresponden los frutos y productos de dichos bienes.

Artículo 328 Deudas personales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cada cónyuge responde de sus deudas con sus propios bienes.

Artículo 329 Separación de patrimonio por declaración de insolvencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Además de los casos a que se refieren los artículos 295 y 296, el régimen de separación es establecido por el juez, a pedido del cónyuge agraviado, cuando el otro abusa de las facultades que le corresponden o actúa con dolo o culpa.

Interpuesta la demanda, puede el juez dictar, a pedido del demandante o de oficio, las providencias concernientes a la seguridad de los intereses de aquél.  Dichas medidas, así como la sentencia, deben ser inscritas en el registro personal para que surtan efecto frente a terceros. La separación surte efecto entre los cónyuges desde la fecha de la notificación con la demanda.

Artículo 330 Separación de patrimonio a solicitud del cónyuge agraviado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La declaración de insolvencia de uno de los cónyuges determina de pleno derecho la sustitución del régimen de sociedad de gananciales por el de separación de patrimonios y, para que produzca efectos frente a terceros, se inscribirá en el registro personal de oficio, a solicitud del insolvente, de su cónyuge o del Administrador Especial. (*)

(*) Artículo modificado por la Quinta Disposición Final del Decreto Legislativo Nº 845, publicado el 21-09-96, cuyo texto es el siguiente:

"Artículo 330.- La declaración de insolvencia de uno de los cónyuges determina de pleno derecho la sustitución del régimen de sociedad de gananciales por el de separación de patrimonios y, para que produzca efectos frente a terceros, se inscribirá en el registro personal de oficio, a solicitud del insolvente, de su cónyuge o del Administrador Especial." (*)

(*) Artículo modificado por la Quinta Disposición Final del Texto Único Ordenado de la Ley de Reestructuración Patrimonial aprobado por el Artículo 1 del Decreto Supremo N° 014-99-ITINCI, publicado el 01 noviembre 1999, cuyo texto es el siguiente:

"Artículo 330.- La declaración de insolvencia de uno de los cónyuges determina de pleno derecho la sustitución del régimen de sociedad de gananciales por el de separación de patrimonios y, para que produzca efectos frente a terceros, se inscribirá en el registro personal de oficio, a solicitud del insolvente, de su cónyuge o del Administrador Especial." (*)

(*) Artículo modificado por la Primera Disposición Modificatoria de la Ley N°  27809, publicada el 08 agosto 2002, la misma que entró en vigencia a los sesenta (60) días siguientes de su publicación en el Diario Oficial El Peruano, cuyo texto es el siguiente:

Separación de patrimonio a solicitud del cónyuge agraviado

"Artículo 330.- La declaración de inicio de Procedimiento Concursal Ordinario de uno de los cónyuges determina de pleno derecho la sustitución del régimen de sociedad de gananciales por el de separación de patrimonios y, para que produzca efectos frente a terceros, se inscribirá en el registro personal de oficio a solicitud de la Comisión de Procedimientos Concursales competente, del deudor, de su cónyuge o del administrador o liquidador, Presidente de la Junta de Acreedores o cualquier acreedor interesado.

No obstante lo anterior, en el supuesto de que al momento de iniciarse el procedimiento concursal de una persona natural se encontrase vigente otro procedimiento de la misma naturaleza previamente difundido conforme a la ley de la materia respecto de la sociedad conyugal que integra, no se producirá la consecuencia prevista en el párrafo precedente en tanto se desarrolle el trámite de tal procedimiento."

Artículo 331 Fin de la separación de patrimonios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El régimen de separación de patrimonios fenece en los casos del artículo 318, incisos 1, 3, 5 y 6.

TITULO IV Decaimiento y Disolución del Vínculo
==============================================

CAPITULO PRIMERO Separación de Cuerpos
######################################

Artículo 332 Efecto de la separación de cuerpos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La separación de cuerpos suspende los deberes relativos al lecho y habitación y pone fin al régimen patrimonial de sociedad de gananciales, dejando subsistente el vínculo matrimonial.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 333 Causales
~~~~~~~~~~~~~~~~~~~~~

Son causas de separación de cuerpos:

1. El adulterio.

2. La sevicia. (*)

(*) Inciso modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

" 2. La violencia, física o psicológica, que el Juez apreciará según las circunstancias."

3. El atentado contra la vida del cónyuge.

4. La injuria grave.

5. El abandono injustificado de la casa conyugal por más de dos años contínuos o cuando la duración sumada de los períodos de abandono exceda a este plazo.

6. La conducta deshonrosa que haga insoportable la vida en común.

7. El uso habitual e injustificado de drogas alucinógenas o de sustancias que puedan generar toxicomanía.

8. La enfermedad venérea grave contraída después de la celebración del matrimonio.

9. La homosexualidad sobreviniente al matrimonio.

10. La condena por delito doloso a pena privativa de la libertad mayor de dos años, impuesta después de la celebración del matrimonio.

11. El mutuo disenso, después de transcurridos dos años de la celebración del matrimonio. (*)

(*) Inciso modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"11. Separación convencional, después de transcurridos dos años de la celebración del matrimonio" (*)

(*) Artículo modificado por el Artículo 2 de la Ley Nº 27495, publicada el 07 julio 2001, cuyo texto es el siguiente:

"Artículo 333.- Causales

Son causas de separación de cuerpos:

1. El adulterio.

2. La violencia física o psicológica, que el juez apreciará según las circunstancias.

3. El atentado contra la vida del cónyuge.

4. La injuria grave, que haga insoportable la vida en común.

5. El abandono injustificado de la casa conyugal por más de dos años continuos o cuando la duración sumada de los períodos de abandono exceda a este plazo.

6. La conducta deshonrosa que haga insoportable la vida en común.

7. El uso habitual e injustificado de drogas alucinógenas o de sustancias que puedan generar toxicomanía, salvo lo dispuesto en el Artículo 347.

8. La enfermedad grave de transmisión sexual contraída después de la celebración del matrimonio.

9. La homosexualidad sobreviniente al matrimonio.

10. La condena por delito doloso a pena privativa de la libertad mayor de dos años, impuesta después de la celebración del matrimonio.

11. La imposibilidad de hacer vida en común, debidamente probada en proceso judicial.

12. La separación de hecho de los cónyuges durante un período ininterrumpido de dos años. Dicho plazo será de cuatro años si los cónyuges tuviesen hijos menores de edad. En estos casos no será de aplicación lo dispuesto en el Artículo 335.

13. La separación convencional, después de transcurridos dos años de la celebración del matrimonio."

CONCORDANCIAS:     Ley Nº 29227 (Ley que regula el Procedimiento No Contencioso de la Separación Convencional y Divorcio Ulterior en las Municipalidades y
Notarías)

Artículo 334 Titulares de la acción de separación de cuerpos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción de separación corresponde a los cónyuges.

Si alguno es incapaz, por enfermedad mental o ausencia, la acción la puede ejercer cualquiera de sus ascendientes si se funda en causal específica. A falta de ellos el curador especial representa al incapaz.

Artículo 335 Prohibición de alegar hecho propio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ninguno de los cónyuges puede fundar la demanda en hecho propio.

Artículo 336 Improcedencia de separación de cuerpos por adulterio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No puede intentarse la separación de cuerpos por adulterio si el ofendido lo provocó, consintió o perdonó. La cohabitación posterior al conocimiento del adulterio impide iniciar o proseguir la acción.

Artículo 337 Apreciación judicial de sevicia, injuria y conducta deshonrosa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La sevicia (*), la injuria grave y la conducta deshonrosa (*) son apreciadas por el Juez teniendo en cuenta la educación, costumbre y conducta de ambos cónyuges.(*)

(*) De conformidad a la Sentencia del Tribunal Constitucional N° 018-96-I-TC, publicada el 13 mayo 1997, se declara fundada en parte la demanda de inconstitucionalidad interpuesta por contra el presente artículo, en la medida que la sevicia y la conducta deshonrosa que hace insoportable la vida en común, sean apreciadas por el juez teniendo en cuenta la educación, costumbre y conducta de ambos cónyuges, disposición que queda derogada; e infundada la demanda en lo referente a la injuria grave, disposición que queda vigente. El preente artículo, en consecuencia, se entenderá referido en adelante exclusivamente a la causal de injuria grave.

Artículo 338 Improcedencia de la acción por delito conocido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No puede invocar la causal a que se refiere el inciso 10 del artículo 333, quien conoció el delito antes de casarse.

Artículo 339 Caducidad de la acción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción basada en el artículo 333, inciso 1, 3, 9 y 10, caduca a los seis meses de conocida la causa por el ofendido y, en todo caso, a los cinco años de producida. La que se funda en los incisos 2 y 4 caduca a los seis meses de producida la causa. En los demás casos, la acción esta expedita mientras subsistan los hechos que la motivan.

Artículo 340 Efectos de la separación convencional respecto de los hijos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los hijos se confían al cónyuge que obtuvo la separación por causa específica, a no ser que el juez determine, por el bienestar de ellos, que se encargue de todos o de alguno el otro cónyuge o, si hay motivo grave, una tercera persona. Esta designación debe recaer por su orden, y siendo posible y conveniente, en alguno de los abuelos, hermanos o tíos.

Si ambos cónyuges son culpables, los hijos varones mayores de siete años quedan a cargo del padre y las hijas menores de edad así como los hijos menores de siete años al cuidado de la madre, a no ser que el juez determine otra cosa.

El padre o madre a quien se haya confiado los hijos ejerce la patria potestad respecto de ellos. El otro queda suspendido en el ejercicio, pero lo reasume de pleno derecho si el primero muere o resulta legalmente impedido.

Artículo 341 Providencia judiciales en beneficio de los hijos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En cualquier tiempo, el juez puede dictar a pedido de uno de los padres, de los hermanos mayores de edad o del consejo de familia, las providencias que sean requeridas por hechos nuevos y que considere beneficiosas para los hijos.

Artículo 342 Determinación de la pensión alimenticia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El juez señala en la sentencia la pensión alimenticia que los padres o uno de ellos debe abonar a los hijos, así como la que el marido debe pagar a la mujer o viceversa.

CONCORDANCIAS:     Ley N° 28970 (Ley que crea el Registro de Deudores Alimentarios Morosos)
R.A. Nº 136-2007-CE-PJ (Crean el Registro de Deudores Alimentarios Morosos -REDAM y aprueban Directiva)

Artículo 343 Pérdida de derechos hereditarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cónyuge separado por culpa suya pierde los derechos hereditarios que le corresponden.

Artículo 344 Revocación de consentimiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando se solicite la separación por mutuo disenso citará el juez a comparendo, pudiendo revocar su consentimiento cualquiera de las partes dentro de los treinta días posteriores a esta diligencia. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 344.- Cuando se solicite la separación convencional cualquiera de las partes puede revocar su consentimiento dentro de los treinta días naturales siguientes a la audiencia."

Artículo 345 Patria Potestad y alimentos en separación convencional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de separación por mutuo disenso, el juez fija el régimen concerniente al ejercicio de la patria potestad, los alimentos de los hijos y los de la mujer o el marido, observando, en cuanto sea conveniente, lo que ambos cónyuges acuerden.

Son aplicables a la separación por mutuo disenso las disposiciones contenidas en los artículos 340, último párrafo, y 341. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 345.- En caso de separación convencional, el Juez fija el régimen concerniente al ejercicio de la patria potestad, los alimentos de los hijos y los de la mujer o el marido, observando, en cuanto sea conveniente, lo que ambos cónyuges acuerden.

Son aplicables a la separación convencional las disposiciones contenidas en los Artículos 340, último párrafo, y 341." (*)

(*) Artículo modificado por el Artículo 3 de la Ley Nº 27495, publicada el 07 julio 2001, cuyo texto es el siguiente:

“Artículo 345.- Patria Potestad y alimentos en separación convencional

En caso de separación convencional o de separación de hecho, el juez fija el régimen concerniente al ejercicio de la patria potestad, los alimentos de los hijos y los de la mujer o el marido, observando, en cuanto sea conveniente, los intereses de los hijos menores de edad y la familia o lo que ambos cónyuges acuerden.

Son aplicables a la separación convencional y a la separación de hecho las disposiciones contenidas en los Artículos 340 último párrafo y 341.”

"Artículo 345-A.- Indemnización en caso de perjuicio"

Para invocar el supuesto del inciso 12 del Artículo 333 el demandante deberá acreditar que se encuentra al día en el pago de sus obligaciones alimentarias u otras que hayan sido pactadas por los cónyuges de mutuo acuerdo.

El juez velará por la estabilidad económica del cónyuge que resulte perjudicado por la separación de hecho, así como la de sus hijos. Deberá señalar una indemnización por daños, incluyendo el daño personal u ordenar la adjudicación preferente de bienes de la sociedad conyugal, independientemente de la pensión de alimentos que le pudiera corresponder.

Son aplicables a favor del cónyuge que resulte más perjudicado por la separación de hecho, las disposiciones contenidas en los Artículos 323, 324, 342, 343, 351 y 352, en cuanto sean pertinentes.” (*)

(*) Artículo incorporado por el Artículo 4 de la Ley Nº 27495, publicada el 07 julio 2001.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 346 Efectos de la reconciliación de los conyuges
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cesan los efectos de la separación por la reconciliación de los cónyuges. Si la reconciliación se produce durante el juicio, el juez manda cortar el proceso. Si ocurriere después de la sentencia ejecutoriada, los cónyuges lo harán presente al juez dentro del mismo proceso.

Tanto la sentencia como la reconciliación producida después de ella se inscriben en el registro personal.

Reconciliados los cónyuges, puede demandarse nuevamente la separación sólo por causas nuevas o recién sabidas. En este juicio no se invocarán los hechos perdonados, sino en cuanto contribuyan a que el juez aprecie el valor de dichas causas.

Artículo 347 Suspensión del deber de cohabitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de enfermedad mental o contagiosa de uno de los cónyuges, el otro puede pedir que se suspenda la obligación de hacer vida común, quedando subsistente las demás obligaciones conyugales.

CAPITULO SEGUNDO Divorcio
#########################

CONCORDANCIAS:     Ley Nº 29227 (Ley que regula el Procedimiento No Contencioso de la Separación Convencional y Divorcio Ulterior en las Municipalidades y
Notarías)

Artículo 348 Noción
~~~~~~~~~~~~~~~~~~~

El divorcio disuelve el vínculo del matrimonio.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 349 Causales de divorcio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puede demandarse el divorcio por las causales señaladas en el artículo 333, incisos 1 al 10. (*)

(*) Artículo modificado por el Artículo 5 de la Ley Nº 27495, publicada el 07 julio 2001, cuyo texto es el siguiente:

"Artículo 349.- Causales de divorcio

Puede demandarse el divorcio por las causales señaladas en el Artículo 333, incisos del 1 al 12.”

Artículo 350 Efectos del divorcio respecto de los cónyuges
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Por el divorcio cesa la obligación alimenticia entre marido y mujer.

Si se declara el divorcio por culpa de uno de los cónyuges y el otro careciere de bienes propios o de gananciales suficientes o estuviere imposibilitado de trabajar o de subvenir a sus necesidades por otro medio, el juez le asignará una pensión alimenticia no mayor de la tercera parte de la renta de aquél.

El ex-cónyuge puede, por causas graves, pedir la capitalización de la pensión alimenticia y la entrega del capital correspondiente.

El indigente debe ser socorrido por su ex-cónyuge aunque hubiese dado motivos para el divorcio.

Las obligaciones a que se refiere este artículo cesan automáticamente si el alimentista contrae nuevas nupcias. Cuando desaparece el estado de necesidad, el obligado puede demandar la exoneración y, en su caso, el reembolso.

CONCORDANCIAS:     Ley N° 28970 (Ley que crea el Registro de Deudores Alimentarios Morosos)
R.A. Nº 136-2007-CE-PJ (Crean el Registro de Deudores Alimentarios Morosos -REDAM y aprueban Directiva)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 351 Reparación del cónyuge inocente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si los hechos que han determinado el divorcio comprometen gravemente el legítimo interés personal del cónyuge inocente, el juez podrá concederle una suma de dinero por concepto de reparación del daño moral.

Artículo 352 Pérdida de gananciales por el cónyuge culpable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cónyuge divorciado por su culpa perderá los gananciales que procedan de los bienes del otro.

Artículo 353 Pérdida de derechos hereditarios entre conyuges divorciados
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los cónyuges divorciados no tienen derecho a heredar entre sí.

Artículo 354 Plazo de conversión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Transcurridos seis meses de la sentencia de separación por mutuo disenso, cualquiera de los cónyuges, basándose en ella, podrá pedir que se declare disuelto el vínculo del matrimonio.

Igual derecho podrá ejercer el cónyuge inocente de la separación por causal específica. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 354.- Transcurridos seis meses desde notificada la sentencia de separación convencional, cualquiera de los cónyuges, basandose en ella, podrá pedir que se declare disuelto el vínculo del matrimonio.

Igual derecho podrá ejercer el cónyuge inocente de la separación por causal específica." (*)

(*) Artículo modificado por el Artículo 6 de la Ley Nº 27495, publicada el 07 julio 2001, cuyo texto es el siguiente:

"Artículo 354.- Plazo de conversión

Transcurridos seis meses desde notificada la sentencia de separación convencional o de separación de cuerpos por separación de hecho, cualquiera de los cónyuges, basándose en ella, podrá pedir que se declare disuelto el vínculo del matrimonio.

Igual derecho podrá ejercer el cónyuge inocente de la separación por causal específica.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28384, publicada el 13 noviembre 2004, cuyo texto es el siguiente:

"Artículo 354.- Plazo de conversión

Transcurridos dos meses desde notificada la sentencia de separación convencional o de separación de cuerpos por separación de hecho, cualquiera de los cónyuges basándose en ella, podrá pedir que se declare disuelto el vínculo del matrimonio.

Igual derecho podrá ejercer el cónyuge inocente de la separación por causal específica." (*)

(*) Artículo modificado por la Primera Disposición Modificatoria de la Ley Nº 29227, publicada el 16 mayo 2008, cuyo texto es el siguiente:

"Artículo 354.- Plazo de conversión"

Transcurridos dos meses desde notificada la sentencia, la resolución de alcaldía o el acta notarial de separación convencional, o la sentencia de separación de cuerpos por separación de hecho, cualquiera de los cónyuges, basándose en ellas, podrá pedir, según corresponda, al juez, al alcalde o al notario que conoció el proceso, que se declare disuelto el vínculo del matrimonio.

Igual derecho podrá ejercer el cónyuge inocente de la separación por causal específica.”

Artículo 355 Reglas aplicadas al divorcio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son aplicables al divorcio las reglas contenidas en los artículos 334 a 342, en cuanto sean pertinentes.

Artículo 356 Corte del proceso por reconciliación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Durante la tramitación del juicio de divorcio por causal específica, el juez mandará cortar el proceso si los cónyuges se reconcilian.

Es aplicable a la reconciliación el último párrafo del artículo 346.

Si se trata de la conversión de la separación en divorcio, la reconciliación de los cónyuges, o el desistimiento de quien pidió la conversión, dejan sin efecto esta solicitud.

Artículo 357 Variación de la demanda de divorcio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El demandante puede, en cualquier estado de la causa, variar su demanda de divorcio convirtiéndola en una de separación.

Artículo 358 Facultad del juez de variar el petitorio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Aunque la demanda o la reconvención tengan por objeto el divorcio, el juez puede declarar la separación, si parece probable que los cónyuges se reconcilien.

Artículo 359 Consulta de la sentencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si no se apela de la sentencia que declara el divorcio, será consultada. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28384, publicada el 13 noviembre 2004, cuyo texto es el siguiente:

"Artículo 359.-Consulta de la sentencia

Si no se apela la sentencia que declara el divorcio, ésta será consultada, con excepción de aquella que declara el divorcio en mérito de la sentencia de separación convencional.”

Artículo 360 Continuidad de los deberes religiosos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las disposiciones de la ley sobre el divorcio y la separación de cuerpos no se extienden más allá de sus efectos civiles y dejan íntegros los deberes que la religión impone.
