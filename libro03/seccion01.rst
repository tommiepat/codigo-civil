=======================================
SECCION PRIMERA Disposiciones Generales
=======================================

PROCESOS CONSTITUCIONALES

TITULO UNICO
============

CAPITULO UNICO
##############

Artículo 233 Finalidad de la regulación de la familia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La regulación jurídica de la familia tiene por finalidad contribuir a su consolidación y fortalecimiento, en armonía con los principios y normas proclamados en la Constitución Política del Perú.

Artículo 234 Noción del matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El matrimonio es la unión voluntariamente concertada por un varón y una mujer legalmente aptos para ella y formalizada con sujeción a las disposiciones de este Código, a fin de hacer vida común.

El marido y la mujer tienen en el hogar autoridad, consideraciones, derechos, deberes y responsabilidades iguales.

Artículo 235 Deberes de los padres
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los padres están obligados a proveer al sostenimiento, protección, educación y formación de sus hijos menores según su situación y posibilidades.

Todos los hijos tienen iguales derechos.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 236 Parentesco cosanguíneo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El parentesco consanguíneo es la relación familiar existente entre las personas que descienden una de otra o de un tronco común.

El grado de parentesco se determina por el número de generaciones.

En la línea colateral, el grado se establece subiendo de uno de los parientes al tronco común y bajando después hasta el otro. Este parentesco produce efectos civiles sólo hasta el cuarto grado.

Artículo 237 Parentesco por afinidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El matrimonio produce parentesco de afinidad entre cada uno de los cónyuges con los parientes consanguíneos del otro. Cada cónyuge se halla en igual línea y grado de parentesco por afinidad que el otro por consanguinidad.

La afinidad en línea recta no acaba por la disolución del matrimonio que la produce. Subsiste la afinidad en el segundo grado de la línea colateral en caso de divorcio y mientras viva el ex-cónyuge.

PROCESOS CONSTITUCIONALES

Artículo 238 Parentesco por adopción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La adopción es fuente de parentesco dentro de los alcances de esta institución.
