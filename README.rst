==========
Civil Code
==========

This generates a ReadTheDocs-theme website that displays the Civil Code of Peru. A toggle switch to
choose between dark mode and light mode is also avaiable on the website. 

Website
=======

You can look at the website at the link below.  

https://clcuc.gitlab.io/codigo-civil  

Building
========
.. https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html#inline-markup-and-special-characters-e-g-bold-italic-verbatim  

To build the website, ``git clone`` this repository and then, on your terminal, follow the commands
shown in the ``gitlab-ci.yml`` file. A virtual environment like ``virtualenv`` or ``conda`` is
recommended.  
