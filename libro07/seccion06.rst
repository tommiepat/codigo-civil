==============================================
SECCION SEXTA Responsabilidad Extracontractual
==============================================

TITULO UNICO
============

CAPITULO UNICO
##############

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1969.- Indemnización por daño moroso y culposo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Aquel que por dolo o culpa causa un daño a otro está obligado a indemnizarlo. El descargo por falta de dolo o culpa corresponde a su autor.

CONCORDANCIAS:     Ley N° 29316, Tercera Disp. Compl. Final

Artículo 1970.- Responsabilidad por riesgo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Aquel que mediante un bien riesgoso o peligroso, o por el ejercicio de una actividad riesgosa o peligrosa, causa un daño a otro, está obligado a repararlo.

CONCORDANCIAS:     Ley N° 29571, Art. 101 (Código de protección y defensa del consumidor)

Artículo 1971.- Inexistencia de responsabilidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No hay responsabilidad en los siguientes casos:

1.- En el ejercicio regular de un derecho.

2.- En legítima defensa de la propia persona o de otra o en salvaguarda de un bien propio o ajeno.

3.- En la pérdida, destrucción o deterioro de un bien por causa de la remoción de un peligro inminente, producidos en estado de necesidad, que no exceda lo indispensable para conjurar el peligro y siempre que haya notoria diferencia entre el bien sacrificado y el bien salvado. La prueba de la pérdida, destrucción o deterioro del bien es de cargo del liberado del peligro.

Artículo 1972.- Irresponsabilidad por caso fortuito o fuerza mayor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos del artículo 1970, el autor no está obligado a la reparación cuando el daño fue consecuencia de caso fortuito o fuerza mayor, de hecho determinante de tercero o de la imprudencia de quien padece el daño.

Artículo 1973.- Reducción judicial de la indemnización
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la imprudencia sólo hubiere concurrido en la producción del daño, la indemnización será reducida por el juez, según las circunstancias.

Artículo 1974.- Irresponsabilidad por estado de inconsciencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si una persona se halla, sin culpa, en estado de pérdida de conciencia, no es responsable por el daño que causa. Si la pérdida de conciencia es por obra de otra persona, ésta última es responsable por el daño que cause aquélla.

Artículo 1975.- Responsabilidad de incapaces con discernimiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La persona sujeta a incapacidad de ejercicio queda obligada por el daño que ocasione, siempre que haya actuado con discernimiento. El representante legal de la persona incapacitada es solidariamente responsable.(*)

(*) Artículo derogado por el Literal b) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

Artículo 1976.- Responsabilidad de representantes de incapaces sin discernimiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No hay responsabilidad por el daño causado por persona incapaz que haya actuado sin discernimiento, en cuyo caso responde su representante legal. (*)

(*) Artículo derogado por el Literal b) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

“Artículo 1976-A.- Responsabilidad de la persona con apoyo

La persona que cuenta con apoyos es responsable por sus decisiones, incluso de aquellas realizadas con dicho apoyo, teniendo derecho a repetir contra él. Las personas comprendidas en el artículo 44 numeral 9 no son responsables por las decisiones tomadas con apoyos designados judicialmente que hayan actuado con dolo o culpa.”(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

Artículo 1977.- Indemnización equitativa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la víctima no ha podido obtener reparación en el supuesto anterior, puede el juez, en vista de la situación económica de las partes, considerar una indemnización equitativa a cargo del autor directo.

Artículo 1978.- Responsabilidad por incitación y/o coautoría
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

También es responsable del daño aquel que incita o ayuda a causarlo. El grado de responsabilidad será determinado por el juez de acuerdo a las circunstancias.

Artículo 1979.- Responsabilidad por daño causado por animal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El dueño de un animal o aquel que lo tiene a su cuidado debe reparar el daño que éste cause, aunque se haya perdido o extraviado, a no ser que pruebe que el evento tuvo lugar por obra o causa de un tercero.

Artículo 1980.- Responsabilidad por caída de edificio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El dueño de un edificio es responsable del daño que origine su caída, si ésta ha provenido por falta de conservación o de construcción.

Artículo 1981.- Responsabilidad por daño del subordinado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Aquel que tenga a otro bajo sus órdenes responde por el daño causado por éste último, si ese daño se realizó en el ejercicio del cargo o en cumplimiento del servicio respectivo. El autor directo y el autor indirecto están sujetos a responsabilidad solidaria.

Artículo 1982.- Responsabilidad por denuncia calumniosa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Corresponde exigir indemnización de daños y perjuicios contra quien, a sabiendas de la falsedad de la imputación o de la ausencia de motivo razonable, denuncia ante autoridad competente a alguna persona, atribuyéndole la comisión de un hecho punible.

Artículo 1983.- Responsabilidad solidaria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si varios son responsables del daño, responderán solidariamente. Empero, aquel que pagó la totalidad de la indemnización puede repetir contra los otros, correspondiendo al juez fijar la proporción según la gravedad de la falta de cada uno de los participantes. Cuando no sea posible discriminar el grado de responsabilidad de cada uno, la repartición se hará por partes iguales.

Artículo 1984.- Daño moral
~~~~~~~~~~~~~~~~~~~~~~~~~~

El daño moral es indemnizado considerando su magnitud y el menoscabo producido a la víctima o a su familia.

Artículo 1985.- Contenido de la indemnización
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La indemnización comprende las consecuencias que deriven de la acción u omisión generadora del daño, incluyendo el lucro cesante, el daño a la persona y el daño moral, debiendo existir una relación de causalidad adecuada entre el hecho y el daño producido. El monto de la indemnización devenga intereses legales desde la fecha en que se produjo el daño.

Artículo 1986.- Nulidad de límites de la responsabilidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son nulos los convenios que excluyan o limiten anticipadamente la responsabilidad por dolo o culpa inexcusable.

Artículo 1987.- Responsabilidad del asegurador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción indemnizatoria puede ser dirigida contra el asegurador por el daño, quien responderá solidariamente con el responsable directo de éste.

Artículo 1988.- Determinación legal del daño sujeto a seguro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La ley determina el tipo de daño sujeto al régimen de seguro obligatorio, las personas que deben contratar las pólizas y la naturaleza, límites y demás características de tal seguro.

