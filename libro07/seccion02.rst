===================================
SECCION SEGUNDA Contratos Nominados
===================================

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

TITULO  I Compraventa
=====================

CAPITULO PRIMERO Disposiciones Generales
########################################

Artículo 1529.  Definición
~~~~~~~~~~~~~~~~~~~~~~~~~~

Por la compraventa el vendedor se obliga a transferir la propiedad de un bien al comprador y éste a pagar su precio en dinero.

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1530.  Gastos de entrega y transporte
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los gastos de entrega son de cargo del vendedor y los gastos de transporte a un lugar diferente del de cumplimiento son de cargo del comprador, salvo pacto distinto.

Artículo 1531.  Condiciones del contrato
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el precio de una transferencia se fija parte en dinero y parte en otro bien, se calificará el contrato de acuerdo con la intención manifiesta de los contratantes, independientemente de la denominación que se le dé.

Si no consta la intención de las partes, el contrato es de permuta cuando el valor del bien es igual o excede al del dinero; y de compraventa, si es menor.

CAPITULO SEGUNDO El Bien Materia de la Venta
############################################

Artículo 1532.  Bienes susceptibles de compra - venta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pueden venderse los bienes existentes o que puedan existir, siempre que sean determinados o susceptibles de determinación y cuya enajenación no esté prohibida por la ley.

Artículo 1533.  Perecimiento parcial del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si cuando se hizo la venta había perecido una parte del bien, el comprador tiene derecho a retractarse del contrato o a una rebaja por el menoscabo, en proporción al precio que se fijó por el todo.

Artículo 1534.  Compra venta de bien futuro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la venta de un bien que ambas partes saben que es futuro, el contrato está sujeto a la condición suspensiva de  que llegue a tener existencia.

Artículo 1535.  Riesgo de cuantía y calidad del bien futuro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el comprador asume el riesgo de la cuantía y calidad del bien futuro, el contrato queda igualmente sujeto a la condición suspensiva de que llegue a tener existencia.

Empero, si el bien llega a existir, el contrato producirá desde ese momento todos sus efectos, cualquiera sea su cuantía y calidad, y el comprador debe pagar íntegramente el precio.

Artículo 1536.  Compra-venta de esperanza incierta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos de los artículos 1534 y 1535, si el comprador asume el riesgo de la existencia del bien, el vendedor tiene derecho a la totalidad del precio aunque no llegue a existir.

Artículo 1537.  Compromiso de venta de bien ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El contrato por el cual una de las partes se compromete a obtener que la otra adquiera la propiedad de un bien que ambas saben que es ajeno, se rige por los artículos 1470, 1471 y 1472.

Artículo 1538.  Conversión del compromiso de venta de bien ajeno en compra - venta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el caso del artículo 1537, si la parte que se ha comprometido adquiere después la propiedad del bien, queda obligada en virtud de ese mismo contrato a transferir dicho bien al acreedor, sin que valga pacto en contrario.

Artículo 1539.  Rescisión del compromiso de venta de bien ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La venta de bien ajeno es rescindible a solicitud del comprador, salvo que hubiese sabido que no pertenecía al vendedor o cuando éste adquiera el bien, antes de la citación con la demanda.

Artículo 1540.  Compra-venta de bien parcialmente ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el caso del artículo 1539, si el bien es parcialmente ajeno, el comprador puede optar entre solicitar la rescisión del contrato o la reducción del precio.

Artículo 1541.  Efectos de la rescisión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos de rescisión a que se refieren los artículos 1539 y 1540, el vendedor debe restituir al comprador el precio recibido, y pagar la indemnización de daños y perjuicios sufridos.

Debe reembolsar igualmente los gastos, intereses y tributos del contrato efectivamente pagados por el comprador y todas las mejoras introducidas por éste.

Artículo 1542.  Adquisición de bienes en locales abiertos al público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los bienes muebles adquiridos en tiendas o locales abiertos al público no son reivindicables si son amparados con facturas o pólizas del vendedor. Queda a salvo el derecho del perjudicado para ejercitar las acciones civiles o penales que correspondan contra quien los vendió indebidamente.

CAPITULO TERCERO El Precio
##########################

Artículo 1543.  Nulidad por precio fijado unilateralmente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La compraventa es nula cuando la determinación del precio se deja al arbitrio de una de las partes.

Artículo 1544.  Determinación del precio por tercero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es válida la compraventa cuando se confía la determinación del precio a un tercero designado en el contrato o a designarse posteriormente, siendo de aplicación las reglas establecidas en los artículos 1407 y 1408.

Artículo 1545.  Determinación del precio en bolsa o mercado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es también válida la compraventa si se conviene que el precio sea el que tuviere el bien en bolsa o mercado, en determinado lugar y día.

Artículo 1546.  Reajuste automático del precio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es lícito que las partes fijen el precio con sujeción a lo dispuesto en el primer párrafo del artículo 1235.

Artículo 1547.  Fijación del precio en caso de silencio de las partes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la compraventa de bienes que el vendedor vende habitualmente, si las partes no han determinado el precio ni han convenido el modo de determinarlo, rige el precio normalmente establecido por el vendedor.

Si se trata de bienes que tienen precio de bolsa o de mercado, se presume, a falta de indicación expresa sobre el precio, que rige el de lugar en que debe realizarse la entrega.

Artículo 1548.  Precio determinado por peso neto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la compraventa en que el precio se fija por peso, a falta de convenio, se entiende que se refiere al peso neto.

CAPITULO CUARTO Obligaciones del Vendedor
#########################################

Artículo 1549.  Perfeccionamiento de transferencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es obligación esencial del vendedor perfeccionar la transferencia de la propiedad del bien.

Artículo 1550.  Estado del bien al momento de la entrega
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El bien debe ser entregado en el estado en que se encuentre en el momento de celebrarse el contrato, incluyendo sus accesorios.

Artículo 1551.  Entrega de documentos y títulos del bien vendido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El vendedor debe entregar los documentos y títulos relativos a la propiedad o al uso del bien vendido, salvo pacto distinto.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1552.  Oportunidad de la entrega del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El bien debe ser entregado inmediatamente después de celebrado el contrato, salvo la demora resultante de su naturaleza o de pacto distinto.

Artículo 1553.  Lugar de entrega del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A falta de estipulación, el bien debe ser entregado en el lugar en que se encuentre en el momento de celebrarse el contrato. Si el bien fuera incierto, la entrega se hará en el domicilio del vendedor, una vez que se realice su determinación.

Artículo 1554.  Entrega de frutos del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El vendedor responde ante el comprador por los frutos del bien, en caso de ser culpable de la demora de su entrega. Si no hay culpa, responde por los frutos sólo en caso de haberlos percibido.

Artículo 1555.  Demora en entrega de frutos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si al tiempo de celebrarse el contrato el comprador conocía el obstáculo que demora la entrega, no se aplica el artículo 1554 ni es responsable el vendedor de la indemnización por los daños y perjuicios.

Artículo 1556.  Resolución por falta de entrega
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando se resuelve la compraventa por falta de entrega, el vendedor debe reembolsar al comprador los tributos y gastos del contrato que hubiera pagado e indemnizarle los daños y perjuicios.

Artículo 1557.  Prórroga de plazos por demora en entrega del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Demorada la entrega del bien por el vendedor en un contrato cuyo precio debe pagarse a plazos, éstos se prorrogan por el tiempo de la demora.

CAPITULO QUINTO Obligaciones del Comprador
##########################################

Artículo 1558.  Tiempo, forma y lugar del pago del precio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comprador está obligado a pagar el precio en el momento, de la manera y en el lugar pactados.

A falta de convenio y salvo usos diversos, debe ser pagado al contado en el momento y lugar de la entrega del bien. Si el pago no puede hacerse en el lugar de la entrega del bien, se hará en el domicilio del comprador.

Artículo 1559.  Resolución por falta de pago del saldo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando se ha pagado parte del precio y en el contrato no se estipuló plazo para la cancelación del saldo, el vendedor puede ejercitar el derecho contemplado en el artículo 1429. Resuelto el contrato, el vendedor debe devolver la parte del precio pagado, deducidos los tributos y gastos del contrato.

Artículo 1560.  Resolución por falta de garantía por el saldo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se observará lo dispuesto en el artículo 1559 si el contrato se resuelve por no haberse otorgado, en el plazo convenido, la garantía debida por el saldo del precio.

Artículo 1561.  Incumplimiento de pago por armadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el precio debe pagarse por armadas en diversos plazos, si el comprador deja de pagar tres de ellas, sucesivas o no, el vendedor puede pedir la resolución del contrato o exigir al deudor el inmediato pago del saldo, dándose por vencidas las cuotas que estuvieren pendientes.

Artículo 1562.  Improcedencia de la acción resolutoria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el caso del artículo 1561, el vendedor pierde el derecho a optar por la resolución del contrato si se ha pagado más del cincuenta por ciento del precio. Es nulo todo pacto en contrario. (*)

(*) Artículo sustituido por el Artículo Único de la Ley Nº 27420, publicada el 07 febrero 2001, cuyo texto es el siguiente:

"Artículo 1562.- Improcedencia de la acción resolutoria

Las partes pueden convenir que el vendedor pierde el derecho a optar por la resolución del contrato si el comprador hubiese pagado determinada parte del precio, en cuyo caso el vendedor sólo podrá optar por exigir el pago del saldo.”

Artículo 1563.  Efectos de la resolución por falta de pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La resolución del contrato por incumplimiento del comprador da lugar a que el vendedor devuelva lo recibido, teniendo derecho a una compensación equitativa por el uso del bien y a la indemnización de los daños y perjuicios, salvo pacto en contrario.

Alternativamente, puede convenirse que el vendedor haga suyas, a título de indemnización, algunas de las armadas que haya recibido, aplicándose en este caso las disposiciones pertinentes sobre las obligaciones con cláusula penal.

Artículo 1564.  Resolución de la compraventa de bienes muebles no entregados
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la compraventa de bienes muebles no entregados al comprador, si éste no paga el precio, en todo o en parte, ni otorga la garantía a que se hubiere obligado, el vendedor puede disponer del bien. En tal caso, el contrato queda resuelto de pleno derecho.

Artículo 1565.  Oportunidad de la obligación de recibir el bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comprador está obligado a recibir el bien en el plazo fijado en el contrato, o en el que señalen los usos.

A falta de plazo convenido o de usos diversos, el comprador debe recibir el bien en el momento de la celebración del contrato.

Artículo 1566.  Compraventa de bienes muebles inscritos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los contratos de compraventa a plazos de bienes muebles inscritos en el registro correspondiente se rigen por la ley de la materia.

CAPITULO SEXTO Transferencia del Riesgo
#######################################

Artículo 1567.  Transferencia del riesgo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El riesgo de pérdida de bienes ciertos, no imputables a los contratantes, pasa al comprador en el momento de su entrega.

Artículo 1568.  Transferencia del riesgo antes de la entrega
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el caso del artículo 1567 el riesgo de pérdida pasa al comprador antes de la entrega de los bienes si, encontrándose a su disposición, no los recibe en el momento señalado en el contrato para la entrega.

Artículo 1569.  Transferencia del riesgo en la compraventa por peso, número o medida
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el caso de compraventa de bienes por peso, número o medida, se aplicará el artículo 1568 si, encontrándose los bienes a su disposición, el comprador no concurre en el momento señalado en el contrato o determinado por el vendedor para pesarlos, contarlos o medirlos, siempre que se encuentren a su disposición.

Artículo 1570.  Transferencia del riesgo por expedición del bien a lugar distinto de la entrega
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si a pedido del comprador, el vendedor expide el bien a lugar distinto a aquél en que debía ser entregado, el riesgo de pérdida pasa al comprador a partir del momento de su expedición.

CAPITULO SEPTIMO Venta a Satisfacción del Comprador, a Prueba y Sobre Muestra
#############################################################################

Artículo 1571.  Compraventa a satisfacción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La compraventa de bienes a satisfacción del comprador se perfecciona sólo en el momento en que éste declara su conformidad.

El comprador debe hacer su declaración dentro del plazo estipulado en el contrato o por los usos o, en su defecto, dentro de un plazo prudencial fijado por el vendedor.

Artículo 1572.  Compraventa a prueba
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La compraventa a prueba se considera hecha bajo la condición suspensiva de que el bien tenga las cualidades pactadas o sea idóneo para la finalidad a que está destinado.

La prueba debe realizarse en el plazo y según las condiciones establecidas en el contrato o por los usos.

Si no se realiza la prueba o el resultado de ésta no es comunicado al vendedor dentro del plazo indicado, la condición se tendrá por cumplida.

Artículo 1573.  Compraventa sobre muestra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la compraventa se hace sobre muestra, el comprador tiene derecho a la resolución del contrato si la calidad del bien no es conforme a la muestra o a la conocida en el comercio.

CAPITULO OCTAVO Compraventa Sobre Medida
########################################

Artículo 1574.  Compraventa por extensión o cabida
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la compraventa de un bien con la indicación de su extensión o cabida y por un precio en razón de un tanto por cada unidad de extensión o cabida, el vendedor está obligado a entregar al comprador la cantidad indicada en el contrato. Si ello no fuese posible, el comprador está obligado a pagar lo que se halle de más, y el vendedor a devolver el precio correspondiente a lo que se halle de menos.

Artículo 1575.  Rescisión de la compraventa sobre medida
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el exceso o falta en la extensión o cabida del bien vendido es mayor que un décimo de la indicada en el contrato, el comprador puede optar por su rescisión.

Artículo 1576.  Plazo para pago del exceso o devolución
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando en el caso del artículo 1574 el comprador no pueda pagar inmediatamente el precio del exceso que resultó, el vendedor está obligado a concederle un plazo no menor de treinta días para el pago.

Si no lo hace, el plazo será determinado por el juez, en la vía incidental, con arreglo a las circunstancias.

Igual regla se aplica, en su caso, para que el vendedor devuelva la diferencia resultante.

Artículo 1577.  Compraventa ad corpus
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el bien se vende fijando precio por el todo y no con arreglo a su extensión o cabida, aun cuando ésta se indique en el contrato, el comprador debe pagar la totalidad del precio a pesar de que se compruebe que la extensión o cabida real es diferente. (*) RECTIFICADO POR FE DE ERRATAS

Sin embargo, si se indicó en el contrato la extensión o cabida, y la real difiere de la señalada en más de una décima parte, el precio sufrirá la reducción o el aumento proporcional.

Artículo 1578.  Compraventa de bienes homogéneos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en la compraventa de varios bienes homogéneos por un solo y mismo precio, pero con indicación de sus respectivas extensiones o cabidas, se encuentra que la extensión o cabida es superior en alguno o algunos e inferior en otro u otros, se hará la compensación entre las faltas y los excesos, hasta el límite de su concurrencia.

Si el precio fue pactado por unidad de extensión o medida, el derecho al suplemento, o a la disminución del precio que resulta después de efectuada la compensación, se regula por los artículos 1574 a 1576.

Artículo 1579.  Caducidad de la acción rescisoria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho del vendedor al aumento del precio y el del comprador a su disminución, así como el derecho de este último de pedir la rescisión del contrato, caducan a los seis meses de la recepción del bien por el comprador.

CAPITULO NOVENO Compraventa sobre Documentos
############################################

Artículo 1580.  Compraventa sobre documentos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la compraventa sobre documentos, la entrega del bien queda sustituida por la de su título representativo y por los otros documentos exigidos por el contrato o, en su defecto, por los usos.

Artículo 1581.  Oportunidad y lugar de pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El pago del precio debe efectuarse en el momento y en el lugar de entrega de los documentos indicados en el artículo 1580, salvo pacto o uso distintos.

CAPITULO DECIMO Pactos que pueden integrar la Compraventa
#########################################################

SUB-CAPITULO I

Disposición General

Artículo 1582.  Pactos que no pueden integrar la compraventa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puede integrar la compraventa cualquier pacto lícito, con excepción de los siguientes, que son nulos:

1.- El pacto de mejor comprador, en virtud del cual puede rescindirse la compraventa por convenirse que, si hubiera quien dé más por el bien, lo devolverá el comprador.

2.- El pacto de preferencia, en virtud del cual se impone al comprador la obligación de ofrecer el bien al vendedor por el tanto que otro proponga, cuando pretenda enajenarlo.

SUB-CAPITULO II

Compraventa con Reserva de Propiedad

Artículo 1583. Compra venta con reserva de propiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la compraventa puede pactarse que el vendedor se reserva la propiedad del bien hasta que se haya pagado todo el precio o una parte determinada de él, aunque el bien haya sido entregado al comprador, quien asume el riesgo de su pérdida o deterioro desde el momento de la entrega.

El comprador adquiere automáticamente el derecho a la propiedad del bien con el pago del importe del precio convenido.

Artículo 1584. Oponibilidad del pacto de reserva de propiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La reserva de la propiedad es oponible a los acreedores del comprador sólo si consta por escrito que tenga fecha cierta anterior al embargo.

Si se trata de bienes inscritos, la reserva de la propiedad es oponible a terceros siempre que el pacto haya sido previamente registrado.

Artículo 1585. Reserva de propiedad en arrendamiento - venta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las disposiciones de los artículos 1583 y 1584 son aplicables a los contratos de arrendamiento en los que se convenga que, al final de los mismos, la propiedad del bien sea adquirida por el arrendatario por efecto del pago de la merced conductiva pactada. (*) RECTIFICADO POR FE DE ERRATAS

SUB-CAPITULO III

Pacto de Retroventa

Artículo 1586. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por la retroventa, el vendedor adquiere el derecho de resolver unilateralmente el contrato, sin necesidad de decisión judicial.

Artículo 1587. Nulidad de estipulaciones en el pacto de retroventa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nula la estipulación que impone al vendedor, como contrapartida de la resolución del contrato, la obligación de pagar al comprador una cantidad de dinero u otra ventaja para éste.

También es nula, en cuanto al exceso, la estipulación que obliga al vendedor a devolver, en caso de resolución del contrato, una suma adicional que no sea la destinada a conservar el valor adquisitivo del precio.

Artículo 1588. Plazo para ejercitar el derecho de resolución
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El plazo para ejercitar el derecho de resolución es de dos años tratándose de inmuebles y de un año en el caso de muebles, salvo que las partes estipulen un plazo menor.

El plazo se computa a partir de la celebración de la compraventa. Si las partes convienen un plazo mayor que el indicado en el primer párrafo de este artículo o prorrogan el plazo para que sea mayor de dos años o de un año, según el caso, el plazo o la prórroga se consideran reducidos al plazo legal.

El comprador tiene derecho a retener el bien hasta que el vendedor le reembolse las mejoras necesarias y útiles.

Artículo 1589. Retroventa en bienes indivisos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los que han vendido conjuntamente un bien indiviso con pacto de retroventa, y los herederos del que ha vendido con el mismo pacto, no pueden usar su derecho separadamente, sino conjuntamente.

Artículo 1590. Retroventa en venta separada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando los copropietarios de un bien indiviso hayan vendido separadamente sus cuotas en la copropiedad con pacto de retroventa, cada uno de ellos puede ejercitar, con la misma separación, el derecho de resolver el contrato por su respectiva participación.

Artículo 1591. Oponibilidad de la retroventa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El pacto de retroventa es oponible a terceros cuando aparece inscrito en el correspondiente registro.

CAPITULO DECIMO PRIMERO Derecho de Retracto
###########################################

Artículo 1592. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de retracto es el que la ley otorga a determinadas personas para subrogarse en el lugar del comprador y en todas las estipulaciones del contrato de compraventa.

El retrayente debe reembolsar al adquiriente el precio, los tributos y gastos pagados por éste y, en su caso, los intereses pactados.

Es improcedente el retracto en las ventas hechas por remate público.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1593. Retracto en dación en pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de retracto también procede en la dación en pago.

Artículo 1594. Procedencia del derecho de retracto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de retracto procede respecto de bienes muebles inscritos y de inmuebles.

Artículo 1595. Irrenunciabilidad e intrasmisibilidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es irrenunciable e intrasmisible por acto entre vivos el derecho de retracto.

Artículo 1596. Plazo para ejercer derecho de retracto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de retracto debe ejercitarse en el plazo de treinta días contados a partir de la comunicación de fecha cierta a la persona que goza de este derecho o del aviso inserto en el diario encargado de la publicación de los avisos judiciales del lugar de la situación de los bienes, salvo disposición distinta de las leyes especiales.  (*)

(*) Artículo modificado por el Artículo 5 del Decreto Ley N° 25940, publicado el 11 diciembre 1992, cuyo texto es el siguiente:

"Artículo 1596.- El derecho de retracto debe ejercerse dentro del plazo de treinta días contados a partir de la comunicación de fecha cierta a la persona que goza de este derecho.

Cuando su domicilio no sea conocido ni conocible, puede hacerse la comunicación mediante publicaciones en el diario encargado de los avisos judiciales y en otro de mayor circulación de la localidad, por tres veces con intervalo de cinco días entre cada aviso. En este caso, el plazo se cuenta desde el día siguiente al de la última publicación." (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por la Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Plazo para ejercer derecho de retracto

"Artículo 1596.- El derecho de retracto debe ejercerse dentro del plazo de treinta días contados a partir de la comunicación de fecha cierta a la persona que goza de este derecho.

Cuando su domicilio no sea conocido ni conocible, puede hacerse la comunicación mediante publicaciones en el diario encargado de los avisos judiciales y en otro de mayor circulación de la localidad, por tres veces con intervalo de cinco días entre cada aviso. En este caso, el plazo se cuenta desde el día siguiente al de la última publicación."

Artículo 1597. Plazo especial para ejercer derecho de retracto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la persona que goza de este derecho conoce la transferencia por un medio distinto de los indicados en el artículo 1596, el plazo señalado se cuenta a partir de la fecha de tal conocimiento. No rige, en su caso, la presunción del artículo 2012.

La carga de la prueba del conocimiento del acto corresponde al demandado. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por la Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Plazo especial para ejercer derecho de retracto

"Artículo 1597.- Si el retrayente conoce la transferencia por cualquier medio distinto del indicado en el artículo 1596, el plazo se cuenta a partir de la fecha de tal conocimiento. Para este caso, la presunción contenida en el  artículo 2012 sólo es oponible después de un año de la inscripción de la transferencia."

Artículo 1598. Garantía en retracto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el precio del bien fue pactado a plazos es obligatorio el otorgamiento de una garantía para el pago del precio pendiente, aunque en el contrato que da lugar al retracto no se hubiera convenido.

Artículo 1599. Titulares del derecho de retracto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tienen derecho de retracto:

1.-  El arrendatario, conforme a la ley de la materia. (*)

(*) Inciso derogado por el Inciso c) de la Primera Disposición Final del Decreto Legislativo N° 757, publicado el 13 noviembre 1991.

2.- El copropietario, en la venta a tercero de las porciones indivisas.

3.- El litigante, en caso de venta por el contrario del bien que se esté discutiendo judicialmente.

4.- El propietario, en la venta del usufructo y a la inversa.

5.- El propietario del suelo y el superficiario, en la venta de sus respectivos derechos.

6.- Los propietarios de predios urbanos divididos materialmente en partes, que no puedan ejercitar sus derechos de propietarios sin someter las demás partes del bien a servidumbres o a servicios que disminuyan su valor.

7.- El propietario de la tierra colindante, cuando se trate de la venta de una finca rústica cuya cabida no exceda de la unidad agrícola o ganadera mínima respectiva, o cuando aquélla y ésta reunidas no excedan de dicha unidad.

Artículo 1600. Orden de prelación de los retrayentes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si hay diversidad en  los títulos de dos o más que tengan derecho de retracto, el orden de preferencia será el indicado en el artículo 1599.

Artículo 1601. Retracto en enajenacion sucesiva
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando se hayan efectuado dos o más enajenaciones antes de que expire el plazo para ejercitar el retracto, este derecho se refiere a la primera enajenación sólo por el precio, tributos, gastos e intereses de la misma. Quedan sin efecto las otras enajenaciones.

TITULO  II Permuta
==================

CAPITULO UNICO
##############

Artículo 1602. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por la permuta los permutantes se obligan a transferirse recíprocamente la propiedad de bienes.

Artículo 1603. Reglas aplicables a la permuta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La permuta se rige por las disposiciones sobre compraventa, en lo que le sean aplicables.

TITULO  III Suministro
======================

CAPITULO UNICO
##############

Artículo 1604. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por el suministro, el suministrante se obliga a ejecutar en favor de otra persona prestaciones periódicas o continuadas de bienes.

Artículo 1605. Prueba y formalidad del contrato de suministro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La existencia y contenido del suministro pueden probarse por cualesquiera de los medios que permite la ley, pero si se hubiera celebrado por escrito, el mérito del instrumento respectivo prevalecerá sobre todos los otros medios probatorios.

Cuando el contrato se celebre a título de liberalidad debe formalizarse por escrito, bajo sanción de nulidad.

Artículo 1606. Volumen y periodicidad indeterminada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando no se haya fijado el volumen del suministro o su periodicidad, se entiende que se ha pactado teniendo en cuenta las necesidades del suministrado, determinadas al momento de la celebración del contrato.

Artículo 1607. Determinación del suministrado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si los contratantes determinan únicamente los límites mínimos y máximos para el suministro total o para las prestaciones singulares, corresponde al suministrado establecer dentro de estos límites el volumen de lo debido.

Artículo 1608. Pago del precio en el suministro periódico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el suministro periódico, el precio se abona en el acto de las prestaciones singulares y en proporción a cada una de ellas.

Artículo 1609. Determinación del precio en el suministro periódico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en el suministro periódico de entrega de bienes en propiedad, no se ha determinado el precio, serán aplicables las reglas pertinentes de la compraventa  y se tendrán en consideración el momento del vencimiento de las prestaciones singulares y el lugar en que éstas deben ser cumplidas.

Artículo 1610. Pago del precio en el suministro continuado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el suministro continuado, el precio se paga, a falta de pacto, de acuerdo con los usos del mercado.

Artículo 1611. Plazo para prestaciones singulares
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El plazo establecido para las prestaciones singulares se presume en interés de ambas partes.

Artículo 1612. Vencimiento de prestaciones singulares
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el beneficiario del suministro tiene la facultad de fijar el vencimiento de las prestaciones singulares, debe comunicar su fecha al suministrante con un aviso previo no menor de siete días.

Artículo 1613. Suministro de plazo indeterminado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la duración del suministro no se encuentra establecida, cada una de las partes puede separarse del contrato dando aviso previo en el plazo pactado, o, en su defecto, dentro de un plazo no menor de treinta días.

Artículo 1614. Pacto de preferencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de haberse pactado la cláusula de preferencia en favor del suministrante o del suministrado, la duración de la obligación no excederá de cinco años y se reduce a este límite si se ha fijado un plazo mayor.

Artículo 1615. Propuesta y ejercicio de la preferencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el caso previsto en el artículo 1614, la parte que tenga la preferencia deberá comunicar en forma indubitable a la otra las condiciones propuestas por terceros. El beneficiado por el pacto de preferencia, a su vez, está obligado a manifestar dentro del plazo obligatoriamente fijado, su decisión de hacer valer la preferencia.

Artículo 1616. Exclusividad del suministrante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando en el contrato de suministro se ha pactado la cláusula de exclusividad en favor del suministrante, el beneficiario del suministro no puede recibir de terceros prestaciones de la misma naturaleza, ni proveerlos con medios propios a la producción de las cosas que constituyen el objeto de la prestación.

Artículo 1617. Exclusividad del suministrado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la cláusula de exclusividad se pacta en favor del beneficiario del suministro, el suministrante no puede, directa ni indirectamente, efectuar prestaciones de igual naturaleza que aquellas que son materia del contrato, en ningún otro lugar.

Artículo 1618. Incumplimiento de promover la venta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El beneficiario del suministro que asume la obligación de promover la venta de los bienes que tiene en exclusividad responde de los daños y perjuicios si incumple esa obligación, aun cuando haya satisfecho el contrato respecto de la cantidad mínima pactada.

Artículo 1619. Incumplimiento de escasa importancia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el beneficiario del suministro no satisface la obligación que le corresponde y este incumplimiento es de escasa importancia, el suministrante no puede suspender la ejecución del contrato sin darle aviso previo.

Artículo 1620. Resolución del suministro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando alguna de las partes incumple las prestaciones singulares a que está obligada, la otra puede pedir la resolución del contrato si el incumplimiento tiene una importancia tal que disminuya la confianza en la exactitud de los sucesivos cumplimientos.

TITULO  IV Donación
===================

CAPITULO UNICO
##############

Artículo 1621. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por la donación el donante se obliga a transferir gratuitamente al donatario la propiedad de un bien.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1622. Donación mortis causa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La donación que ha de producir sus efectos por muerte del donante, se rige por las reglas establecidas para la sucesión testamentaria.

Artículo 1623. Donación verbal de bienes muebles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La donación de bienes muebles puede hacerse verbalmente cuando su valor no exceda de treinta veces el sueldo mínimo vital mensual vigente en el momento y lugar en que se celebre el contrato. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 26189, publicada el 22 mayo 1993, cuyo texto es el siguiente:

Donación verbal de bienes muebles

"Artículo 1623.- La donación de bienes muebles puede hacerse verbalmente, cuando su valor no exceda del 25% de la Unidad Impositiva Tributaria, vigente al momento en que se celebre el contrato."

Artículo 1624. Donación por escrito de bienes muebles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el valor de los bienes muebles excede del límite fijado en el artículo 1623 hasta un máximo de ciento cincuenta veces el sueldo mínimo vital mensual, la donación se hará por escrito de fecha cierta, bajo sanción de nulidad.

En el instrumento deben especificarse y valorizarse los bienes que se donan. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 26189, publicada el 22 mayo 1993, cuyo texto es el siguiente:

Donación por escrito de bienes muebles

"Artículo 1624.- Si el valor de los bienes muebles excede el límite fijado en el artículo 1623, la donación se deberá hacer por escrito de fecha cierta, bajo sanción de nulidad.
En el instrumento deben especificarse y valorizarse los bienes que se donen."

Artículo 1625. Donación de bienes inmuebles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La donación de bienes muebles cuyo valor sea superior a ciento cincuenta veces el sueldo mínimo vital mensual, así como la de bienes inmuebles, debe hacerse por escritura pública, con indicación individual de los bienes donados, de su valor y el de las cargas que ha de satisfacer el donatario, bajo sanción de nulidad. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 26189, publicada el 22 mayo 1993, cuyo texto es el siguiente:

Donación de bienes inmuebles

"Artículo 1625.- La donación de bienes inmuebles, debe hacerse por escritura pública, con indicación individual del inmueble o inmuebles donados, de su valor real y el de las cargas que ha de satisfacer el donatario, bajo sanción de nulidad."

Artículo 1626. Donación de muebles por nupcias
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La donación de bienes muebles con ocasión de bodas o acontecimientos similares no está sujeta a las formalidades establecidas por los artículos 1624 y 1625.

Artículo 1627. Compromiso de donar bien ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El contrato en virtud del cual una persona se obliga a obtener que otra adquiera gratuitamente la propiedad de un bien que ambos saben que es ajeno, se rige por los artículos 1470, 1471 y 1472.

Artículo 1628. Donación a favor de tutor o curador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La donación en favor de quien ha sido tutor o curador del donante está sujeta a la condición suspensiva de ser aprobadas las cuentas y pagado el saldo resultante de la administración.

Artículo 1629. Límites de la donación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nadie puede dar por vía de donación, más de lo que puede disponer por testamento.

La donación es inválida en todo lo que exceda de esta medida.

El exceso se regula por el valor que tengan o debían tener los bienes al momento de la muerte del donante.

Artículo 1630. Donación conjunta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando la donación se ha hecho a varias personas conjuntamente, se entenderá por partes iguales y no se dará entre ellas el derecho de acrecer.

Se exceptúan de esta disposición las donaciones hechas conjuntamente a marido y mujer, entre los cuales tendrá lugar el derecho de acrecer, si el donante no dispuso lo contrario.

Artículo 1631. Donación conjunta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puede establecerse la reversión sólo en favor del donante. La estipulada en favor de tercero es nula; pero no producirá la nulidad de la donación.

Artículo 1632. Renuncia tácita a la reversión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El asentimiento del donante a la enajenación de los bienes que constituyeron la donación determina la renuncia del derecho de reversión. El asentimiento del donante a la constitución de una garantía real por el donatario no importa renuncia del derecho de reversión sino en favor del acreedor.

Artículo 1633. Beneficio del donante empobrecido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El donante que ha desmejorado de fortuna sólo puede eximirse de entregar el bien donado en la parte necesaria para sus alimentos.

Artículo 1634. Invalidez de donación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Queda invalidada de pleno derecho la donación hecha por persona que no tenía hijos, si resulta vivo el hijo del donante que éste reputaba muerto.

La donación hecha por quien no tenía hijos al tiempo de celebrar el contrato, no queda invalidada si éstos sobrevinieren, salvo que expresamente estuviese establecida esta condición.

Artículo 1635. Efectos de la invalidación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Invalidada la donación se restituye al donante el bien donado, o su valor de reposición si el donatario lo hubiese enajenado o no pudiese ser restituido. (*) RECTIFICADO POR FE DE ERRATAS

Si el bien donado se halla gravado, el donante libera el gravamen pagando la cantidad que corresponda y se subroga en los derechos del acreedor.

Artículo 1636. Excepción a invalidez de pleno derecho
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No queda invalidada de pleno derecho la donación en el caso del artículo 1634 cuando el valor del bien donado no exceda de la décima parte de los bienes que tuvo el donante al tiempo de hacer la donación. En este caso, es necesario que el donante la declare sin efecto.

Artículo 1637. Revocación de donación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El donante puede revocar la donación por las mismas causas de indignidad para suceder y de desheredación.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1638. Intrasmisibilidad de la revocación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No pasa a los herederos la facultad de revocar la donación.

Artículo 1639. Caducidad de la revocación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La facultad de revocar la donación caduca a los seis meses desde que sobrevino alguna de las causas del artículo 1637.

Artículo 1640. Comunicación de la revocación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No produce efecto la revocación si dentro de sesenta días de hecha por el donante, no se comunica en forma indubitable al donatario o a sus herederos.

Artículo 1641. Contradicción de la revocación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El donatario o sus herederos pueden contradecir las causas de la revocación para que judicialmente se decida sobre el mérito de ellas. Quedará consumada la revocación que no fuese contradicha dentro de sesenta días después de comunicada en forma indubitable al donatario o a sus herederos.

Artículo 1642. Invalidez de donación remuneratoria o sujeta a cargo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el caso de donaciones remuneratorias o sujetas a cargo, su invalidación o revocación determina la obligación del donante de abonar al donatario el valor del servicio prestado o del cargo satisfecho.

Artículo 1643. Frutos por revocación o invalidación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los frutos de las donaciones revocadas pertenecen al donante desde que se comunica en forma indubitable la revocación; y en caso de invalidación de pleno derecho, desde que se cita con la demanda de restitución del bien donado.

Artículo 1644. Caducidad de la donación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Caduca la donación si el donatario ocasiona intencionalmente la muerte del donante.

Artículo 1645. Donación inoficiosa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si las donaciones exceden la porción disponible de la herencia, se suprimen o reducen en cuanto al exceso las de fecha más reciente, o a prorrata, si fueran de la misma fecha.

Artículo 1646. Donación por matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La donación hecha por razón de matrimonio está sujeta a la condición de que se celebre el acto.

Artículo 1647. Irrevocabilidad de donación por matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La donación a que se refiere el artículo 1646 no es revocable por causa de ingratitud.

TITULO  V Mutuo
===============

CAPITULO UNICO
##############

Artículo 1648. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por el mutuo, el mutuante se obliga a entregar al mutuatario una determinada cantidad de dinero o de bienes consumibles, a cambio de que se le devuelvan otros de la misma especie, calidad o cantidad.

Artículo 1649. Prueba y formalidad del mutuo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La existencia y contenido del mutuo se rigen por lo dispuesto en la primera parte del artículo 1605.

Artículo 1650. Mutuo entre cónyuges
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mutuo entre cónyuges constará por escritura pública, bajo sanción de nulidad, cuando su valor exceda el límite previsto por el artículo 1625.

Artículo 1651. Mutuo de representantes de incapaces o ausentes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los representantes de incapaces o ausentes, para celebrar mutuo en representación de las personas cuyos bienes administran, deben observar lo dispuesto en el artículo 1307. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 1652. Mutuo de incapaces o ausentes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el caso del artículo 1651, no será necesaria la intervención de los representantes o el cumplimiento de las formalidades de la transacción, según el caso, cuando el valor del bien mutuado no exceda diez veces el sueldo mínimo vital mensual. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 1653. Entrega de bien mutado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mutuante está obligado a efectuar la entrega en la oportunidad convenida y, en su defecto, al momento de celebrarse el contrato.

Artículo 1654. Efecto de la entrega
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Con la entrega del bien mutuado se desplaza la propiedad al mutuatario y desde este instante le corresponde la mejora, el deterioro o destrucción que sobrevengan.

Artículo 1655. Presunción del buen estado del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Recibido el bien por el mutuatario, se presume que se halla en estado de servir para el uso a que se destinó.

Artículo 1656. Plazo legal de devolución
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando no se ha fijado plazo para la devolución ni éste resulta de las circunstancias, se entiende que es de treinta días contados desde la entrega.

Artículo 1657. Plazo judicial de devolución
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se ha convenido que el mutuatario pague sólo cuando pueda hacerlo o tenga los medios, el plazo será fijado por el juez atendiendo las circunstancias y siguiendo el procedimiento establecido para el juicio de menor cuantía.

Artículo 1658. Pago anticipado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se conviene que el mutuatario no abone intereses u otra contraprestación al mutuante, aquél puede efectuar el pago antes del plazo estipulado.

Artículo 1659. Lugar de cumplimiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La entrega de lo que se presta y su devolución se harán en el lugar convenido o, en su defecto, en el que se acostumbre hacerlo.

Artículo 1660. Lugar de cumplimiento a falta de convenio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando no se ha convenido lugar ni exista costumbre, la entrega se hará en el sitio en que se encuentre el bien y la devolución en el domicilio del mutuatario.

Artículo 1661. Pago por imposibilidad de devolver el bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el mutuatario no pudiese devolver bien similar en especie, cantidad y calidad al que recibió, satisfará su prestación pagando el valor que tenía al momento y lugar en que debió hacerse el pago.

Artículo 1662. Pago previa evaluación del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en el caso del artículo 1661, fueran evaluados los bienes al momento de celebración del contrato, el mutuatario está obligado a satisfacer el valor que se les dio, aunque valgan más o menos al momento del pago.

Artículo 1663. Pago de intereses
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mutuatario debe abonar intereses al mutuante, salvo pacto distinto.

Artículo 1664. Usura encubierta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en el mutuo se declara recibida mayor cantidad que la verdaderamente entregada, el contrato se entiende celebrado por esta última, quedando sin efecto en cuanto al exceso.

Artículo 1665. Falso mutuo
~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando se presta una cantidad de dinero que debe devolverse en mercaderías o viceversa, el contrato es de compraventa.

TITULO  VI Arrendamiento
========================

CAPITULO PRIMERO Disposiciones Generales
########################################

Artículo 1666. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por el arrendamiento el arrendador se obliga a ceder temporalmente al arrendatario el uso de un bien por cierta renta convenida.

Artículo 1667. Facultad de arrendar bienes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puede dar en arrendamiento el que tenga esta facultad respecto de los bienes que administra.

Artículo 1668. Personas impedidas de arrendar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No puede tomar en arrendamiento:

1.- El administrador, los bienes que administra.
2.- Aquel que por ley está impedido.

Artículo 1669. Arrendamiento de bien indiviso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El copropietario de un bien indiviso no puede arrendarlo sin consentimiento de los demás partícipes. Sin embargo, si lo hace, el arrendamiento es válido si los demás copropietarios lo ratifican expresa o tácitamente.

Artículo 1670. Prelación entre arrendatarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando se arrienda un mismo bien a dos o más personas, se prefiere al arrendatario de buena fe cuyo título ha sido primeramente inscrito o, en defecto de inscripción, al que ha empezado a poseerlo. Si ninguno ha empezado a poseerlo, será preferido el arrendatario cuyo título sea de fecha anterior, salvo que el de alguno conste de documento de fecha cierta.

Artículo 1671. Arrendamiento de bien ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el arrendatario sabía que el bien era ajeno, el contrato se rige por lo dispuesto en los artículos 1470, 1471 y 1472.

Artículo 1672. Prohibición de arrendatarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El arrendador no puede realizar en el bien innovaciones que disminuyan el uso por parte del arrendatario.

Artículo 1673. Reparaciones de bien arrendado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en el curso del arrendamiento el bien requiere reparaciones que no pueden diferirse hasta el fin del contrato, el arrendatario debe tolerarlas aun cuando importen privación del uso de una parte de él.

Artículo 1674. Resolución o rebaja de renta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando para reparar el bien se impide al arrendatario que use una parte de él, éste tiene derecho a dar por resuelto el contrato o a la rebaja en la renta proporcional al tiempo y a la parte que no utiliza.

Artículo 1675. Restitución de bien mueble arrendado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El bien mueble arrendado se debe restituir en el lugar en que fue entregado, salvo pacto distinto.

Artículo 1676. Pago de renta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El pago de la renta puede pactarse por períodos vencidos o adelantados. A falta de estipulación, se entiende que se ha convenido por períodos vencidos.

Artículo 1677. Arrendamiento financiero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El contrato de arrendamiento financiero se rige por su legislación especial y, supletoriamente, por el presente título y los artículos 1419 a 1425, en cuanto sean aplicables.

CAPITULO SEGUNDO Obligaciones del Arrendador
############################################

Artículo 1678. Obligación de entregar el bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El arrendador está obligado a entregar al arrendatario el bien arrendado con todos sus accesorios, en el plazo, lugar y estado convenidos.

Si no se indica en el contrato el tiempo ni el lugar de la entrega, debe realizarse inmediatamente donde se  celebró, salvo que por costumbre deba efectuarse en otro lugar o época.

Artículo 1679. Presunción de buen estado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Entregado el bien al arrendatario, se presume que se halla en estado de servir y con todo lo necesario para su uso.

Artículo 1680. Obligaciones adicionales al arrendador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

También está obligado el arrendador:

1.- A mantener al arrendatario en el uso del bien durante el plazo del contrato y a conservarlo en buen estado para el fin del arrendamiento.

2.- A realizar durante el arrendamiento todas las reparaciones necesarias, salvo pacto distinto.

CAPITULO TERCERO Obligaciones del Arrendatario
##############################################

Artículo 1681. Obligaciones del arrendatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El arrendatario está obligado:

1.- A recibir el bien, cuidarlo diligentemente y usarlo para el destino que se le concedió en el contrato o al que pueda presumirse de las circunstancias.

2.- A pagar puntualmente la renta en el plazo y lugar convenidos y, a falta de convenio, cada mes, en su domicilio.

3.- A pagar puntualmente los servicios públicos suministrados en beneficio del bien, con sujeción a las normas que los regulan.
4.- A dar aviso inmediato al arrendador de cualquier usurpación, perturbación o imposición de servidumbre que se intente contra el bien.

5.- A permitir al arrendador que inspeccione por causa justificada el bien, previo aviso de siete días.

6.- A efectuar las reparaciones que le correspondan conforme a la ley o al contrato.

7.- A no hacer uso imprudente del bien o contrario al orden público o a las buenas costumbres.

8.- A no introducir cambios ni modificaciones en el bien, sin asentimiento del arrendador.

9.- A no subarrendar el bien, total o parcialmente, ni ceder el contrato, sin asentimiento escrito del arrendador.

10.- A devolver el bien al arrendador al vencerse el plazo del contrato en el estado en que lo recibió, sin más deterioro que el de su uso ordinario.

11.- A cumplir las demás obligaciones que establezca la ley o el contrato.

Artículo 1682. Reparación por arrendatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El arrendatario está obligado a dar aviso inmediato al arrendador de las reparaciones que haya que efectuar, bajo responsabilidad por los daños y perjuicios resultantes.

Si se trata de reparaciones urgentes, el arrendatario debe realizarlas directamente con derecho a reembolso, siempre que avise al mismo tiempo al arrendador.

En los demás casos, los gastos de conservación y de mantenimiento ordinario son de cargo del arrendatario, salvo pacto distinto.

Artículo 1683. Responsabilidad por pérdida y deterioro del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El arrendatario es responsable por la pérdida y el deterioro del bien que ocurran en el curso del arrendamiento, aun cuando deriven de incendio, si no prueba que han ocurrido por causa no imputable a él.

Es también responsable por la pérdida y el deterioro ocasionados por causas imputables a las personas que ha admitido, aunque sea temporalmente, al uso del bien.

Artículo 1684. Pérdida y deterioro de bienes asegurados
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el bien destruido o deteriorado por incendio había sido asegurado por el arrendador o por cuenta de éste, la responsabilidad del arrendatario frente al arrendador se limita a la diferencia entre la indemnización abonada o por abonar por el asegurador y el daño efectivo.

Si se trata del bien valorizado y el seguro se ha fijado en una cantidad igual a la tasación, no hay responsabilidad del arrendatario frente al arrendador, si éste es indemnizado por el asegurador.

Quedan a salvo, en todo caso, las normas concernientes al derecho de subrogación del asegurador.

Artículo 1685. Pérdida y deterioro en pluralidad de arrendatarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si son varios los arrendatarios, todos son responsables por la pérdida o deterioro del bien en proporción al valor de la parte que ocupan, salvo que se pruebe que el siniestro comenzó en la habitación o parte del inmueble arrendado a uno de ellos, quien, en tal caso, será el único responsable.

Artículo 1686. Responsabilidad del arrendador ocupante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el arrendador ocupa alguna parte del predio, será considerado como arrendatario, respecto a la responsabilidad a que se refiere el artículo 1685.

CAPITULO CUARTO Duración del Arrendamiento
##########################################

Artículo 1687. Duración del arrendamiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El arrendamiento puede ser de duración determinada o indeterminada.

Artículo 1688. Plazo máximo de arrendamiento de duración determinada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El plazo del arrendamiento de duración determinada no puede  exceder de diez años.

Cuando el bien arrendado pertenece a entidades públicas o a incapaces el plazo no puede ser mayor de seis años.

Todo plazo o prórroga que exceda de los términos señalados se entiende reducido a dichos plazos.

Artículo 1689. Presunciones del arrendamiento de duración determinada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A falta de acuerdo expreso, se presume que el arrendamiento es de duración determinada en los siguientes casos y por los períodos que se indican:

1. Cuando el arrendamiento tenga una finalidad específica, se entiende pactado por el tiempo necesario para llevarla a cabo.

2. Si se trata de predios ubicados en lugares de temporada, el plazo de arrendamiento será el de una temporada.

Artículo 1690. Arrendamiento de duración indeterminada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El arrendamiento de duración indeterminada se reputa por meses u otro período, según se pague la renta.

Artículo 1691. Períodos forzosos y voluntarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El arrendamiento puede ser celebrado por períodos forzosos y períodos voluntarios, pudiendo ser éstos en favor de una o ambas partes.

CAPITULO QUINTO Subarrendamiento y Cesión del Arrendamiento
###########################################################

Artículo 1692. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

El subarrendamiento es el arrendamiento total o parcial del bien arrendado que celebra el arrendatario en favor de un tercero, a cambio de una renta, con asentimiento escrito del arrendador.

Artículo 1693. Obligación solidaria de las partes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tanto el subarrendatario como el arrendatario están obligados solidariamente ante el arrendador por las obligaciones asumidas por el arrendatario.

Artículo 1694. Accesoriedad del subarrendamiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A la conclusión del arrendamiento se extinguen los subarrendamientos cuyos plazos no han vencido, dejándose a salvo el derecho del subarrendatario para exigir del arrendatario la indemnización correspondiente.

Artículo 1695. Subsistencia del arrendamiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El subarrendamiento no termina si el arrendamiento cesa por consolidación en la persona del arrendatario y del arrendador.

Artículo 1696. Cesión del arrendamiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La cesión del arrendamiento constituye la trasmisión de los derechos y obligaciones del arrendatario en favor de un tercero que lo sustituye y se rige por las reglas de la cesión de posición contractual.

CAPITULO SEXTO Resolución del Arrendamiento
###########################################

Artículo 1697. Causales de resolución
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El contrato de arrendamiento puede resolverse:

1.- Si el arrendatario no ha pagado la renta del mes anterior y se vence otro mes y además quince días. Si la renta se pacta por períodos mayores, basta el vencimiento de un solo período y además quince días. Si el alquiler se conviene por períodos menores a un mes, basta que venzan tres períodos.

2.- En los casos previstos en el inciso 1, si el arrendatario necesitó que hubiese contra él sentencia para pagar todo o parte de la renta, y se vence con exceso de quince días el plazo siguiente sin que haya pagado la nueva renta devengada.

3.- Si el arrendatario da al bien destino diferente de aquél para el que se le concedió expresa o tácitamente, o permite algún acto contrario al orden público o a las buenas costumbres.

4.- Por subarrendar o ceder el arrendamiento contra pacto expreso, o sin asentimiento escrito del arrendador.

5.- Si el arrendador o el arrendatario no cumplen cualesquiera de sus obligaciones.

Artículo 1698. Resolución por falta de pago de la renta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La resolución por falta de pago de la renta se sujeta a lo pactado, pero en ningún caso procede, tratándose de casas-habitación comprendidas en leyes especiales, si no se han cumplido por lo menos dos mensualidades y quince días.

CAPITULO SEPTIMO Conclusión del Arrendamiento
#############################################

Artículo 1699. Fin de arrendamiento de duración determinada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El arrendamiento de duración determinada concluye al vencimiento del plazo establecido por las partes, sin que sea necesario aviso previo de ninguna de ellas.

Artículo 1700. Continuación de arrendamiento de duración determinada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Vencido el plazo del contrato, si el arrendatario permanece en el uso del bien arrendado, no se entiende que hay renovación tácita, sino la continuación del arrendamiento, bajo sus mismas estipulaciones, hasta que el arrendador solicite su devolución, la cual puede pedir en cualquier momento.

Artículo 1701. Conversión de los periodos voluntarios en forzosos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el arrendamiento cuya duración se pacta por períodos forzosos para ambas partes y voluntarios a opción de una de ellas, los períodos voluntarios se irán convirtiendo uno a uno en forzosos si la parte a la que se concedió la opción no avisa a la otra que el arrendamiento concluirá al finalizar los períodos forzosos o cada uno de los voluntarios.

El aviso a que se refiere el párrafo anterior debe cursarse con no menos de dos meses de anticipación al día del vencimiento del respectivo período, si se trata de inmuebles, y de no menos de un mes, en el caso de los demás bienes.

Artículo 1702. Períodos voluntarios para ambas partes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en el contrato se establece que los períodos sean voluntarios para ambas partes, basta que cualquiera de ellas dé a la otra el aviso prescrito en el artículo 1701 para que el arrendamiento concluya al finalizar los períodos forzosos.

Artículo 1703. Fin del arrendamiento de duración indeterminada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se pone fin a un arrendamiento de duración indeterminada dando aviso judicial o extrajudicial al otro contratante.

Artículo 1704. Exigibilidad de devolución del bien y cobro de penalidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Vencido el plazo del contrato o cursado el aviso de conclusión del arrendamiento, si el arrendatario no restituye el bien, el arrendador tiene derecho a exigir su devolución y a cobrar la penalidad convenida o, en su defecto, una prestación igual a la renta del período precedente, hasta su devolución efectiva. El cobro de cualquiera de ellas no importará la continuación del arrendamiento.

Artículo 1705. Causales de conclusión extrajudicial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Además concluye el arrendamiento, sin necesidad de declaración judicial, en los siguientes casos:

1.- Cuando el arrendador sea vencido en juicio sobre el derecho que tenía.

2.- Si es preciso para la conservación del bien que el arrendatario lo devuelva con el fin de repararlo.

3.- Por la destrucción total o pérdida del bien arrendado.

4.- En caso de expropiación.

5.- Si dentro de los noventa días de la muerte del arrendatario, sus herederos que usan el bien, comunican al arrendador que no continuarán el contrato.

Artículo 1706. Consignación del bien arrendado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En todo caso de conclusión del arrendamiento o teniendo el arrendatario  derecho para resolverlo, si pone el bien a disposición del arrendador y éste no puede o no quiere recibirlo, aquél podrá consignarlo.

Artículo 1707. Extinción de responsabilidad por consignación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Desde el día en que el arrendatario efectúe la consignación se extingue su responsabilidad por la renta, salvo que la impugnación a la consignación fuese declarada fundada.

Artículo 1708. Enajenación del bien arrendado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de enajenación del bien arrendado se procederá del siguiente modo:

1.- Si el arrendamiento estuviese inscrito, el adquirente deberá respetar el contrato, quedando sustituido desde el momento de su adquisición en todos los derechos y obligaciones del arrendador.

2.- Si el arrendamiento no ha sido inscrito, el adquirente puede darlo por concluido.
Excepcionalmente, el adquirente está obligado a respetar el arrendamiento, si asumió dicha obligación.

3.- Tratándose de bienes muebles, el adquirente no está obligado a respetar el contrato si recibió su posesión de buena fe.

Artículo 1709. Responsabilidad por bien enajenado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando concluya el arrendamiento por enajenación del bien arrendado, el arrendador queda obligado al pago de los daños y perjuicios irrogados al arrendatario.

Artículo 1710. Continuación del arrendamiento con herederos del arrendatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si dos o más herederos del arrendatario usan el bien, y la mitad o el mayor número de ellos no manifiesta su voluntad de extinguirlo, continúa el contrato para éstos, sin ninguna responsabilidad de los otros.

En tal caso, no subsisten las garantías que estaban constituidas en favor del arrendador. Este tiene, sin embargo, el derecho de exigir nuevas garantías; si no se le otorgan dentro de quince días, concluye el contrato.

Artículo 1711. Autorización para desocupar bien arrendado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para desocupar el bien el arrendatario debe previamente recabar autorización  escrita del arrendador o, en su defecto, de la autoridad respectiva.

Si el arrendatario desocupa el bien sin alguna de esas autorizaciones, será responsable:

1.- De la renta y de los pagos por los servicios a su cargo que se devenguen después de la desocupación hasta que el arrendador tome posesión del bien.

2.- De los daños y perjuicios correspondientes.

3.- De que un tercero se introduzca en él.

Artículo 1712. Arrendamiento especial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los contratos de arrendamiento regulados por leyes especiales se rigen supletoriamente por las normas de este título.

TITULO  VII Hospedaje
=====================

CAPITULO UNICO
##############

Artículo 1713. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por el hospedaje, el hospedante se obliga a prestar al huésped albergue y, adicionalmente, alimentación y otros servicios que contemplan la ley y los usos, a cambio de una retribución. Esta podrá ser fijada en forma de tarifa por la autoridad competente si se trata de hoteles, posadas u otros establecimientos similares.

Artículo 1714. Sujeción a normas reglamentarias y cláusulas generales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El hospedaje se sujeta además a las normas reglamentarias y a las cláusulas generales de contratación aprobadas por la autoridad competente.

Artículo 1715. Derechos del huésped
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El huésped tiene derecho a exigir del hospedante que la habitación presente las condiciones de aseo y funcionamiento de servicios normales y que los alimentos, en su caso, respondan a los requisitos de calidad e higiene adecuados.

Artículo 1716. Exhibición de tarifas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los establecimientos destinados a hospedaje exhibirán en lugar visible las tarifas y cláusulas generales de contratación que rigen este contrato.

Artículo 1717. Derecho de retención
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los equipajes y demás bienes entregados o introducidos por el huésped responden preferencialmente por el pago de la retribución del hospedaje y por los daños y perjuicios que aquél hubiese causado al establecimiento, pudiendo el hospedante retenerlos hasta su cancelación.

Artículo 1718. Responsabilidad del hospedante como depositario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El hospedante responde como depositario por el dinero, joyas, documentos y otros bienes recibidos en custodia del huésped y debe poner en su cuidado la diligencia ordinaria exigida por la naturaleza de la obligación y que corresponda a las circunstancias de las personas, del tiempo y del lugar.

Artículo 1719. Responsabilidad del hospedante sobre objetos de uso corriente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El hospedante responde igualmente  de los objetos de uso corriente  introducidos por el huésped, siempre que éste cumpla las prescripciones del aviso que estará fijado en lugar visible de las habitaciones.

La autoridad competente fijará el límite de la responsabilidad.

Artículo 1720. Declaración de objetos de uso común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El hospedante tiene derecho a solicitar del huésped, dentro de las veinticuatro horas de su ingreso, una declaración escrita de los objetos de uso común introducidos, así como a comprobar su exactitud.

Artículo 1721. Negativa a la custodia de bienes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El hospedante no puede negarse a recibir  en custodia o a que se introduzcan los bienes a que se refiere el artículo 1718, sin justos motivos. Se consideran tales, el excesivo valor de los bienes en relación con la importancia del establecimiento, así como su naturaleza en cuanto constituya obstáculo respecto a la capacidad del local.

Artículo 1722. Extensión de responsabilidad del hospedante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La responsabilidad del hospedante por la custodia de los bienes depositados o introducidos se extiende a los actos u omisiones de los familiares que trabajan con él y a sus dependientes.

Artículo 1723. Comunicación de sustracción, pérdida o deterioro de bienes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El huésped está obligado a comunicar al hospedante la sustracción, pérdida o deterioro de los bienes introducidos en el establecimiento tan pronto tenga conocimiento de ello. De no hacerlo, quedará excluida la responsabilidad del hospedante, salvo cuando tales hechos se produzcan por dolo o culpa inexcusable de éste último.

Artículo 1724. Liberación de responsabilidad del hospedante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El hospedante no tiene responsabilidad si prueba que la sustracción, pérdida o deterioro de los bienes introducidos por el huésped se debe a su culpa exclusiva o de quienes le visiten, acompañen o sean dependientes suyos o si tiene como causa la naturaleza o vicio de ellos.

Artículo 1725. Caducidad del crédito del hospedante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El crédito del hospedante caduca a los seis meses contados a partir del momento de la terminación del contrato.

Artículo 1726. Servicio de estacionamiento y similares
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El servicio adicional de estacionamiento de vehículos o similares, se rige por los artículos 1713 a 1725, en cuanto sean aplicables.

Artículo 1727. Extensión normativa de normas de hospedaje
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las disposiciones de los artículos 1713 a 1725 comprenden a los hospitales, clínicas y casas de salud o de reposo, establecimientos comerciales o de espectáculos públicos, balnearios, restaurantes, clubes, naves, aeronaves, coches-cama y similares, en lo que les sean aplicables.

TITULO  VIII Comodato
=====================

CAPITULO UNICO
##############

Artículo 1728. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por el comodato, el comodante se obliga a entregar gratuitamente al comodatario un bien no consumible, para que lo use por cierto tiempo o para cierto  fin y luego lo devuelva.

Artículo 1729. Comodato de bien consumible
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Hay comodato de un bien consumible sólo si es prestado a condición de no ser consumido.

Artículo 1730. Prueba del comodato
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La existencia y  contenido del comodato se rigen por lo dispuesto en la primera parte del artículo 1605.

Artículo 1731. Presunción del buen estado del bien recibido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se presume que el comodatario recibe el bien en buen estado de uso y conservación, salvo prueba en contrario.

Artículo 1732. Aumento, menoscabo y pérdida del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Corresponde al comodante el aumento y el menoscabo o pérdida del bien, salvo culpa del comodatario o pacto de satisfacer todo perjuicio.

Artículo 1733. Intrasmisibilidad del comodato
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las obligaciones y derechos que resulten del comodato no se trasmiten a los herederos del comodatario, salvo que el bien haya sido dado en comodato para una finalidad que no pueda suspenderse.

Artículo 1734. Prohibición de ceder uso del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comodatario no puede ceder el uso del bien a un tercero sin autorización  escrita del comodante, bajo sanción de nulidad.

Artículo 1735. Obligaciones del comodante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son obligaciones del comodante:

1.- Entregar el bien en el plazo convenido

2.- Comunicar oportunamente al comodatario si el bien adolece de algún vicio que conoce.

3.- No solicitar la devolución del bien antes del plazo estipulado y, en defecto de pacto, antes de haber servido al uso para el que fue dado en comodato, salvo el caso previsto en el artículo 1736.

4.- Pagar los gastos extraordinarios que hubiese hecho el comodatario para la conservación del bien.

Artículo 1736. Devolución anticipada del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el comodante necesita con urgencia imprevista el bien o acredita que existe peligro de deterioro o pérdida si continúa en poder del comodatario, puede solicitarle su devolución antes de cumplido el plazo o de  haber servido para el uso.

Artículo 1737. Comodato de duración indeterminada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando no se ha determinado la duración del contrato, el comodatario está obligado a restituir el bien cuando el comodante lo solicite.

Artículo 1738. Obligaciones del comodatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son obligaciones del comodatario:

1.- Custodiar y conservar el bien con la mayor diligencia y cuidado, siendo responsable de la pérdida o deterioro que no provenga de su  naturaleza o del uso ordinario.

2.- Emplear el bien para el uso determinado en el contrato o, en su  defecto, según la naturaleza del mismo y la costumbre, siendo responsable del deterioro o pérdida provenientes del abuso.

3.- Permitir que el comodante inspeccione el bien para establecer su estado de uso y conservación.

4.-  Pagar los gastos ordinarios indispensables que exija la conservación y uso del bien.

5.-  Devolver el bien en el plazo estipulado o, en su defecto, después del uso para el que fue dado en comodato.

Artículo 1739. Eximencia de responsabilidad por uso ordinario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comodatario no responde si el bien se deteriora o modifica por efecto del uso para el que ha sido entregado.

Artículo 1740. Gastos de recepción y restitución del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los gastos de recepción y restitución del bien corren por cuenta del comodatario.

Artículo 1741. Responsabilidad por uso distinto del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comodatario que emplea el bien para un uso distinto o por un plazo mayor del convenido es responsable de la pérdida o deterioro ocurridos por causa que no le sea imputable, salvo que pruebe que estos hechos se habrían producido aun cuando no lo hubiese usado diversamente o lo hubiese restituido en su oportunidad.

Artículo 1742. Responsabilidad por perecimiento del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comodatario debe pagar el valor del bien dado en comodato si perece por causa que no le es imputable, cuando hubiese podido evitarla sustituyéndolo con uno de su propiedad.

Artículo 1743. Responsabilidad por el deterioro y pérdida de bien tasado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el bien fue tasado al tiempo de celebrarse el contrato, su pérdida o deterioro es de cuenta del comodatario, aun cuando hubiera ocurrido por causa que no le sea imputable.

Artículo 1744. Lugar de devolución del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comodatario debe devolver el bien dado en comodato al comodante o a quien tenga derecho a recibirlo, en el lugar en que lo recibió.

Artículo 1745. Suspensión de la devolución del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comodatario no puede suspender la restitución alegando que el comodante no tiene derecho sobre el bien, salvo que haya sido perdido, hurtado o robado o que el comodatario sea designado depositario por mandato judicial.

Artículo 1746. Consignación del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el comodatario supone que se le ha dado en comodato un bien extraviado, hurtado o robado, debe consignarlo de inmediato y bajo responsabilidad, con citación del comodante y del presunto propietario, si lo conoce.

Artículo 1747. Suspensión de devolución
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comodatario está obligado a suspender la restitución del bien si se pretende utilizarlo para la comisión de una infracción penal.

En este caso, debe consignarlo de inmediato y bajo responsabilidad, con citación del comodante.

Artículo 1748. Derecho de retención
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comodatario  tiene derecho a retener el bien, sólo cuando no le hayan sido pagados los gastos extraordinarios a que se refiere el artículo 1735, inciso 4.

Artículo 1749. Enajenación del bien por herederos del comodatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si los herederos del comodatario hubiesen enajenado el bien sin tener conocimiento del comodato, el comodante puede exigir que le paguen su valor o le cedan los derechos que en virtud de la enajenación le corresponden, salvo que haya hecho uso de la acción reinvindicatoria.

Si los herederos hubiesen conocido del comodato, indemnizarán además los daños y perjuicios.

Artículo 1750. Pago por imposibilidad de restituir el bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando sea imposible devolver el bien, el comodatario pagará, a elección del comodante, otro de la misma especie y calidad, o su valor, de acuerdo con las circunstancias y lugar en que debía haberse restituido.

Artículo 1751. Hallazgo del bien dado en comodato
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pagado el bien dado en comodato por haberse perdido, si posteriormente lo halla el comodatario, no podrá obligar al comodante a recibirlo pero éste tendrá la facultad de recuperarlo, devolviendo al comodatario lo que recibió.

Si el hallazgo lo realiza el comodante, puede retenerlo devolviendo el bien o valor que recibió o, en su defecto, entregando el bien hallado al comodatario.

Si el bien fue hallado por un tercero, el comodante está facultado para reclamarlo y, una vez recuperado, devolverá al comodatario lo que éste le hubiese pagado.

Artículo 1752. Responsabilidad solidaria en pluralidad de comodatarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el bien se ha dado en comodato a dos o más personas para que lo usen al mismo tiempo, todas son responsables solidariamente.

Artículo 1753. Caducidad de la acción por deterioro o modificación del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción del comodante para reclamar por el deterioro o modificación del bien, cuando la causa sea imputable al comodatario, caduca a los seis meses de haberlo recuperado.

Artículo 1754. Caducidad de la acción de reintegro de gastos extraordinarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción del comodatario para que se le reintegren los gastos extraordinarios a que se refiere el artículo 1735, inciso 4, caduca a los seis meses contados desde que devolvió el bien.

TITULO  IX Prestación de Servicios
==================================

CAPITULO PRIMERO Disposiciones Generales
########################################

Artículo 1755. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por la prestación de servicios se conviene que éstos o su resultado sean proporcionados por el prestador al comitente.

Artículo 1756. Modalidades de las prestación de servicios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son modalidades de la prestación de servicios nominados:

a. La locación de servicios.
b. El contrato de obra.
c. El mandato.
d. El depósito.
e. El secuestro.

Artículo 1757. Contratos innominados de prestación de servicios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son también modalidades de la prestación de servicios, y les son aplicables las disposiciones contenidas en este capítulo, los contratos innominados de doy para que hagas y hago para que des.

Artículo 1758. Prestación de servicios entre ausentes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se presume la aceptación entre ausentes cuando los servicios materia del contrato constituyen la profesión habitual del destinatario de la oferta, o el ejercicio de su calidad oficial, o cuando los servicios sean públicamente anunciados, salvo que el destinatario haga conocer su excusa sin dilación.

Artículo 1759. Oportunidad de pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el servicio sea remunerado, la retribución se pagará después de prestado el servicio o aceptado su resultado, salvo cuando por convenio, por la naturaleza del contrato, o por la costumbre, deba pagarse por adelantado o periódicamente.

Artículo 1760. Límites de la prestación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que presta los servicios no puede exceder los límites del encargo.

Empero, puede apartarse de las instrucciones recibidas si llena el encargo de una manera más ventajosa que la expresada en el contrato o cuando sea razonable suponer que el comitente aprobaría su conducta si conociese ciertas circunstancias que no fue posible comunicarle en tiempo oportuno.

Artículo 1761. Aprobación tácita de excesos de la prestación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Informado el comitente del apartamiento de las instrucciones por el prestador de servicios, el silencio de aquél por tiempo superior al que tenía para pronunciarse, según los usos o, en su defecto, de acuerdo con la naturaleza del asunto, importa la aprobación del encargo.

Artículo 1762. Responsabilidad por prestación de servicios profesionales o técnicos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la prestación de servicios implica la solución de asuntos profesionales o de problemas técnicos de especial dificultad, el prestador de servicios no responde por los daños y perjuicios, sino en caso de dolo o culpa inexcusable.

Artículo 1763. Muerte o incapacidad del prestador de servicio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El contrato de prestación de servicios se extingue por muerte o incapacidad del prestador, salvo que la consideración de su persona no hubiese sido el motivo determinante del contrato.

CAPITULO SEGUNDO Locación de Servicios
######################################

Artículo 1764. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por la locación de servicios el locador se obliga, sin estar subordinado al comitente, a prestarle sus servicios por cierto tiempo o para un trabajo determinado, a cambio de una retribución.

PROCESOS CONSTITUCIONALES

Artículo 1765. Objeto
~~~~~~~~~~~~~~~~~~~~~

Pueden ser materia del contrato toda clase de servicios materiales e intelectuales.

Artículo 1766. Carácter personal del servicio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El locador debe prestar personalmente el servicio, pero puede valerse, bajo su propia dirección y responsabilidad, de auxiliares y sustitutos si la colaboración de otros está permitida por el contrato o por los usos y no es incompatible con la naturaleza de la prestación.

Artículo 1767. Determinación de la retribución
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si no se hubiera establecido la retribución del locador y no puede determinarse según las tarifas profesionales o los usos, será fijada en relación a la calidad, entidad y demás circunstancias de los servicios prestados.

Artículo 1768. Plazo máximo de locación de servicios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El plazo máximo de este contrato es de seis años si se trata de servicios profesionales y de tres años en el caso de otra clase de servicios. Si se pacta un plazo mayor, el límite máximo indicado sólo puede invocarse por el locador.

Artículo 1769. Conclusión anticipada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El locador puede poner fin a la prestación de servicios por justo motivo, antes del vencimiento del plazo estipulado, siempre que no cause perjuicio al comitente.

Tiene derecho al reembolso de los gastos efectuados y a la retribución de los servicios prestados.

Artículo 1770. Normas aplicables cuando el locador proporciona materiales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las disposiciones de los artículos 1764 a 1769, son aplicables cuando el locador proporciona los materiales, siempre que éstos no hayan sido predominantemente tomados en consideración.

En caso contrario, rigen las disposiciones sobre la compraventa.

CAPITULO TERCERO Contrato de Obra
#################################

Artículo 1771. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por el contrato de obra el contratista se obliga a hacer una obra determinada y el comitente a pagarle una retribución. (*) RECTIFICADO POR FE DE ERRATAS

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1772. Subcontrato de obra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El contratista no puede subcontratar íntegramente la realización de la obra, salvo autorización escrita del comitente.

La responsabilidad frente al comitente es solidaria entre el contratista y el subcontratista, respecto de la materia del subcontrato.

Artículo 1773. Obligación del comitente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los materiales necesarios para la ejecución de la obra deben ser proporcionados por el comitente, salvo costumbre o pacto distinto.

Artículo 1774. Obligacion del contratista
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El contratista está obligado:

1.- A hacer la obra en la forma y plazos convenidos en el contrato o, en su defecto, en el que se acostumbre.

2.- A dar inmediato aviso al comitente de los defectos del suelo o de la mala calidad de los materiales proporcionados por éste, si se descubren antes o en el curso de la obra y pueden comprometer su ejecución regular.

3.- A pagar los materiales que reciba, si éstos, por negligencia o impericia del contratista, quedan en imposibilidad de ser utilizados para la realización de la obra.

Artículo 1775. Prohibición de introducir variaciones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El contratista no puede introducir variaciones en las características convenidas de la obra sin la aprobación escrita del comitente.

Artículo 1776. Obra por ajuste alzado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El obligado a hacer una obra por ajuste alzado tiene derecho a compensación por las variaciones convenidas por escrito con el comitente, siempre que signifiquen mayor trabajo o aumento en el costo de la obra. (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 25291, publicada el  24 diciembre 1990, cuyo texto es el siguiente:

Obra por ajuste alzado

"Artículo 1776.- El obligado a hacer una obra por ajuste alzado tiene derecho a compensación por las variaciones convenidas por escrito con el comitente, siempre que signifiquen mayor trabajo o aumento en el costo de la obra. El comitente, a su vez, tiene derecho al ajuste compensatorio en caso de que dichas variaciones signifiquen menor trabajo o disminución en el costo de la obra."

Artículo 1777. Inspección de la obra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comitente tiene derecho a inspeccionar, por cuenta propia, la ejecución de la obra. Cuando en el curso de ella se compruebe que no se ejecuta conforme a lo convenido y según las reglas del arte, el comitente puede fijar un plazo adecuado para que el contratista se ajuste a tales reglas. Transcurrido el plazo establecido, el comitente puede solicitar la resolución del contrato, sin perjuicio del pago de la indemnización de daños y perjuicios.

Tratándose de un edificio o de un inmueble destinado por su naturaleza a larga duración, el inspector deber ser un técnico calificado y no haber participado en la elaboración de los estudios, planos y demás documentos necesarios para la ejecución de la obra.

Artículo 1778. Comprobación de la obra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comitente, antes de la recepción de la obra, tiene derecho a su comprobación. Si el comitente descuida proceder a ella sin justo motivo o bien no comunica su resultado dentro de un breve plazo, la obra se considera aceptada.

Artículo 1779. Aceptación tácita de la obra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se entiende aceptada la obra, si el comitente la recibe sin reserva, aun cuando no se haya procedido a su verificación.

Artículo 1780. Obra a satisfacción del comitente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando se estipula que la obra se ha de hacer a satisfacción del comitente, a falta de conformidad, se entiende reservada la aceptación a la pericia correspondiente. Todo pacto distinto es nulo.

Si la persona que debe aceptar la obra es un tercero, se estará a lo dispuesto en los artículos 1407 y 1408.

Artículo 1781. Obra por pieza o medida
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que se obliga a hacer una obra por pieza o medida, tiene derecho a la verificación por partes y, en tal caso, a que se le pague en proporción a la obra realizada.

El pago hace presumir la aceptación de la parte de la obra realizada.

No produce este efecto el desembolso de simples cantidades a cuenta. (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 25291, publicada el  24 diciembre 1990, cuyo texto es el siguiente:

Obra por pieza o medida

"Artículo 1781.- El que se obliga a hacer una obra por pieza o medida tiene derecho a la verificación por partes y, en tal caso, a que se le pague en proporción a la obra realizada.

El pago hace presumir la aceptación de la parte de la obra realizada.

No produce a este efecto el desembolso de simples cantidades a cuenta ni el pago de valorizaciones por avance de obra convenida."

Artículo 1782. Responsabilidad por diversidad y vicios de la obra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El contratista está obligado a responder por las diversidades y los vicios de la obra.

La recepción de la obra, sin reserva del comitente, descarga de responsabilidad al contratista por las diversidades y los vicios exteriores de ésta.

Artículo 1783. Acciones del comitente por vicios de la obra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comitente puede solicitar, a su elección, que las diversidades o los vicios de la obra se eliminen a costa del contratista, o bien que la retribución sea disminuida proporcionalmente, sin perjuicio del resarcimiento del daño.

Si las diversidades o los vicios son tales que hagan inútil la obra para la finalidad convenida, el comitente puede pedir la resolución del contrato y la indemnización por los daños y perjuicios.

El comitente debe comunicar al contratista las diversidades o los vicios dentro de los sesenta días de recepcionada la obra. Este plazo es de caducidad. La acción contra el contratista prescribe al año de construida la obra.

Artículo 1784. Responsabilidad del contratista por destrucción, vicios o ruina
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en el curso de los cinco años desde su aceptación la obra se destruye, total o parcialmente, o bien presenta evidente peligro de ruina o graves defectos por vicio de la construcción, el contratista es responsable ante el comitente o sus herederos, siempre que se le avise por escrito de fecha cierta dentro de los seis meses siguientes al descubrimiento. Todo pacto distinto es nulo.

El contratista es también responsable, en los casos indicados en el párrafo anterior, por la mala calidad de los materiales o por defecto del suelo, si es que hubiera suministrado los primeros o elaborado los estudios, planos y demás documentos necesarios para la ejecución de la obra.

El plazo para interponer la acción es de un año computado desde el día siguiente al aviso a que se refiere el primer párrafo.

Artículo 1785. Supuesto de ausencia de responsabilidad de contratista
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No existe responsabilidad del contratista en los casos a que se refiere el artículo 1784, si prueba que la obra se ejecutó de acuerdo a las reglas del arte y en estricta conformidad con las instrucciones de los profesionales que elaboraron los estudios,  planos y demás documentos necesarios para la realización de la obra, cuando ellos le son proporcionados por el comitente.

Artículo 1786. Facultad del comitente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comitente puede separarse del contrato, aun cuando se haya iniciado la ejecución de la obra, indemnizando al contratista por los trabajos realizados, los gastos soportados, los materiales preparados y lo que hubiere podido ganar si la obra hubiera sido concluida.

Artículo 1787. Obligación de pago a la muerte del contratista
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de terminarse el contrato por muerte del contratista, el comitente está obligado a pagar a los herederos hasta el límite en que le fueren útiles las obras realizadas, en proporción a la retribución pactada para la obra entera, los gastos soportados y los materiales preparados. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 1788. Pérdida de la obra sin culpa de las partes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la obra se pierde sin culpa de las partes, el contrato se resuelve de pleno derecho.

Si los materiales son suministrados por el comitente, el contratista está obligado a devolverle los que no se hubieren perdido y el comitente no está obligado a pagar la retribución de la parte de la obra no ejecutada.

Cuando se trate de un contrato por ajuste alzado o por unidad de medida, el contratista debe devolver la retribución proporcional correspondiente a la parte de la obra no ejecutada, pero no está obligado a reponerla o restaurarla. Por su parte, el comitente no está obligado a pagar la retribución proporcional de la parte de la obra no ejecutada.

Artículo 1789. Deterioro sustancial de la obra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la obra se deteriora sustancialmente por causa no imputable a las partes, es de aplicación el artículo 1788.

CAPITULO CUARTO Mandato
#######################

SUB-CAPITULO I

Disposiciones Generales

Artículo 1790. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por el mandato el mandatario se obliga a realizar uno o más actos jurídicos, por cuenta y en interés del mandante.

Artículo 1791. Presunción de onerosidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mandato se presume oneroso.

Si el monto de la retribución no ha sido pactado, se fija sobre la base de las tarifas del oficio o profesión del mandatario; a falta de éstas, por los usos; y, a falta de unas y otros, por el juez.

Artículo 1792. Extensión del mandato
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mandato comprende no sólo los actos para los cuales ha sido conferido, sino también aquellos que son necesarios para su cumplimiento.

El mandato general no comprende los actos que excedan de la administración ordinaria, si no están indicados expresamente.

SUB-CAPITULO II

Obligaciones del Mandatario

Artículo 1793. Obligaciones del mandatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mandatario está obligado:

1.- A practicar personalmente, salvo disposición distinta, los actos comprendidos en el mandato y sujetarse a las instrucciones del mandante.

2.-  A comunicar sin retardo al mandante la ejecución del mandato.

3.- A rendir cuentas de su actuación en la oportunidad fijada o cuando lo exija el mandante.

Artículo 1794. Responsabilidad del mandatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el mandatario utiliza en su beneficio o destina a otro fin el dinero o los bienes que ha de emplear para el cumplimiento del mandato o que deba entregar al mandante, está obligado a su restitución y al pago de la indemnización de daños y perjuicios.

Artículo 1795. Solidaridad en mandato conjunto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si son varios los mandatarios y están obligados a actuar conjuntamente, su responsabilidad es solidaria.

SUB-CAPITULO III

Obligaciones del Mandante

Artículo 1796. Obligaciones del mandante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mandante está obligado frente al mandatario:

1.- A facilitarle los medios necesarios para la ejecución del mandato y para el cumplimiento de las obligaciones que a tal fin haya contraído, salvo pacto distinto.

2.-  A pagarle la retribución que le corresponda y a hacerle provisión de ella según los usos.

3.- A reembolsarle los gastos efectuados para el desempeño del mandato, con los intereses legales desde el día en que fueron efectuados.

4.- A indemnizarle los daños y perjuicios sufridos como consecuencia del mandato.

Artículo 1797. Mora del mandante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mandatario puede abstenerse de ejecutar el mandato en tanto el mandante estuviera en mora frente a él en el cumplimiento de sus obligaciones.

Artículo 1798. Preferencia del mandatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mandatario tiene derecho a satisfacer los créditos que le corresponden según el artículo 1796 con los bienes que han sido materia de los negocios que ha concluido, con preferencia sobre su mandante y sobre los acreedores de éste.

Artículo 1799. Derecho de retención
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

También puede el mandatario retener los bienes que obtenga para el mandante en cumplimiento del mandato, mientras no cumpla aquél las obligaciones que le corresponden según los incisos 3 y 4 del artículo 1796.

Artículo 1800. Responsabilidad solidaria en el mandato colectivo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si son varios los mandantes, sus obligaciones frente al mandatario común son solidarias.

SUB-CAPITULO IV

Extinción del Mandato

Artículo 1801. Causales de extinción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mandato se extingue por:

1.- Ejecución total del mandato.

2.- Vencimiento del plazo del contrato.

3.- Muerte, interdicción o inhabilitación del mandante o del mandatario.

Artículo 1802. Validez de actos posteriores a la extinción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son válidos los actos que el mandatario realiza antes de conocer la extinción del mandato.

Artículo 1803. Mandato en interés del mandatario o de tercero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La muerte, interdicción o inhabilitación del mandante no extinguen el mandato cuando éste ha sido celebrado también en interés del mandatario o de un tercero.

Artículo 1804. Extinción del mandato por causas especiales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el mandato se extingue por muerte, interdicción o inhabilitación del mandatario, sus herederos o quien lo represente o asista, deben informar de inmediato al mandante y tomar entretanto las providencias exigidas por las circunstancias.

Artículo 1805. Extinción del mandato conjunto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando hubiera varios mandatarios con la obligación de actuar conjuntamente, el mandato se extingue para todos aun cuando la causa de la extinción concierna a uno solo de los mandatarios, salvo pacto en contrario.

SUB-CAPITULO V

Mandato con Representación

Artículo 1806. Normas aplicables a mandato con representación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el mandatario fuere representante por haber recibido poder para actuar en nombre del mandante, son también aplicables al mandato las normas del título III del Libro II.

En este caso, el mandatario debe actuar en nombre del mandante.

Artículo 1807. Presunción de representación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se presume que el mandato es con representación.

Artículo 1808. Extinción por revocación o renuncia de poder
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el mandato con representación, la revocación y la renuncia del poder implican la extinción del mandato.

SUB-CAPITULO VI

Mandato Sin Representación

Artículo 1809. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

El mandatario que actúa en nombre propio adquiere los derechos y asume las obligaciones derivados de los actos que celebra en interés y por cuenta del mandante, aun cuando los terceros hayan tenido conocimiento del mandato.

Artículo 1810. Transferencia de bienes adquiridos por el mandatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mandatario queda automáticamente obligado en virtud del mandato a transferir al mandante los bienes adquiridos en ejecución del contrato, quedando a salvo los derechos adquiridos por los terceros de buena fe.

Artículo 1811. Obligaciones asumidas por mandante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mandante está obligado a asumir las obligaciones contraídas por el mandatario en ejecución del mandato.

Artículo 1812. Responsabilidad de mandatario por incumplimiento de tercero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El mandatario no es responsable frente al mandante por la falta de cumplimiento de las obligaciones asumidas por las personas con quienes haya contratado, a no ser que al momento de la celebración del contrato conociese o debiese serle conocida su insolvencia, salvo pacto distinto.

Artículo 1813. Inafectación de bienes por deudas del mandatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los acreedores del mandatario no pueden hacer valer sus derechos sobre los bienes que éste hubiese adquirido en ejecución del mandato, siempre que conste de documento de fecha cierta anterior al requerimiento que efectúen los acreedores a fin de afectar dichos bienes con embargo u otras medidas.

CAPITULO QUINTO Depósito
########################

SUB-CAPITULO I

Depósito Voluntario

Artículo 1814. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por el depósito voluntario el depositario se obliga a recibir un bien para custodiarlo y devolverlo cuando lo solicite el depositante.

Artículo 1815. Depósito hecho a un incapaz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No hay acción civil por el depósito hecho a un incapaz, sino únicamente para recobrar lo que existe y para exigir el valor de lo consumido en provecho del depositario.

Artículo 1816. Prueba del depósito
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La existencia y el contenido del depósito se rigen por lo dispuesto en el primer párrafo del artículo 1605.

Artículo 1817. Cesión de depósito
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No puede cederse el depósito sin autorización escrita del depositante, bajo sanción de nulidad.

Artículo 1818. Presunción de gratuidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depósito se presume gratuito, salvo que, por pacto distinto o por la calidad profesional, por la actividad del depositario u otras circunstancias, se deduzca que es remunerado.

Si las partes no determinan el importe de la remuneración, ésta se regula según los usos del lugar de celebración del contrato.

Artículo 1819. Deber de custodia y conservación del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario debe poner en la custodia y conservación del bien, bajo responsabilidad, la diligencia ordinaria exigida por la naturaleza de la obligación y que corresponda a las circunstancias de las personas, del tiempo y del lugar.

Artículo 1820. Prohibición de usar el bien depositado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario no puede usar el bien en provecho propio ni de tercero, salvo autorización expresa del depositante o del juez. Si infringe esta prohibición, responde por el deterioro, pérdida o destrucción del bien, inclusive por caso fortuito o fuerza mayor.

Artículo 1821. Liberación de responsabilidad del depositario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No hay lugar a la responsabilidad prevista en el artículo 1820, si el depositario prueba que el deterioro, pérdida o destrucción se habrían producido aunque no hubiera hecho uso del bien.

Artículo 1822. Variación del modo de la custodia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando existan circunstancias urgentes, el depositario puede ejercitar la custodia de modo diverso al convenido, dando aviso de ello al depositante tan pronto sea posible.

Artículo 1823. Deterioro, pérdida o destrucción del bien sin culpa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No corren a cargo del depositario el deterioro, la pérdida o la destrucción del bien sobrevenidos sin culpa, salvo el caso previsto por el artículo 1824.

Artículo 1824. Deterioro, pérdida o destrucción por culpa o vicio aparente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario responderá por el deterioro, pérdida o destrucción del bien cuando se originen por su culpa, o cuando provengan de la naturaleza o vicio aparente del mismo, si no hizo lo necesario para evitarlos o remediarlos, dando además aviso al depositante en cuanto comenzaron a manifestarse.

Artículo 1825. Depósito reservado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La obligación de custodia y conservación del bien comprende la de respetar los sellos y cerraduras del bulto o cubierta del continente, salvo autorización del depositante. Se presume la culpa del depositario en caso de fractura o forzamiento.

Artículo 1826. Responsabilidad por violación de depósito reservado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se han roto los sellos o forzado las cerraduras por culpa del depositario, se estará a la declaración del depositante en cuanto al número y calidad de los bienes depositados, salvo prueba distinta actuada por el depositario.

Artículo 1827. Depósito secreto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario no debe violar el secreto de un depósito, ni podrá ser obligado a revelarlo, salvo mandato judicial.

Artículo 1828. Depósito de títulos valores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los depositarios de títulos valores, o documentos que devenguen intereses, están obligados a realizar su cobro en las épocas de sus vencimientos, así como a practicar los actos que sean necesarios para que dichos documentos conserven el valor y los derechos que les correspondan.

Artículo 1829. Depósito irregular
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el depositante permite que el depositario use el bien, el contrato se convierte en comodato o mutuo, según las circunstancias.

Artículo 1830. Devolución del bien depositado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario debe devolver el bien en cuanto lo solicite el depositante, aunque hubiese plazo convenido, salvo que el contrato haya sido celebrado en beneficio o interés del depositario o de un tercero.

Artículo 1831. Depósito en interés de un tercero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el bien es depositado también en interés de un tercero y éste comunica su adhesión a las partes contratantes, el depositario no puede liberarse restituyéndolo sin asentimiento del tercero.

Artículo 1832. Depósito a plazo indeterminado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en el contrato no se hubiere fijado plazo, el depositario puede efectuar la restitución del bien en cualquier momento, siempre que le avise al depositante con prudente anticipación para que lo reciba.

Artículo 1833. Devolución anticipada del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario que tenga justo motivo para no conservar el bien puede, antes del plazo señalado, restituirlo al depositante, y si éste se niega a recibirlo, debe consignarlo.

Artículo 1834. Persona a quien se debe restituir el bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario no debe restituir el bien sino a quien se lo confió o a la persona en cuyo nombre se hizo el depósito o a aquélla para quien se destinó al tiempo de celebrarse el contrato.

Artículo 1835. Incapacidad sobreviniente del depositario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el depositario deviene incapaz, la persona que asume la administración de sus bienes procederá de inmediato a la restitución del bien o lo consignará si el depositante no quiere recibirlo.

Artículo 1836. Consignación del bien de procedencia delictuosa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No debe efectuarse la restitución al depositante del bien si el depositario tiene conocimiento de su procedencia delictuosa, en cuyo caso deberá consignarlo de inmediato, con citación de aquél y bajo responsabilidad.

Artículo 1837. Estado del bien a la devolución
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario debe devolver el mismo bien recibido, en el estado en que se halle al momento de su restitución, con sus accesorios, frutos y rentas. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 1838. Depósito de bien divisible
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario devolverá a cada depositante parte del bien, siempre que éste sea divisible y si, al celebrarse el contrato, se hubiera indicado lo que a cada uno corresponde.

Artículo 1839. Devolución a pluralidad de depositantes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si son varios los depositantes y no se hubiera dispuesto a quién se hará la restitución, a falta de acuerdo deberá efectuarse según las modalidades que establezca el juez. (*) RECTIFICADO POR FE DE ERRATAS

La misma norma se aplica cuando a un solo depositante suceden varios herederos.

Artículo 1840. Devolución a pluralidad de depositarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si son varios los depositarios, el depositante pedirá la restitución al que tenga en su poder el bien, dando aviso inmediato a los demás.

Artículo 1841. Exoneración de restituir el bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario que pierde la posesión del bien como consecuencia de un hecho que no le es imputable, queda liberado de restituirlo, pero lo comunicará de inmediato al depositante, bajo responsabilidad. El depositante puede exigir lo que haya recuperado el depositario y se sustituye en sus derechos.

Artículo 1842. Obligación de devolver el bien sustituto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario que pierde sin culpa el bien y recibe otro en su lugar, está obligado a entregarlo al depositante.

Artículo 1843. Responsabilidad de herederos por enajenación del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El heredero del depositario que enajena el bien ignorando que estaba en depósito, sólo debe restituir lo que hubiese recibido o ceder sus derechos contra el adquiriente, en caso que el valor no le hubiese sido entregado.

En caso que el heredero conozca que el bien está en depósito, se sujeta a lo dispuesto en el párrafo anterior y responde, además, por los daños y perjuicios.

Artículo 1844. Devolución del bien por muerte del depositante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de muerte del depositante, el bien debe ser restituido a su heredero, legatario o albacea.

Artículo 1845. Devolución del bien al representado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depósito hecho por el administrador será devuelto a la persona que él representaba cuando se celebró el contrato, si hubiera terminado su administración o gestión.

Artículo 1846. Devolución del bien al representante del incapaz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el depósito hecho por un incapaz, el bien no puede ser devuelto sino a quien lo represente legalmente, aun cuando la incapacidad se haya producido con posterioridad al contrato.

Artículo 1847. Negativa a la devolución del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Salvo los casos previstos en los artículos 1836 y 1852, el depositario no puede negarse a la devolución del bien, y si lo hace, responde por los daños y perjuicios.

Le serán aplicables las mismas reglas si negare el depósito y éste le fuera probado en juicio.

Artículo 1848. Lugar de devolución del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La devolución del depósito se hace en el lugar en que estuvo en custodia.

Artículo 1849. Gastos de entrega y devolución
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los gastos de entrega y de devolución son de cuenta del depositante.

Artículo 1850. Bien de propiedad del depositario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario está liberado de toda obligación si resulta que el bien le pertenece y que el depositante no tiene derecho alguno sobre éste.

Artículo 1851. Reembolso de gastos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositante está obligado a reembolsar al depositario los gastos hechos en la custodia y conservación del bien depositado y a pagarle la indemnización correspondiente.

Artículo 1852. Derecho de retención
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario sólo puede retener el bien hasta que se le pague lo que se le debe por razón del contrato.

Artículo 1853. Depósitos regulados por leyes especiales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los depósitos en los bancos, cooperativas, financieras, almacenes generales de depósito, mutuales y otras instituciones análogas se rigen por las leyes especiales que los regulan.

SUB-CAPITULO II

Depósito Necesario

Artículo 1854. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

El depósito necesario es el que se hace en cumplimiento de una obligación legal o bajo el apremio de un hecho o situación imprevistos.

Artículo 1855. Obligatoriedad de recibir el depósito necesario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toda persona está obligada a recibir el depósito necesario, a menos que tenga impedimento físico u otra justificación.

Artículo 1856. Normas aplicables al depósito necesario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depósito necesario se rige supletoriamente por las reglas del depósito voluntario.

CAPITULO SEXTO Secuestro
########################

Artículo 1857. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por el secuestro, dos o más depositantes confían al depositario la custodia y conservación de un bien respecto del cual ha surgido controversia.

Artículo 1858. Formalidad del secuestro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El contrato debe constar por escrito, bajo sanción de nulidad.

Artículo 1859. Administración del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando la naturaleza del bien lo exija, el depositario tiene la obligación de administrarlo.

Artículo 1860. Conclusión de contratos celebrados por el depositario - administrador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cualquier contrato que celebre el depositario de conformidad con lo dispuesto en el artículo 1859, concluye de pleno derecho si, antes del vencimiento del plazo, se pusiere fin a la controversia.

Artículo 1861. Enajenación del bien secuestrado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de inminente peligro de pérdida o grave deterioro del bien, el depositario puede enajenarlo con autorización del juez y conocimiento de los depositantes.

Artículo 1862. Incapacidad o muerte del depositario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el depositario deviene incapaz o muere, los depositantes designarán a su reemplazante. En caso de discrepancia, la designación la hace el juez.

Artículo 1863. Solidaridad de los depositantes y derecho de retención
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los depositantes son solidariamente responsables por el pago de la retribución convenida, los gastos, costas y cualquier otra erogación que se derive del secuestro. El depositario puede retener el bien en tanto no le haya sido satisfecho su crédito.

Artículo 1864. Reclamo del bien por desposesión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario que sea desposeído del bien puede reclamarlo a quien lo tenga en su poder, incluyendo cualquiera de los depositantes que lo haya tomado sin consentimiento de los demás o sin mandato del juez.

Artículo 1865. Liberación del depositario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El depositario puede ser liberado sólo antes de la terminación de la controversia con el asentimiento de todos los depositantes o por causa justificada a criterio del juez.

Artículo 1866. Entrega del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El bien debe ser entregado, conforme al resultado de la controversia, a quien le corresponda.

Artículo 1867. Normas aplicables al secuestro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Rigen para el secuestro las normas del depósito voluntario, en cuanto sean aplicables.

TITULO  X Fianza
================

CAPITULO UNICO
##############

Artículo 1868. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por la fianza, el fiador se obliga frente al acreedor a cumplir determinada prestación, en garantía de una obligación ajena, si ésta no es cumplida por el deudor.

La fianza puede constituirse no sólo en favor del deudor sino de otro fiador.

Artículo 1869. Fianza sin intervención del deudor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se puede afianzar sin orden y aun sin noticia o contra la voluntad del deudor.

Artículo 1870. Fianza de personas jurídicas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los representantes de las personas jurídicas pueden otorgar fianza en nombre de sus representados, siempre que tengan poder suficiente.

Artículo 1871. Formalidad de la fianza
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La fianza debe constar por escrito, bajo sanción de nulidad.

Artículo 1872. Fianza de obligaciones futuras
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puede prestarse fianza en garantía de obligaciones futuras determinadas o determinables cuyo importe no sea aún conocido, pero no se puede reclamar contra el fiador hasta que la deuda sea líquida.

Es igualmente válida la fianza por una obligación condicional o a plazo.

Artículo 1873. Extensión de la obligación del fiador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sólo queda obligado el fiador por aquello a que expresamente se hubiese comprometido, no pudiendo exceder de lo que debe el deudor. Sin embargo, es válido que el fiador se obligue de un modo más eficaz que el deudor.

Artículo 1874. Exceso en la obligación del fiador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se produce el exceso a que se refiere el artículo 1873 la fianza vale dentro de los límites de la obligación principal.

Artículo 1875. Carácter accesorio de la fianza
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La fianza no puede existir sin una obligación válida, salvo que se haya constituido para asegurar una obligación anulable por defecto de capacidad personal.

Artículo 1876. Requisitos del fiador y sustitución de la garantía
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El obligado a dar fianza debe presentar a persona capaz de obligarse, que sea propietaria de bienes suficientes para cubrir la obligación y realizables dentro del territorio de la República. El fiador, en este caso, queda sujeto a la jurisdicción del juez del lugar donde debe cumplirse la obligación del deudor.

El obligado puede sustituir la fianza por prenda, hipoteca o anticresis, con aceptación del acreedor o aprobación del juez, salvo disposición distinta de la ley.

Artículo 1877. Insolvencia del fiador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el fiador ha devenido insolvente, el deudor debe reemplazarlo por otro que reúna los requisitos establecidos en el artículo 1876.

Si el deudor no puede dar otro fiador o no ofrece otra garantía idónea, el acreedor tiene derecho a exigir el cumplimiento inmediato de la obligación.

Artículo 1878. Extensión de la fianza
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La fianza, si no fuere limitada, se extiende a todos los accesorios de la obligación principal, así como a las costas del juicio contra el fiador, que se hubiesen devengado después de ser requerido para el pago.

Artículo 1879. Beneficio de excusión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El fiador no puede ser compelido a pagar al acreedor sin hacerse antes excusión de los bienes del deudor.

Artículo 1880. Oponibilidad de beneficio de excusión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para que el fiador pueda aprovecharse del beneficio de la excusión, debe oponerlo al acreedor luego que éste lo requiera para el pago y acreditar la existencia de bienes del deudor realizables dentro del territorio de la República, que sean suficientes para cubrir el importe de la obligación.

Artículo 1881. Responsabilidad del acreedor negligente en la excusión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acreedor negligente en la excusión de los bienes del deudor es responsable hasta donde ellos alcancen, de la insolvencia que resulte de su descuido.

Artículo 1882. Bienes no considerados en la excusión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No se tomarán en cuenta para la excusión, los bienes embargados, litigiosos, hipotecados, dados en anticresis o prendados, por deudas preferentes, en la parte que fuere necesario para su cumplimiento.

Si los bienes del deudor no producen más que un pago parcial de la deuda, el acreedor puede accionar contra el fiador por el saldo, incluyendo intereses y gastos.

Artículo 1883. Improcedencia del beneficio de excusión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La excusión no tiene lugar:

1.- Cuando el fiador ha renunciado expresamente a ella.

2.- Cuando se ha obligado solidariamente con el deudor.

3.- En caso de quiebra del deudor.

Artículo 1884. Riesgos para el acreedor negligente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acreedor negligente en la excusión de los bienes señalados por el fiador asume el riesgo de la pérdida o no persecución de estos bienes para los fines de la excusión.

Artículo 1885. Excepciones del fiador contra acreedor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El fiador puede oponer contra el acreedor todas las excepciones que corresponden al deudor, aunque éste haya renunciado a ellas, salvo las que sean inherentes a su persona.

Artículo 1886. Responsabilidad solidaria de fiadores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Siendo varios los fiadores de un mismo deudor y por una misma deuda y todos ellos se hubieran obligado a prestaciones iguales, cada uno responde por el íntegro de su obligación, salvo que se haya pactado el beneficio de la división.

Artículo 1887. Beneficio de división
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se ha estipulado el beneficio de la división, todo fiador que sea demandado para el pago de la deuda puede exigir que el acreedor reduzca la acción a la parte que le corresponde.

Si alguno de los fiadores es insolvente en el momento en que otro ha hecho valer el beneficio de la división, éste resulta obligado únicamente por esa insolvencia, en proporción a su cuota.

Artículo 1888. Beneficio de excusión del subfiador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El subfiador goza del beneficio de excusión, tanto respecto del fiador como del deudor.

Artículo 1889. Subrogación del fiador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El fiador que paga la deuda queda subrogado en los derechos que el acreedor tiene contra el deudor.

Si ha transigido con el acreedor, no puede pedir al deudor más de lo que realmente ha pagado. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 1890. Indemnización al fiador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La indemnización que debe serle pagada al fiador comprende:

1.- El total de lo pagado por el fiador.

2.- El interés legal desde que hubiese hecho saber el pago al deudor, aunque no lo produjese para el acreedor.

3.- Los gastos ocasionados al fiador, después de poner éste en conocimiento del deudor que ha sido requerido para el pago.

4.- Los daños y perjuicios, cuando procedan

Artículo 1891. Subrogación del fiador de codeudores solidarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si son varios los deudores obligados solidariamente, el fiador que ha garantizado por todos puede subrogarse contra cualquiera de ellos por el íntegro de lo pagado.

Artículo 1892. Improcedencia de la acción contra el deudor principal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El fiador no tiene acción contra el deudor si, por haber omitido comunicarle el pago efectuado, éste ha cancelado igualmente la deuda.

Lo expuesto es sin perjuicio del derecho de repetición del fiador contra el acreedor.

Artículo 1893. Repetición del fiador contra cofiadores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando varias personas otorgan fianza a un mismo deudor por la misma deuda, el fiador que haya pagado tiene acción contra los demás fiadores por su parte respectiva. Si alguno de ellos resulta insolvente, la parte de éste se distribuye proporcionalmente entre los demás.

Artículo 1894. Excepciones del deudor contra fiador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el fiador paga sin comunicarlo al deudor, éste puede hacer valer contra él todas las excepciones que hubiera podido oponer al acreedor.

Artículo 1895. Excepciones entre cofiadores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los cofiadores pueden oponer al que pagó las mismas excepciones que habrían correspondido al deudor contra el acreedor y que no sean inherentes al deudor.

Artículo 1896. Pago anticipado por fiador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El fiador que pagó anticipadamente la obligación principal no puede subrogarse contra el deudor sino después de vencido el plazo de aquélla.

Artículo 1897. Acciones del fiador antes del pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El fiador puede accionar contra el deudor, antes de haber pagado, para que éste lo releve o, en su defecto, preste garantía suficiente para asegurarle la satisfacción de sus eventuales derechos de subrogación en los casos siguientes:

1.-  Cuando el deudor es citado judicialmente para el pago.

2.- Cuando el deudor deviene insolvente o realiza actos tendientes a la disminución de su patrimonio.

3.-  Cuando el deudor se obligó a relevarlo de la fianza dentro de un plazo determinado y éste ha vencido.

4.-  Cuando la deuda se ha hecho exigible.

Artículo 1898. Fianza por plazo determinado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El fiador que se obliga por un plazo determinado, queda libre de responsabilidad si el acreedor no exige notarial o judicialmente el cumplimiento de la obligación dentro de los quince días siguientes a la expiración del plazo, o abandona la acción iniciada.(*)

(*) De conformidad con el Numeral 2.1 de la Segunda Disposición Complementaria Final del Decreto de Urgencia N° 036-2020, publicado el 10 abril 2020, se dispone la prórroga del plazo para la ejecución de las fianzas, cartas fianza y pólizas de caución emitidas en el territorio nacional a que se refiere el presente artículo u otra disposición de carácter específico referida al plazo, respectivamente, por el periodo de vigencia del Estado de Emergencia Nacional establecido por el Decreto Supremo Nº 044-2020-PCM y sus prórrogas, de aquellas garantías cuyo vencimiento formal se produzca desde la fecha de entrada en vigencia del citado Decreto de Urgencia hasta la culminación del Estado de Emergencia Nacional antes mencionado. El citado Decreto de Urgencia tiene vigencia hasta el 31 de diciembre de 2020.

(*) De conformidad con el Numeral 2.2 de la Segunda Disposición Complementaria Final del Decreto de Urgencia N° 036-2020, publicado el 10 abril 2020, se dispone que para el caso de aquellas garantías cuyo vencimiento formal se produjo desde el 11 de marzo de 2020 hasta el día anterior a la entrada en vigencia del citado Decreto de Urgencia, dispóngase de un nuevo plazo adicional para la ejecución de las fianzas, cartas fianza y pólizas de caución emitidas en el territorio nacional a que se refiere el presente artículo u otra disposición de carácter específico referida al plazo, respectivamente, por el periodo de vigencia del Estado de Emergencia Nacional establecido por el Decreto Supremo Nº 044-2020-PCM y sus prórrogas. El citado Decreto de Urgencia tiene vigencia hasta el 31 de diciembre de 2020.

Artículo 1899. Fianza sin plazo determinado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la fianza se ha otorgado sin plazo determinado, puede el fiador pedir al acreedor que cuando la deuda sea exigible, haga efectivo su derecho y demande al deudor. Si el acreedor no ejercita ese derecho en el plazo de treinta días después de requerido o si abandona el procedimiento, el fiador queda libre de su obligación.

Artículo 1900. Liberación del fiador por dación en pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Que liberado el fiador si el acreedor acepta del deudor un bien en pago de la deuda, aunque después lo pierda por evicción.

Artículo 1901. Extinción de la fianza por prórroga al deudor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La prórroga concedida al deudor por el acreedor sin el asentimiento del fiador extingue la fianza, salvo que éste la haya aceptado anticipadamente.

Artículo 1902. Liberación del fiador por imposibilidad de subrogación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El fiador queda liberado de su obligación siempre que por algún hecho del acreedor no pueda subrogarse.

Artículo 1903. Consolidación entre deudor y fiador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La consolidación del deudor con el fiador, no extingue la obligación del subfiador.

Artículo 1904. Documentos que no constituyen fianza
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las cartas de recomendación u otros documentos en que se asegure o certifique la probidad o solvencia de alguien no constituyen fianza.

Artículo 1905. Normas aplicables a la fianza legal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los artículos 1868 a 1904 rigen, en cuanto sean aplicables, la prestación de la fianza en los casos en que ésta es necesaria por disposición de la ley.

TITULO  XI Cláusula Compromisoria y Compromiso Arbitral
=======================================================

CAPITULO PRIMERO Cláusula Compromisoria
#######################################

Artículo 1906.- Las partes pueden obligarse mediante un pacto principal o una estipulación accesoria, a celebrar en el futuro un compromiso arbitral. En tal caso, no se requiere la designación de los árbitros. Es obligatorio fijar la extensión de la materia a que habrá de referirse el arbitraje. No es de aplicación a la cláusula compromisoria lo dispuesto en el artículo 1416. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1907.- Los otorgantes del contrato preliminar se encuentran obligados a la realización de cuantos actos sean necesarios para que el compromiso pueda tener efecto y, en particular, a la designación de los árbitros y a la determinación del asunto controvertido. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1908.- Si el compromiso no fuera formalizado voluntariamente o no lo hubiera hecho el juez a solicitud de parte, la cláusula compromisoria queda sin efecto. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

CAPITULO SEGUNDO Compromiso Arbitral
####################################

(*) De conformidad con el Numeral 1 de la Primera Disposición Transitoria del Decreto Legislativo Nº 1071, publicado el 28 junio 2008, la misma que de acuerdo con su Tercera Disposición Final, entró en vigencia el 1 de setiembre de 2008, las cláusulas y compromisos arbitrales celebrados bajo la vigencia del Código de Procedimientos Civiles de 1911 y el Código Civil de 1984 que no establecieron expresamente la clase de arbitraje, se entiende estipulado un arbitraje de derecho.

Artículo 1909.- Por el compromiso arbitral dos o más partes convienen que una controversia determinada, materia o no de un juicio, sea resuelta por tercero o terceros a quienes designan y a cuya jurisdicción y decisión se someten expresamente. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1910.- El compromiso arbitral debe celebrarse por escrito, bajo sanción de nulidad. Si hay juicio pendiente, se formaliza mediante escrito presentado al juez con firmas certificadas por el secretario de la causa. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1911.- El compromiso arbitral contendrá:

1. El nombre y domicilio de los otorgantes y de los árbitros.

Si el árbitro designado fuese una persona jurídica se indicará su denominación o razón social y domicilio.

2. La controversia que se somete al fallo arbitral, con expresión de sus circunstancias.

3. El plazo en que los árbitros deben pronunciar el laudo.

4. El lugar en que debe desarrollarse el arbitraje. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1912.- Los contratantes pueden estipular la sanción a la parte que deje de cumplir cualquier acto indispensable para su ejecución, así como la facultad de los árbitros para fijar el pago de costas. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1913.- No pueden ser objeto de compromiso arbitral las cuestiones siguientes: (*) RECTIFICADO POR FE DE ERRATAS

1. Las que versan sobre el estado y la capacidad civil de las personas.

2. Las referentes al Estado o sus bienes, salvo si se trata de los bienes de las empresas de derecho público o de las estatales de derecho privado o de economía mixta, en que el contrato requiere aprobación mediante Resolución Suprema.

Las diferencias surgidas de la ejecución de contratos derivados del cumplimiento del objeto social de las empresas de derecho público, o de las estatales de derecho privado o de economía mixta podrán ser objeto de compromiso arbitral, con conocimiento previo de la Contraloría General de la República.

3. Las que interesen a la moral y las buenas costumbres. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1914.- La existencia de la cláusula compromisoria y del compromiso arbitral puede se invocada como excepción por cualquiera de las partes. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1915.- Los árbitros deben fallar con arreglo a derecho o de acuerdo a su leal saber y entender actuando como amigables componedores.

A falta de elección expresa se presume que las partes optan por un arbitraje de derecho. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1916.- El nombramiento de árbitros de derecho debe recaer en abogados.

Pueden ser designados amigables componedores las personas naturales, nacionales o extranjeras, mayores de veinticinco años de edad, que se encuentren en el pleno ejercicio de los derechos civiles. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1917.- No pueden ser nombrados árbitros quienes tengan, en relación con las partes o con la controversia que se les somete, alguna de las causales que determinan la excusa o recusación de un juez.

Si las partes, no obstante conocer dichas circunstancias, las dispensan expresamente, el laudo no podrá ser impugnado por tal motivo. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1918.- Los árbitros deben ser designados por las partes en número impar. Si son tres o más formarán tribunal, presidido por el que ellos elijan.

Es nulo el pacto de deferir a una de las partes la facultad de hacer el nombramiento de alguno de los árbitros. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1919.- Otorgado el compromiso las partes lo presentarán a los árbitros para que acepten el cargo. De la aceptación o de la negativa se extenderá acta que firmarán los árbitros y las partes. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1920.- La aceptación de los árbitros da derecho a las partes para compelerles a que cumplan su encargo, bajo pena de responder por los daños y perjuicios que ocasionen por su incumplimiento.

Los árbitros tienen derecho a exigir la retribución convenida por las partes. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1921.- Se extingue el compromiso:

1. Por voluntad de quienes lo otorgaron, expresada en alguna de las formas establecidas en el artículo 1910.

2. Por el vencimiento del plazo señalado por las partes. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

Artículo 1922.- El procedimiento arbitral se sujeta a lo establecido en el Código de Procedimientos Civiles. (*)

(*) Artículo derogado por la Primera Disposición Final del Decreto Ley Nº 25935, publicado el 10 diciembre 1992.

TITULO  XII Renta Vitalicia
===========================

CAPITULO UNICO
##############

Artículo 1923. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por la renta vitalicia se conviene la entrega de una suma de dinero u otro bien fungible, para que sean pagados en los períodos estipulados.

Artículo 1924. Clases de renta vitalicia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La renta vitalicia puede constituirse a título oneroso o gratuito.

Artículo 1925. Formalidad en renta vitalicia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La renta vitalicia se constituye por escritura pública, bajo sanción de nulidad.

Artículo 1926. Formalidad en renta vitalicia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para la duración de la renta vitalicia debe señalarse la vida de una o varias personas.

En el contrato se determinará la vida en que concluya la renta, cuando se hubiere fijado en cabeza de varias personas.

Artículo 1927. Causales de nulidad de la Renta Vitalicia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nula la renta vitalicia cuya duración se fijó en cabeza de una persona que hubiera muerto a la fecha de la escritura pública.

También es nula la renta vitalicia constituida en cabeza de una persona que padece de enfermedad, si murió por efecto directo de ella dentro de los treinta días siguientes a la fecha de la escritura pública.

Artículo 1928. Muerte de acreedor en renta a favor de tercero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el acreedor de una renta constituida en cabeza de un tercero muere antes que éste, la renta pasa a sus herederos hasta la muerte del tercero.

Artículo 1929. Transferencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el deudor de la renta muere antes que el tercero en cuya cabeza se ha establecido su duración, la obligación se trasmite a los herederos de aquél.

Artículo 1930. Reajuste de renta vitalicia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es válida la cláusula que permite el reajuste de la renta a fin de mantenerla en valor constante.

Artículo 1931. Pluralidad de beneficiarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si al constituirse la renta en favor de varias personas, no se expresa la porción de que gozará cada una, se entiende que ellas se benefician por cuotas iguales.

Artículo 1932. Nulidad de la prohibición de cesión o embargo de la renta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nulo el pacto que prohíbe la cesión de la renta constituida a título oneroso o el embargo de ésta por deuda de la persona a quien favorece.

Artículo 1933. Prueba de supervivencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acreedor no puede pedir el pago de la renta si no justifica que vive la persona en cuya cabeza se constituyó, a no ser que la vida del acreedor fue la señalada para la duración del contrato.

Artículo 1934. Falta de pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La falta de pago de las pensiones vencidas da al acreedor el derecho a reclamar sólo el pago de éstas y el aseguramiento de las futuras.

Artículo 1935. Resolución de contrato por falta de garantía
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El beneficiario para quien se constituyó la renta vitalicia a título oneroso puede solicitar la resolución del contrato si el que recibió el bien y se obligó a pagar la pensión, no da las garantías estipuladas.

Artículo 1936. Pago por plazo adelantado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se pactó que el pago se haría por plazos adelantados, se tiene por vencido el transcurrido desde la muerte de la persona sobre cuya vida se pactó la renta.

Si el acreedor muere mientras transcurre la próxima prestación a pagar, se abonará la renta en proporción a los días en que ha vivido el sujeto en cuya cabeza se pactó.

Si la prestación se paga anticipadamente, la renta es debida en su integridad.

Artículo 1937. Extinción de la renta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si muere la persona cuya vida se designó para el pago de la renta, se extingue ésta sin que exista obligación de devolver los bienes que sirvieron de contraprestación.

Artículo 1938. Sanción por muerte causada por el obligado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El obligado a pagar la renta vitalicia que causa intencionalmente la muerte de la persona por cuya vida la constituyó, restituirá los bienes recibidos como contraprestación con sus frutos, sin que pueda exigir la devolución de la renta que antes hubiese satisfecho.

Artículo 1939. Efectos del suicidio del obligado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se constituye la renta en cabeza de quien paga y éste pierde la vida por suicidio, el acreedor tiene derecho a que se devuelvan los bienes con sus frutos, deducidas las cantidades que hubiese recibido por renta.

Artículo 1940. Derecho de acrecer en renta vitalicia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de establecerse la renta en favor y en cabeza de dos o más personas o sólo en favor de éstas, excepto entre cónyuges, la muerte de cualquiera de ellas no acrece la parte de quienes sobrevivan, salvo pacto distinto.

Artículo 1941. Renta vitalicia testamentaria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La renta vitalicia constituida por testamento se sujeta a lo dispuesto en los artículos 1923 a 1940, en cuanto sean aplicables.

TITULO  XIII Juego y Apuesta
============================

CAPITULO UNICO
##############

Artículo 1942. Definición
~~~~~~~~~~~~~~~~~~~~~~~~~

Por el juego y la apuesta permitidos, el perdedor queda obligado a satisfacer la prestación convenida, como resultado de un acontecimiento futuro o de uno realizado, pero desconocido para las partes.

El juez puede reducir equitativamente el monto de la prestación cuando resulta excesiva en relación con la situación económica del perdedor.

Artículo 1943. Juego y apuesta no autorizado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El juego y la apuesta no autorizados son aquellos que tienen carácter lucrativo, sin estar prohibidos por la ley, y no otorgan acción para reclamar por su resultado.

El que paga voluntariamente una deuda emanada del juego y la apuesta no autorizados, no puede solicitar su repetición, salvo que haya mediado dolo en la obtención de la ganancia o que el repitente sea incapaz.

Artículo 1944. Juego y apuesta prohibidos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El juego y la apuesta prohibidos son los expresamente indicados en la ley. No existe acción para reclamar por su resultado y, en caso de producirse el pago, es nulo de pleno derecho.

Artículo 1945. Nulidad de legalización de deudas de juego y apuestas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las deudas de los juegos y apuestas a que se refieren los artículos 1943 y 1944 no pueden ser materia de novación, otorgamiento de garantía para su pago, ni cualquier otro acto jurídico que encubra o envuelva su reconocimiento. Empero, la nulidad no puede oponerse al tercero de buena fe.

Estas deudas tampoco pueden ser objeto de emisión de títulos de crédito a la orden del ganador y en contra del perdedor, salvo los derechos del tercero de buena fe.

Artículo 1946. Improcedencia de repetición en deuda pagada por tercero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El tercero que sin asentimiento del perdedor paga la deuda de un juego o apuesta no autorizados no tiene acción para reclamar su reintegro. Empero, si el perdedor le cancela el importe abonado, quedará sujeto a la disposición contenida en el segundo párrafo del artículo 1943.

Artículo 1947. Juego y apuesta masiva
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los contratos de lotería, pronósticos sobre competencias deportivas, apuestas hípicas, peleas de gallos y otros espectáculos y concursos similares, se rigen por las normas legales o administrativas pertinentes. En estos casos no es de aplicación la reducción prevista en el segundo párrafo del artículo 1942.

Artículo 1948. Autorización para rifas y concursos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las rifas y demás concursos públicos eventuales serán autorizados previamente por la autoridad correspondiente.

Artículo 1949. Caducidad de la acción de cobro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción para reclamar la deuda derivada de los juegos y apuestas permitidos caduca al año de haber sido puesto su resultado en conocimiento público, salvo plazo distinto señalado por ley especial.
