===========================================
SECCION SEGUNDA EFECTOS DE LAS OBLIGACIONES
===========================================

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

TITULO  I Disposiciones Generales
=================================

CAPITULO UNICO
##############

Artículo 1218 Trasmisibilidad de la obligación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La obligación se trasmite a los herederos, salvo cuando es inherente a la persona, lo prohibe la ley o se ha pactado en contrario.

Artículo 1219 Derechos y acciones del acreedor como efecto de las obligaciones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es efecto de las obligaciones autorizar al acreedor para lo siguiente:

1.- Emplear las medidas legales a fin de que el deudor le procure aquello a que está obligado.

2.- Procurarse la prestación o hacérsela procurar por otro, a costa del deudor.

3.- Obtener del deudor la indemnización correspondiente.

4.- Ejercer los derechos del deudor, sea en vía de acción o para asumir su defensa, con excepción de los que sean inherentes a la persona o cuando lo prohiba la ley. El acreedor para el ejercicio de los derechos mencionados en este inciso, no necesita recabar previamente autorización judicial, pero deberá hacer citar a su deudor en el juicio que promueva.

Es posible ejercitar simultáneamente los derechos previstos en este artículo, salvo los casos de los incisos 1 y 2.

TITULO  II Pago
===============

CAPITULO PRIMERO Disposiciones Generales
########################################

Artículo 1220 Noción de pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se entiende efectuado el pago sólo cuando se ha ejecutado íntegramente la prestación.

Artículo 1221 Indivisibilidad del pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No puede compelerse al acreedor a recibir parcialmente la prestación objeto de la obligación, a menos que la ley o el contrato lo autoricen.

Sin embargo, cuando la deuda tiene una parte líquida y otra ilíquida, puede exigir el acreedor el pago de la primera, sin esperar que se liquide la segunda.

Artículo 1222 Pago realizado por tercero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puede hacer el pago cualquier persona, tenga o no interés en el cumplimiento de la obligación, sea con el asentimiento del deudor o sin él, salvo que el pacto o su naturaleza lo impidan.

Quien paga sin asentimiento del deudor, sólo puede exigir la restitución de aquello en que le hubiese sido útil el pago.

Artículo 1223 Aptitud legal para efectuar el pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es válido el pago de quien se encuentra en aptitud legal de efectuarlo.

Sin embargo, quien de buena fe recibió en pago bienes que se consumen por el uso o dinero de quien no podía pagar, sólo está obligado a devolver lo que no hubiese consumido o gastado.

Artículo 1224 Aptitud legal para recibir el pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sólo es válido el pago que se efectúe al acreedor o al designado por el juez, por la ley o por el propio acreedor, salvo que, hecho a persona no autorizada, el acreedor lo ratifique o se aproveche de él.

Artículo 1225 Pago a persona con derecho a cobrar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Extingue la obligación el pago hecho a persona que está en posesión del derecho de cobrar, aunque después se le quite la posesión o se declare que no la tuvo.

Artículo 1226 Presunción de autorización para cobrar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El portador de un recibo se reputa autorizado para recibir el pago, a menos que las circunstancias se opongan a admitir esta presunción.

Artículo 1227 Pago a Incapaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El pago hecho a incapaces sin asentimiento de sus representantes legales, no extingue la obligación. Si se prueba que el pago fue útil para el incapaz, se extingue la obligación en la parte pagada.

Artículo 1228 Ineficacia del pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El pago efectuado por el deudor después de notificado judicialmente para que no lo verifique, no extingue la obligación.

Artículo 1229 Prueba del pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La prueba del pago incumbe a quien pretende haberlo efectuado.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1230 Retención del pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El deudor puede retener el pago mientras no le sea otorgado el recibo correspondiente.

Tratándose de deudas cuyo recibo sea la devolución del título, perdido éste, quien se encuentre en aptitud de verificar el pago puede retenerlo y exigir del acreedor la declaración judicial que inutilice el título extraviado.

Artículo 1231 Presunción de pago total
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el pago deba efectuarse en cuotas periódicas, el recibo de alguna o de la última, en su caso, hace presumir el pago de las anteriores, salvo prueba en contrario.

Artículo 1232 Presunción de pago de intereses
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El recibo de pago del capital otorgado sin reserva de intereses, hace presumir el pago de éstos, salvo prueba en contrario.

Artículo 1233 Pago con títulos valores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La entrega de títulos valores que constituyen órdenes o promesas de pago, sólo extinguirá la obligación primitiva cuando hubiesen sido pagados o cuando por culpa del acreedor se hubiesen perjudicado, salvo pacto en contrario.

Entre tanto la acción derivada de la obligación primitiva quedará en suspenso.

Artículo 1234 Inexigibilidad de pago en moneda distinta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El pago de una deuda contraída en moneda nacional no podrá exigirse en moneda distinta, ni en cantidad diferente al monto nominal originalmente pactado.

Artículo 1235 Teoría valorista
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No obstante lo establecido en el artículo 1234, las partes pueden acordar que el monto de una deuda contraída en moneda nacional sea referido a índices de reajuste automático que fije el Banco Central de Reserva del Perú, a otras monedas o a mercancías, a fin de mantener dicho monto en valor constante.

El pago de las deudas a que se refiere el párrafo anterior se efectuará en moneda nacional, en monto equivalente al valor de referencia, al día del vencimiento de la obligación.

Si el deudor retardara el pago, el acreedor puede exigir, a su elección,  que la deuda sea pagada al valor de referencia al día del vencimiento de la obligación o al día en que se efectúe el pago.

Artículo 1236 Cálculo del valor del pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando deba restituirse el valor de una prestación, aquél se calcula al que tenga al día del pago, salvo disposición legal diferente o pacto en contrario. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por la Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 1236.- Cuando por mandato de la ley o resolución judicial deba restituirse una prestación o determinar su valor, éste se calcula al que tenga el día del pago, salvo disposición legal diferente o pacto en contrario.

El Juez, incluso durante el proceso de ejecución, está facultado para actualizar la pretensión dineraria, aplicando los criterios a que se refiere el Artículo 1235 o cualquier otro índice de corrección que permita reajustar el monto de la obligación a valor constante. Para ello deberá tener en cuenta las circunstancias del caso concreto, en resolución debimente motivada.

La actualización de valor es independiente de lo que se resuelva sobre intereses." (*)

(*) Artículo sustituido por el Artículo 1 de la Ley Nº 26598, publicada el 24 abril 1996, cuyo texto es el siguiente:

"Artículo 1236.- Cuando deba restituirse el valor de una prestación, aquel se calcula al que tenga al día del pago, salvo disposición legal diferente o pacto en contrario."

Artículo 1237 Deuda contraida en moneda extranjera
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pueden concertarse obligaciones en moneda extranjera no prohibidas por leyes especiales.

El pago de una deuda en moneda extranjera puede hacerse en moneda nacional al tipo de cambio de venta del día y lugar del vencimiento de la obligación. Es nulo todo pacto en contrario.

En el caso previsto por el párrafo anterior, si el deudor retardara el pago, el acreedor puede exigir, a su elección, que la deuda sea pagada en moneda nacional según el tipo de cambio de venta de la fecha de vencimiento de la obligación, o el que rija el día del pago. (*)

(*) Artículo sustituido por el Artículo 1 del Decreto Ley Nº  25878, publicado el 26 noviembre 1992, cuyo texto es el siguiente:

"Artículo 1237.- Pueden concertarse obligaciones en moneda extranjera no prohibidas por leyes especiales.

Salvo pacto en contrario, el pago de una deuda en moneda extranjera puede hacerse en moneda nacional al tipo de cambio de venta del día y lugar del vencimiento de la obligación.

En el caso a que se refiere el párrafo anterior, si no hubiera mediado pacto en contrario en lo referido a la moneda de pago y el deudor retardara el pago, el acreedor puede exigir, a su elección, que el pago en moneda nacional se haga al tipo de cambio de venta en la fecha de vencimiento de la obligación, o al que rija el día del pago."

Artículo 1238 Lugar de pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~

El pago debe efectuarse en el domicilio del deudor, salvo estipulación en contrario, o que ello resulte de la ley, de la naturaleza de la obligación o de las circunstancias del caso.

Designados varios lugares para el pago, el acreedor puede elegir cualquiera de ellos. Esta regla se aplica respecto al deudor, cuando el pago deba efectuarse en el domicilio del acreedor.

Artículo 1239 Cambio de domicilio de las partes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el deudor cambia de domicilio, habiendo sido designado éste como lugar para el pago, el acreedor puede exigirlo en el primer domicilio o en el nuevo.

Igual regla es de aplicación, respecto al deudor, cuando el pago deba verificarse en el domicilio del acreedor.

Artículo 1240 Plazo para el pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si no hubiese plazo designado, el acreedor puede exigir el pago inmediatamente después de contraída la obligación.

Artículo 1241 Gastos del pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los gastos que ocasione el pago son de cuenta del deudor.

CAPITULO SEGUNDO Pago de Intereses
##################################

Artículo 1242 Interés compensatorio y moratorio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El interés es compensatorio cuando constituye la contraprestación por el uso del dinero o de cualquier otro bien.

Es moratorio cuanto tiene por finalidad indemnizar la mora en el pago.

Artículo 1243 Tasa máxima de interés convencional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La tasa máxima del interés convencional compensatorio o moratorio, es fijada por el Banco Central de Reserva del Perú.

Cualquier exceso sobre la tasa máxima da lugar a la devolución o a la imputación al capital, a voluntad del deudor.

CONCORDANCIAS:     Ley N° 29571, Art. 94 (Determinación de las tasas de interés)

Artículo 1244 Tasa de interés legal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La tasa del interés legal es fijada por el Banco Central de Reserva del Perú.

Artículo 1245 Pago de interés legal a falta de pacto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando deba pagarse interés, sin haberse fijado la tasa, el deudor debe abonar el interés legal.

Artículo 1246 Pago del interés por mora
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si no se ha convenido el interés moratorio, el deudor sólo está obligado a pagar por causa de mora el interés compensatorio pactado y, en su defecto, el interés legal.

Artículo 1247 Intereses en obligaciones no pecuniarias
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la obligación no pecuniaria, el interés se fija de acuerdo al valor que tengan los bienes materia de la obligación en la plaza donde deba pagarse al día siguiente del vencimiento, o con el que determinen los peritos si el bien ha perecido al momento de hacerse la evaluación.

Artículo 1248 Intereses en obligaciones consistentes en títulos valores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando la obligación consiste en títulos valores, el interés es igual a la renta que devenguen o, a falta de ella, al interés legal. En este último caso, se determina el valor de los títulos de acuerdo con su cotización en la bolsa o, en su defecto, por el que tengan en la plaza el día siguiente al de su vencimiento.

Artículo 1249 Limitación al anatocismo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No se puede pactar la capitalización de intereses al momento de contraerse la obligación, salvo que se trate de cuentas mercantiles, bancarias o similares.

Artículo 1250 Validez del convenio de capitalización de intereses
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es válido el convenio sobre capitalización de intereses celebrado por escrito después de contraída la obligación, siempre que medie no menos de un año de atraso en el pago de los intereses.

CAPITULO TERCERO Pago por Consignación
######################################

Artículo 1251 Consignación y presupuestos de procedencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el acreedor a quien se hace el ofrecimiento de pago se niega a admitirlo, el deudor queda libre de responsabilidad si consigna la prestación debida. Es necesario, en este caso, que el ofrecimiento se haya efectuado concurriendo las circunstancias requeridas para hacer válidamente el pago.

Procede también la consignación en los casos en que el deudor no puede hacer un pago válido. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por la Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 1251.- El deudor queda libre de su obligación si consigna la prestacion debida y concurren los siguientes requisitos:

1.- Que el deudor haya ofrecido al acreedor el pago de la prestación debida, o lo hubiere puesto a su disposición de la manera pactada en el título de la obligación.

2.- Que, respecto del acreedor, concurran los supuestos del Artículo 1338 o injustificadamente se haya negado a recibir el pago. Se entiende que hay negativa tácita en los casos de respuestas evasivas, de inconcurrencia al lugar pactado en el día y hora señalados para el cumplimiento, cuando se rehuse a entregar recibo o conductas análogas."

Artículo 1252 Consignación judicial o extrajudicial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La consignación debe efectuarse con citación del acreedor, en la persona que la ley o el juez designen, extendiéndose acta de todas las circunstancias del depósito.

Los depósitos de dinero y valores se hacen en el Banco de la Nación. El dinero consignado devenga la tasa del interés legal. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por la Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 1252.-  El ofrecimiento puede ser judicial o extrajudicial.

Es judicial en los casos que así se hubiera pactado y además: cuando no estuviera establecida contractual o legalmente la forma de hacer el pago, cuando por causa que no le sea imputable el deudor estuviera impedido de cumplir la prestación de la manera prevista, cuando el acreedor no realiza los actos de colaboración necesarios para que el deudor pueda cumplir la que le compete, cuando el acreedor no sea conocido o fuese incierto, cuando se ignore su domicilio, cuando se encuentre ausente o fuera incapaz sin tener representante o curador designado, cuando el crédito fuera litigioso o lo reclamaran varios acreedores y en situaciones análogas que impidan al deudor ofrecer o efectuar directamente un pago válido. (*)

(*) Párrafo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

"Es judicial en los casos que así se hubiera pactado y además: cuando no estuviera establecida contractual o legalmente la forma de hacer el pago, cuando por causa que no le sea imputable el deudor estuviera impedido de cumplir la prestación de la manera prevista, cuando el acreedor no realiza los actos de colaboración necesarios para que el deudor pueda cumplir la que le compete, cuando el acreedor no sea conocido o fuese incierto, cuando se ignore su domicilio, cuando se encuentre ausente o fuera una persona contemplada en el artículo 43 o 44 del Código Civil sin tener representante, curador o apoyo designado, cuando el crédito fuera litigioso o lo reclamaran varios acreedores y en situaciones análogas que impidan al deudor ofrecer o efectuar directamente un pago válido."

El ofrecimiento extrajudicial debe efectuarse de la manera que estuviera pactada la obligación y, en su defecto, mediante carta notarial cursada al acreedor con una anticipación no menor de cinco días anteriores a la fecha de cumplimiento debido, si estuviera determinado. Si no lo estuviera, la anticipación debe ser de diez días anteriores a la fecha de cumplimiento que el deudor señale."

Artículo 1253 Ofrecimiento judicial de pago, consignación y oposición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La consignación de dinero o de otros bienes que no es impugnada por el acreedor, dentro de los diez días siguientes a la fecha de su citación, surte los efectos del pago retroactivamente al día del ofrecimiento. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por la Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 1253.-  El ofrecimiento judicial de pago y la consignación se tramitan como proceso no contencioso de la manera que establece el Código Procesal Civil.

La oposición al ofrecimiento extrajudicial y, en su caso, a la consignación efectuada, se tramitan en el proceso contencioso que corresponda a la naturaleza de la relación jurídica respectiva."

Artículo 1254 Validez del pago con efecto retroactivo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La consignación impugnada surte los efectos del pago retroactivamente al día del ofrecimiento, cuando la impugnación del acreedor se desestima por resolución con autoridad de cosa juzgada. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por la Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 1254.- El pago se reputa válido con efecto retroactivo a la fecha de ofrecimiento, cuando:

1.- El acreedor no se opone al ofrecimiento judicial dentro de los cinco días siguientes de su emplazamiento;

2.- La oposición del acreedor al pago por cualquiera de las formas de ofrecimiento, es desestimada por resolución con autoridad de cosa juzgada.

El ofrecimiento judicial se entiende efectuado el día en que el acreedor es válidamente emplazado. El extrajudicial se entiende efectuado el día que es puesto en conocimiento."

Artículo 1255 Desistimiento del ofrecimiento de pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La consignación puede ser retirada por quien la efectuó, en los siguientes casos:

1.- Antes de la aceptación por el acreedor.

2.- Cuando hay impugnación, mientras ésta no sea desestimada por resolución con autoridad de cosa juzgada. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por la Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"Artículo 1255.- El deudor puede desistirse del pago ofrecido y, en su caso, retirar el depósito efectuado, en los casos siguientes:

1.- Antes de la aceptación por el acreedor.

2.- Cuando hay oposición, mientras no sea desestimada por resolución con autoridad de cosa juzgada."

CAPITULO CUARTO Imputación del Pago
###################################

Artículo 1256 Imputación del pago por el deudor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quien tiene varias obligaciones de la misma naturaleza constituidas por prestaciones fungibles y homogéneas, en favor de un solo acreedor, puede indicar al tiempo de hacer el pago, o, en todo caso, antes de aceptar el recibo emitido por el acreedor, a cual de ellas se aplica éste. Sin el asentimiento del acreedor, no se imputará el pago parcialmente o a una deuda ilíquida o no vencida.

Artículo 1257 Orden de la imputación convencional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quien deba capital, gastos e intereses, no puede, sin el asentimiento del acreedor, aplicar el pago al capital antes que a los gastos, ni a éstos antes que a los intereses.

Artículo 1258 Imputación por acreedor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el deudor no ha indicado a cual de las deudas debe imputarse el pago, pero hubiese aceptado recibo del acreedor aplicándolo a alguna de ellas, no puede reclamar contra esta imputación, a menos que exista causa que impida practicarla.

Artículo 1259 Imputación legal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No expresándose a qué deuda debe hacerse la imputación, se aplica el pago a la menos garantizada; entre varias deudas igualmente garantizadas, a la más onerosa para el deudor; y entre varias deudas igualmente garantizadas y onerosas, a la más antigua. Si estas reglas no pueden aplicarse, la imputación se hará proporcionalmente.

CAPITULO QUINTO Pago con Subrogación
####################################

Artículo 1260 Subrogación legal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La subrogación opera de pleno derecho en favor:

1.- De quien paga una deuda a la cual estaba obligado, indivisible o solidariamente, con otro u otros.
2.- De quien por tener legítimo interés cumple la obligación.
3.- Del acreedor que paga la deuda del deudor común a otro acreedor que le es preferente.

Artículo 1261 Subrogación convencional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La subrogación convencional tiene lugar:

1.- Cuando el acreedor recibe el pago de un tercero y lo sustituye en sus derechos.

2.- Cuando el tercero no interesado en la obligación paga con aprobación expresa o tácita del deudor.

3.- Cuando el deudor paga con una prestación que ha recibido en mutuo y subroga al mutuante en los derechos el acreedor, siempre que el contrato de mutuo se haya celebrado por documento de fecha cierta, haciendo constar tal propósito en dicho contrato y expresando su procedencia al tiempo de efectuar el pago.

Artículo 1262 Efectos de la subrogación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La subrogación sustituye al subrogado en todos los derechos, acciones y garantías del antiguo acreedor, hasta por el monto de lo que hubiese pagado.

Artículo 1263 Efectos de la subrogación en las obligaciones solidarias o indivisibles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos del artículo 1260, inciso 1, el subrogado está autorizado a ejercitar los derechos del acreedor contra sus codeudores, sólo hasta la concurrencia de la parte por la que cada uno de éstos estaba obligado a contribuir para el pago de la deuda, aplicándose, sin embargo, las reglas del artículo 1204.

Artículo 1264 Subrogación parcial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el subrogado en lugar del acreedor lo fuese sólo parcialmente, y los bienes del deudor no alcanzasen para pagar la parte restante que corresponda al acreedor y la del subrogado, ambos concurrirán con igual derecho por la porción que respectivamente se les debiera.

CONCORDANCIAS:     R.M. N° 581-2008-EF-15 , Art. 24 (Procedimiento para el honramiento de las garantías y/o indemnización de los seguros)

CAPITULO SEXTO Dación en Pago
#############################

Artículo 1265 Noción
~~~~~~~~~~~~~~~~~~~~

El pago queda efectuado cuando el acreedor recibe como cancelación total o parcial una prestación diferente a la que debía cumplirse.

Artículo 1266 Normas aplicables a dación en pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se determina la cantidad por la cual el acreedor recibe el bien en pago, sus relaciones con el deudor se regulan por las reglas de la compraventa.

CAPITULO SEPTIMO Pago Indebido
##############################

Artículo 1267 Pago indebido por error de hecho o de derecho
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que por error de hecho o de derecho entrega a otro algún bien o cantidad en pago, puede exigir la restitución de quien la recibió.

Artículo 1268 Pago indebido recibido de buena fe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Queda exento de la obligación de restituir quien, creyendo de buena fe que el pago se hacía por cuenta de un crédito legítimo y subsistente, hubiese inutilizado el título, limitado o cancelado las garantías de su derecho o dejado prescribir la acción el verdadero deudor. El que pagó indebidamente sólo podrá dirigirse contra el verdadero deudor.

Artículo 1269 Pago indebido recibido de mala fe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que acepta un pago indebido, si ha procedido de mala fe, debe abonar el interés legal cuando se trate de capitales o los frutos percibidos o que ha debido percibir cuando el bien recibido los produjera, desde la fecha del pago indebido.

Además, responde de la pérdida o deterioro que  haya sufrido el bien por cualquier causa, y de los perjuicios irrogados a quien lo entregó, hasta que lo recobre.

Puede liberarse de esta responsabilidad, si prueba que la causa no imputable habría afectado al bien del mismo modo si hubiera estado en poder de quien lo entregó.

Artículo 1270 Enajenación del bien recibido como pago indebido de mala fe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si quien acepta un pago indebido de mala fe, enajena el bien a un tercero que también actúa de mala fe, quien efectúo el pago puede exigir la restitución, y a ambos, solidariamente, la indemnización de daños y perjuicios.

En caso que la enajenación hubiese sido a título oneroso pero el tercero hubiera procedido de buena fe, quien recibió el pago indebido deberá devolver el valor del bien, más la indemnización de daños y perjuicios.

Si la enajenación se hizo a título gratuito y el tercero procedió de buena fe, quien efectuó el pago indebido puede exigir la restitución del bien. En este caso, sin embargo, sólo está obligado a pagar la correspondiente indemnización de daños y perjuicios quien recibió el pago indebido de mala fe.

Artículo 1271 Restitución de intereses o frutos por pago indebido de buena fe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que de buena fe acepta un pago indebido debe restituir los intereses o frutos percibidos y responde de la pérdida o deterioro del bien en cuanto por ellos se hubiese enriquecido.

Artículo 1272 Restitución de intereses o frutos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si quien acepta un pago indebido de buena fe, hubiese enajenado el bien a un tercero que también tuviera buena fe, restituirá el precio o cederá la acción para hacerlo efectivo.

Si el bien se hubiese transferido a un tercero a título gratuito, o el tercero, adquirente a título oneroso, hubiese actuado de mala fe, quien paga indebidamente puede exigir la restitución. En estos casos sólo el tercero, adquirente a título gratuito u oneroso, que actuó de mala fe, estará obligado a indemnizar los daños y perjuicios irrogados.

Artículo 1273 Carga de la prueba del error
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Corre a cargo de quien pretende haber efectuado el pago probar el error con que lo hizo, a menos que el demandado negara haber recibido el bien que se le reclame. En este caso, justificada por el demandante la entrega, queda relevado de toda otra prueba. Esto no limita el derecho del demandado para acreditar que le era debido lo que se supone recibió.

Sin embargo, se presume que hubo error en el pago cuando se cumple con una prestación que nunca se debió o que ya estaba pagadas. Aquel a quien se pide la devolución, puede probar que la entrega se efectuó a título de liberalidad o por otra causa justificada.

Artículo 1274 Prescripción de la acción por pago indebido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción para recuperar lo indebidamente pagado prescribe a los cinco años de efectuado el pago.

Artículo 1275 Improcedencia de la repetición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No hay repetición de lo pagado en virtud de una deuda prescrita, o para cumplir deberes morales o de solidaridad social o para obtener un fin inmoral o ilícito.

Lo pagado para obtener un fin inmoral o ilícito corresponde a la institución encargada del bienestar familiar.

Artículo 1276 Reglas de pago indebido para obligaciones de hacer y no hacer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las reglas de este capítulo se aplican, en cuanto sean pertinentes, a las obligaciones de hacer en las que no proceda restituir la prestación y a las obligaciones de no hacer.

En tales casos, quien acepta el pago indebido de buena fe, sólo está obligado a indemnizar aquello en que se hubiese beneficiado, si procede de mala fe, queda obligado a restituir el íntegro del valor de la prestación, más la correspondiente indemnización de daños y perjuicios.

TITULO  III Novación
====================

CAPITULO UNICO
##############

Artículo 1277 Definición: requisitos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Por la novación se sustituye una obligación por otra.

Para que exista novación es preciso que la voluntad de novar se manifieste indubitablemente en la nueva obligación, o que la existencia de la anterior sea incompatible con la nueva.

Artículo 1278 Novación objetiva
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Hay novación objetiva cuando el acreedor y el deudor sustituyen la obligación primitiva por otra, con prestación distinta o a título diferente.

Artículo 1279 Actos que no constituyen novación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La emisión de títulos valores o su renovación, la modificación de un plazo o del lugar del pago, o cualquier otro cambio accesorio de la obligación, no producen novación.

Artículo 1280 Novación subjetiva activa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la novación por cambio de acreedor se requiere, además del acuerdo entre el acreedor que se sustituye y el sustituido, el asentimiento del deudor.

Artículo 1281 Novación subjetiva por delegación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La novación por delegación requiere, además del acuerdo entre el deudor que se sustituye y el sustituido, el asentimiento del acreedor.

Artículo 1282 Novación por expromisión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La novación por expromisión puede efectuarse aun contra la voluntad del deudor primitivo.

Artículo 1283 Intransmisibilidad de garantía a la nueva obligación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la novación no se trasmiten a la nueva obligación las garantías de la obligación extinguida, salvo pacto en contrario.

Sin embargo, en la novación por delegación la obligación es exigible contra el deudor primitivo y sus garantes, en caso que la insolvencia del nuevo deudor hubiese sido anterior y pública, o conocida del deudor al delegar su deuda.

Artículo 1284 Novación de obligación sujeta a condición suspensiva
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando una obligación pura se convierte en otra sujeta a condición suspensiva, sólo habrá novación si se cumple la condición, salvo pacto en contrario.

Las mismas reglas se aplican si la antigua obligación estuviera sujeta a condición suspensiva y la nueva fuera pura.

Artículo 1285 Novación de obligación sujeta a condición resolutoria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando una obligación pura se convierte en otra sujeta a condición resolutoria, opera la novación, salvo pacto en contrario.

Las mismas reglas se aplican si la antigua obligación estuviera sujeta a condición resolutoria y la nueva fuera pura.

Artículo 1286 Novación de obligación nula o anulable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la obligación primitiva fuera nula, no existe novación.

Si la obligación primitiva fuera anulable, la novación tiene validez si el deudor, conociendo del vicio,  asume la nueva obligación.

Artículo 1287 Nulidad o anulabilidad de la nueva obligación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la nueva obligación se declara nula o es anulada, la primitiva obligación revive, pero el acreedor no podrá valerse de las garantías prestadas por terceros.

TITULO  IV Compensación
=======================

CAPITULO UNICO
##############

Artículo 1288 Extinción de la obligación por compensación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Por la compensación se extinguen las obligaciones recíprocas, líquidas, exigibles y de prestaciones fungibles y homogéneas, hasta donde respectivamente alcancen, desde que hayan sido opuestas la una a la otra. La compensación no opera cuando el acreedor y el deudor la excluyen de común acuerdo.

Artículo 1289 Oponibilidad de la compensación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puede oponerse la compensación por acuerdo entre las partes, aun cuando no concurran los requisitos previstos por el artículo 1288. Los requisitos para tal compensación pueden establecerse previamente.

Artículo 1290 Prohibición de la compensación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se prohíbe la compensación:

1.- En la restitución de bienes de los que el propietario haya sido despojado.

2.- En la restitución de bienes depositados o entregados en comodato.

3.- Del crédito inembargable.

4.- Entre particulares y el Estado, salvo en los casos permitidos por la ley.

Artículo 1291 Oponibilidad de la compensación por el garante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El garante puede oponer la compensación de lo que el acreedor deba al deudor.

Artículo 1292 lnoponibilidad de la compensación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El deudor que ha consentido que el acreedor ceda su derecho a un tercero, no puede oponer a éste la compensación que hubiera podido oponer al cedente.

Artículo 1293 Imputación legal de compensación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando una persona tuviera respecto de otra varias deudas compensables, y no manifestara al oponer la compensación a cuál la imputa, se observarán las disposiciones del artículo 1259. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 1294 Intangibilidad de los derechos adquiridos por efecto de compensación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La compensación no perjudica los derechos adquiridos sobre cualquiera de los créditos.

TITULO  V Condonación
=====================

CAPITULO UNICO
##############

Artículo 1295 Extinción de obligación por condonación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

De cualquier modo que se pruebe la condonación de la deuda efectuada de común acuerdo entre el acreedor y el deudor, se extingue la obligación, sin perjuicio del derecho de tercero.

Artículo 1296 Efectos de la condonación a uno de los garantes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La condonación a uno de los garantes no extingue la obligación del deudor principal, ni la de los demás garantes.

La condonación efectuada a uno de los garantes sin asentimiento de los otros aprovecha a todos, hasta donde alcance la parte del garante en cuyo favor se realizó.

Artículo 1297 Condonación de deuda
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Hay condonación de la deuda cuando el acreedor entrega al deudor el documento original en que consta aquella, salvo que el deudor pruebe que la ha pagado.

Artículo 1298 Presunción de condonación de la prenda
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La prenda en poder del deudor hace presumir su devolución voluntaria, salvo prueba en contrario.

Artículo 1299 Condonación de la prenda
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La devolución voluntaria de la prenda determina la condonación de la misma, pero no la de la deuda.

TITULO  VI Consolidación
========================

CAPITULO UNICO
##############

Artículo 1300 Consolidación total o parcial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La consolidación puede producirse respecto de toda la obligación o de parte de ella.

Artículo 1301 Efectos del cese de la consolidación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la consolidación cesa, se restablece la separación de las calidades de acreedor y deudor reunidas en la misma persona.

En tal caso, la obligación extinguida renace con todos sus accesorios, sin perjuicio del derecho de terceros.

TITULO  VII Transacción
=======================

CAPITULO UNICO
##############

Artículo 1302 Noción
~~~~~~~~~~~~~~~~~~~~

Por la transacción las partes, haciéndose concesiones recíprocas, deciden sobre algún asunto dudoso o litigioso, evitando el pleito que podría promoverse o finalizando el que está iniciado.

Con las concesiones recíprocas, también se pueden crear, regular, modificar o extinguir relaciones diversas de aquellas que han constituido objeto de controversia entre las partes.

La transacción tiene valor de cosa juzgada.

Artículo 1303 Contenido de la transacción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La transacción debe contener la renuncia de las partes a cualquier acción que tenga una contra otra sobre el objeto de dicha transacción.

Artículo 1304 Formalidad de la transacción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La transacción debe hacerse por escrito, bajo sanción de nulidad, o por petición al juez que conoce el litigio.

Artículo 1305 Derechos transigibles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sólo los derechos patrimoniales pueden ser objeto de transacción.

Artículo 1306 Transacción de responsabilidad civil
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se puede transigir sobre la responsabilidad civil que provenga de delito.

Artículo 1307 Transacción del ausente o incapaz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los representantes de ausentes o incapaces pueden transigir con aprobación del juez, quien para este efecto oirá al Ministerio Público y al consejo de familia cuando lo haya y lo estime conveniente. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 1308 Transacción de obligación nula o anulable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la obligación dudosa o litigiosa fuera nula, la transacción adolecerá de nulidad. Si fuera anulable y las partes, conociendo el vicio, la celebran, tiene validez la transacción.

Artículo 1309 Transacción sobre nulidad o anulabilidad de obligación sujeta a litigio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la cuestión dudosa o litigiosa versara sobre la nulidad o anulabilidad de la obligación, y las partes así lo manifestaran expresamente, la transacción será válida.

Artículo 1310 Indivisibilidad de la transacción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La transacción es indivisible y si alguna de sus estipulaciones fuese nula o se anulase, queda sin efecto, salvo pacto en contrario.

En tal caso, se restablecen las garantías otorgadas por las partes pero no las prestadas por terceros.

Artículo 1311 La suerte como medio de transacción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando las partes se sirven de la suerte para dirimir cuestiones, ello produce los efectos de la transacción y le son aplicables las reglas de este título. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 1312 Ejecución de la transacción judicial y extrajudicial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La transacción judicial se ejecuta de la misma manera que la sentencia y la extrajudicial, en la vía ejecutiva.

TITULO  VIII Mutuo Disenso
==========================

CAPITULO UNICO
##############

Artículo 1313 Noción del mutuo disenso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Por el mutuo disenso las partes que han celebrado un acto jurídico acuerdan dejarlo sin efecto. Si perjudica el derecho de tercero se tiene por no efectuado.

PROCESOS CONSTITUCIONALES

TITULO  IX Inejecución de Obligaciones
======================================

CAPITULO PRIMERO Disposiciones Generales
########################################

Artículo 1314 Inimputabilidad por diligencia ordinaria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quien actúa con la diligencia ordinaria requerida, no es imputable por la inejecución de la obligación o por su cumplimiento parcial, tardío o defectuoso.

Artículo 1315 Caso fortuito o fuerza mayor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Caso fortuito o fuerza mayor es la causa no imputable, consistente en un evento extraordinario, imprevisible e irresistible, que impide la ejecución de la obligación o determina su cumplimiento parcial, tardío o defectuoso.

CONCORDANCIAS:     R. Nº 020-2008-CD-OSIPTEL, Art. 44 (Interrupción por caso fortuito o fuerza mayor)

Artículo 1316 Extinción de la obligación por causas no imputables al deudor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La obligación se extingue si la prestación no se ejecuta por causa no imputable al deudor.

Si dicha causa es temporal, el deudor no es responsable por el retardo mientras ella perdure. Sin embargo, la obligación se extingue si la causa que determina la inejecución persiste hasta que al deudor, de acuerdo al título de la obligación o a la naturaleza de la prestación, ya no se le pueda considerar obligado a ejecutarla; o hasta que el acreedor justificadamente pierda interés en su cumplimiento o ya no le sea útil.

También se extingue la obligación que sólo es susceptible de ejecutarse parcialmente, si ella no fuese útil para el acreedor o si éste no tuviese justificado interés en su ejecución parcial. En caso contrario, el deudor queda obligado a ejecutarla con reducción de la contraprestación, si la hubiere.

Artículo 1317 Daños y perjuicios por inejecución no imputable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El deudor no responde de los daños y perjuicios resultantes de la inejecución de la obligación, o de su cumplimiento parcial, tardío o defectuoso, por causas no imputables, salvo que lo contrario esté previsto expresamente por la ley o por el título de la obligación.

Artículo 1318 Dolo
~~~~~~~~~~~~~~~~~~

Procede con dolo quien deliberadamente no ejecuta la obligación.

Artículo 1319 Culpa inexcusable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Incurre en culpa inexcusable quien por negligencia grave no ejecuta la obligación.

Artículo 1320 Culpa leve
~~~~~~~~~~~~~~~~~~~~~~~~

Actúa con culpa leve quien omite aquella diligencia ordinaria exigida por la naturaleza de la obligación y que corresponda a las circunstancias de las personas, del tiempo y del lugar.

Artículo 1321 Indemnización por dolo, culpa leve e inexcusable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Queda sujeto a la indemnización de daños y perjuicios quien no ejecuta sus obligaciones por dolo, culpa inexcusable o culpa leve.

El resarcimiento por la inejecución de la obligación o por su cumplimiento parcial, tardío o defectuoso, comprende tanto el daño emergente como el lucro cesante, en cuanto sean consecuencia inmediata y directa de tal inejecución.

Si la inejecución o el cumplimiento parcial, tardío o defectuoso de la obligación, obedecieran a culpa leve, el resarcimiento se limita al daño que podía preverse al tiempo en que ella fue contraída.

Artículo 1322 Indemnización por daño moral
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El daño moral, cuando él se hubiera irrogado, también es susceptible de resarcimiento.

Artículo 1323 Incumplimiento de pago de cuota
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el pago deba efectuarse en cuotas periódicas, el incumplimiento de tres cuotas, sucesivas o no, concede al acreedor el derecho de exigir al deudor el inmediato pago del saldo, dándose por vencidas las cuotas que estuviesen pendientes, salvo pacto en contrario.

Artículo 1324 Efectos de la inejecución de obligaciones dinerarias
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las obligaciones de dar sumas de dinero devengan el interés legal que fija el Banco Central de Reserva del Perú, desde el día en que el deudor incurra en mora, sin necesidad de que el acreedor pruebe haber sufrido daño alguno. Si antes de la mora se debían intereses mayores, ellos continuarán devengándose después del día de la mora, con la calidad de intereses moratorios.

Si se hubiese estipulado la indemnización del daño ulterior, corresponde al acreedor que demuestre haberlo sufrido el respectivo resarcimiento.

Artículo 1325 Responsabilidad en obligaciones ejecutadas por tercero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El deudor que para ejecutar la obligación se vale de terceros, responde de los hechos dolosos o culposos de éstos, salvo pacto en contrario.

Artículo 1326 Reducción del resarcimiento por actos del acreedor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el hecho doloso o culposo del acreedor hubiese concurrido a ocasionar el daño, el resarcimiento se reducirá según su gravedad y la importancia de las consecuencias que de él deriven.

Artículo 1327 Liberación del resarcimiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El resarcimiento no se debe por los daños que el acreedor habría podido evitar usando la diligencia ordinaria, salvo pacto en contrario.

Artículo 1328 Nulidad de pacto de exoneración y limitación de responsabilidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nula toda estipulación que excluya o limite la responsabilidad por dolo o culpa inexcusable del deudor o de los terceros de quien éste se valga.

También es nulo cualquier pacto de exoneración o de limitación de responsabilidad para los casos en que el deudor o dichos terceros violen obligaciones derivadas de normas de orden público.

Artículo 1329 Presunción de la culpa leve del deudor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se presume que la inejecución de la obligación, o su cumplimiento parcial, tardío o defectuoso, obedece a culpa leve del deudor.

Artículo 1330 Prueba de dolo y culpa inexcusable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La prueba del dolo o de la culpa inexcusable corresponde al perjudicado por la inejecución de la obligación, o por su cumplimiento parcial, tardío o defectuoso.

Artículo 1331 Prueba de daños y perjuicios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La prueba de los daños y perjuicios y de su cuantía también corresponde al perjudicado por la inejecución de la obligación, o por su cumplimiento parcial, tardío o defectuoso.

Artículo 1332 Valoración del resarcimiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el resarcimiento del daño no pudiera ser probado en su monto preciso, deberá fijarlo el juez con valoración equitativa.

CAPITULO SEGUNDO Mora
#####################

Artículo 1333 Constitución en mora
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Incurre en mora el obligado desde que el acreedor le exija, judicial o extrajudicialmente, el cumplimiento de su obligación.

No es necesaria la intimación para que la mora exista:

1.- Cuando la ley o el pacto lo declaren expresamente.

2.- Cuando de la naturaleza y circunstancias de la obligación resultare que la designación del tiempo en que había de entregarse el bien, o practicarse el servicio, hubiese sido motivo determinante para contraerla.

3.- Cuando el deudor manifieste por escrito su negativa a cumplir la obligación.

4.- Cuando la intimación no fuese posible por causa imputable al deudor.

Artículo 1334 Mora en obligaciones de dar sumas de dinero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En las obligaciones de dar sumas de dinero cuyo monto requiera ser determinado mediante resolución judicial, hay mora a partir de la fecha de la citación con la demanda.

Se exceptúa de esta regla lo dispuesto en el artículo 1985. (*)

(*) De conformidad con la Octava Disposición Complementaria del Decreto Legislativo Nº 1071, publicado el 28 junio 2008, dispone que para efectos de lo dispuesto en el presente artículo, la referencia a la citación con la demanda se entenderá referida en materia arbitral a la recepción de la solicitud para someter la controversia a arbitraje, el mismo que entró en vigencia el 1 de setiembre de 2008.

Artículo 1335 Mora en obligaciones recíprocas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En las obligaciones recíprocas, ninguno de los obligados incurre en mora sino desde que alguno de ellos cumple su obligación, u otorga garantías de que la cumplirá.

Artículo 1336 Responsabilidad del deudor en caso de mora
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El deudor constituido en mora responde de los daños y perjuicios que irrogue por el retraso en el cumplimiento de la obligación y por la imposibilidad sobreviniente, aun cuando ella obedezca a causa que no le sea imputable. Puede sustraerse a esta responsabilidad probando que ha incurrido en retraso sin culpa, o que la causa no imputable habría afectado la prestación; aunque se hubiese cumplido oportunamente.

Artículo 1337 Indemnización en caso de mora que inutiliza la obligación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando por efecto de la morosidad del deudor, la obligación resultase sin utilidad para el acreedor, éste puede rehusar su ejecución y exigir el pago de la indemnización de daños y perjuicios compensatorios.

Artículo 1338 Mora del acreedor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acreedor incurre en mora cuando sin motivo legítimo se niega a aceptar la prestación ofrecida o no cumple con practicar los actos necesarios para que se pueda ejecutar la obligación.

Artículo 1339 Indemnización por mora del acreedor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acreedor en mora queda obligado a indemnizar los daños y perjuicios derivados de su retraso.

Artículo 1340 Riesgo por imposibilidad de cumplimiento de obligación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acreedor en mora asume los riesgos por la imposibilidad de cumplimiento de la obligación, salvo que obedezca a dolo o culpa inexcusable del deudor.

CAPITULO TERCERO Obligaciones con Cláusula Penal
################################################

Artículo 1341 Cláusula penal compensatoria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El pacto por el que se acuerda que, en caso de incumplimiento, uno de los contratantes queda obligado al pago de una penalidad,  tiene el efecto de limitar el resarcimiento a esta prestación y a que se devuelva la contraprestación, si la hubiere; salvo que se haya estipulado la indemnización del daño ulterior. En este último caso, el deudor deberá pagar el íntegro de la penalidad, pero ésta se computa como parte de los daños y perjuicios si fueran mayores.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1342 Exigibilidad de la penalidad y de la obligación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando la cláusula penal se estipula para el caso de mora o en seguridad de un pacto determinado, el acreedor tiene derecho para exigir, además de la penalidad, el cumplimiento de la obligación.

Artículo 1343 Exigibilidad de pena
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para exigir la pena no es necesario que el acreedor pruebe los daños y perjuicios sufridos. Sin embargo, ella sólo puede exigirse cuando el incumplimiento obedece a causa imputable al deudor, salvo pacto en contrario.

Artículo 1344 Oportunidad de estipulación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La cláusula penal puede ser estipulada conjuntamente con la obligación o por acto posterior.

Artículo 1345 Accesoriedad de cláusula penal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La nulidad de la cláusula penal no origina la de la obligación principal.

Artículo 1346 Reducción judicial de la pena
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El juez, a solicitud del deudor, puede reducir equitativamente la pena cuando sea manifiestamente excesiva o cuando la obligación principal hubiese sido en parte o irregularmente cumplida.

Artículo 1347 Cláusula penal divisible
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cada uno de los deudores o de los herederos del deudor está obligado a satisfacer la pena en proporción a su parte, siempre que la cláusula penal sea divisible, aunque la obligación sea indivisible.

Artículo 1348 Cláusula penal indivisible
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la cláusula penal es indivisible, cada uno de los deudores y de sus herederos queda obligado a satisfacer íntegramente la pena.

Artículo 1349 Cláusula penal solidaria y divisible
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la cláusula penal fuese solidaria, pero divisible, cada uno de los deudores queda obligado a satisfacerla íntegramente.

En caso de muerte de un codeudor, la penalidad se divide entre sus herederos en proporción a las participaciones que les corresponda en la herencia.

Artículo 1350 Derecho de codeudores no culpables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los codeudores que no fuesen culpables tienen expedito su derecho para reclamar de aquél que dio lugar a la aplicación de la pena.
