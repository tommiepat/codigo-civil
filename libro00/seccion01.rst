=============
SECCION UNICA
=============

TITULO UNICO
============

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA


Artículo  I.  Abrogación de la ley
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La ley se deroga sólo por otra ley.

La derogación se produce por declaración expresa, por incompatibilidad entre la nueva ley y la anterior o cuando la materia de ésta es íntegramente regulada por aquélla.

Por la derogación de una ley no recobran vigencia las que ella hubiere derogado.

Artículo II.- La ley no ampara el abuso del derecho. El interesado puede exigir la adopción de las medidas necesarias para evitar o suprimir el abuso y, en su caso, la indemnización que corresponda. (*)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(*) Artículo modificado por el Artículo 5 del Decreto Ley N° 25940, publicado el 11 diciembre 1992, cuyo texto es el siguiente:

"Artículo II.- La ley no ampara el ejercicio ni la omisión abusivos de un derecho. Al demandar indemnización u otra pretensión, el interesado puede solicitar las medidas cautelares apropiadas para evitar o suprimir provisionalmente el abuso."

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993. La misma que recoge las modificaciones hechas por la Primera Disposición Modificatoria del Decreto Legislativo Nº 768, publicado el 04 marzo 1992 y la del Artículo 5 del Decreto Ley N° 25940, publicado el 11 diciembre 1992, cuyo texto es el siguiente:

Ejercicio abusivo del derecho

"Artículo II.- La ley no ampara el ejercicio ni la omisión abusivos de un derecho. Al demandar indemnización u otra pretensión, el interesado puede solicitar las medidas cautelares apropiadas para evitar o suprimir provisionalmente el abuso."

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA


Artículo  III.  Aplicación de la ley en el tiempo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La ley se aplica a las consecuencias de las relaciones y situaciones jurídicas existentes. No tiene fuerza ni efectos retroactivos, salvo las excepciones previstas en la Constitución Política del Perú.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA


Artículo  IV.  Aplicación analógica de la ley
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La ley que establece excepciones o restringe derechos no se aplica por analogía.


Artículo  V.  Orden público, buenas costumbres y nulidad del acto jurídico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nulo el acto jurídico contrario a las leyes que interesan al  orden público o a las buenas costumbres.


Artículo  VI.  Interés para obrar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para ejercitar o contestar una acción es necesario tener legítimo interés económico o moral.

El interés moral autoriza la acción sólo cuando se refiere directamente al agente o a su familia, salvo disposición expresa de la ley.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES


Artículo  VII.  Aplicación de norma pertinente  por el juez
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los jueces tienen la obligación de aplicar la norma jurídica pertinente, aunque no haya sido invocada en la demanda.


Artículo  VIII.  Obligación de suplir los defectos o deficiencias de la ley
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los jueces no pueden dejar de administrar justicia por defecto o deficiencia de la ley. En tales casos, deben aplicar los principios generales del derecho y, preferentemente, los que inspiran el derecho peruano.


Artículo  IX.  Aplicación supletoria del Código Civil
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las disposiciones del Código Civil se aplican supletoriamente a las relaciones y situaciones jurídicas reguladas por otras leyes, siempre que no sean incompatibles con su naturaleza.


Artículo  X.  Vacíos de la ley
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La Corte Suprema de Justicia, el Tribunal de Garantías Constitucionales (*) y el Fiscal de la Nación están obligados a dar cuenta al Congreso de los vacíos o defectos de la legislación.

Tienen la misma obligación los jueces y fiscales respecto de sus correspondientes superiores.

(*)  La referencia al Tribunal de Garantías Constitucionales debe entenderse efectuada al Tribunal Constitucional.
