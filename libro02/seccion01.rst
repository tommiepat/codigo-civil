===========================
SECCION UNICA Acto Juridico
===========================

PROCESOS CONSTITUCIONALES (link en SPIJ)
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)

TITULO I Disposiciones Generales
================================

Capítulo único
##############

Artículo 140 Noción de Acto Jurídico: elementos esenciales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto jurídico es la manifestación de voluntad destinada a crear, regular, modificar o extinguir relaciones jurídicas. Para su validez se requiere:

1.- Agente capaz. (*)

(*) Numeral modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

"1.- Plena capacidad de ejercicio, salvo las restricciones contempladas en la ley.”
2.- Objeto física y jurídicamente posible.
3.- Fin lícito.
4.- Observancia de la forma prescrita bajo sanción de nulidad.

Artículo 141 
~~~~~~~~~~~~~

La manifestación de voluntad puede ser expresa o tácita. Es expresa cuando se formula oralmente, por escrito o por cualquier otro medio directo. Es tácita, cuando la voluntad se infiere indubitablemente de una actitud o de circunstancias de comportamiento que revelan su existencia.

No puede considerarse que existe manifestación tácita cuando la ley exige declaración expresa o cuando el agente formula reserva o declaración en contrario. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27291, publicada el 24 junio 2000, cuyo texto es el siguiente:

"Artículo 141.- Manifestación de voluntad
La manifestación de voluntad puede ser expresa o tácita. Es expresa cuando se realiza en forma oral o escrita, a través de cualquier medio directo, manual, mecánico, electrónico u otro análogo. Es tácita cuando la voluntad se infiere indubitablemente de una actitud o de circunstancias de comportamiento que revelan su existencia.

No puede considerarse que existe manifestación tácita cuando la ley exige declaración expresa o cuando el agente formula reserva o declaración en contrario." (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

“Artículo 141.- Manifestación de voluntad

La manifestación de voluntad puede ser expresa o tácita. Es expresa cuando se realiza en forma oral, escrita, a través de cualquier medio directo, manual, mecánico, digital, electrónico, mediante la lengua de señas o algún medio alternativo de comunicación, incluyendo el uso de ajustes razonables o de los apoyos requeridos por la persona.

Es tácita cuando la voluntad se infiere indubitablemente de una actitud o conductas reiteradas en la historia de vida que revelan su existencia.

No puede considerarse que existe manifestación tácita cuando la ley exige declaración expresa o cuando el agente formula reserva o declaración en contrario.”

"Artículo 141-A.- Formalidad

En los casos en que la ley establezca que la manifestación de voluntad deba hacerse a través de alguna formalidad expresa o requiera de firma, ésta podrá ser generada o comunicada a través de medios electrónicos, ópticos o cualquier otro análogo.

Tratándose de instrumentos públicos, la autoridad competente deberá dejar constancia del medio empleado y conservar una versión íntegra para su ulterior consulta." (*)

(*) Artículo adicionado por el Artículo 2 de la Ley Nº 27291, publicada el 24 junio 2000.

CONCORDANCIAS:     D.LEG.N° 1310, Art. 3 (Simplificación para la emisión, remisión y conservación de documentos en materia laboral)

Artículo 142 El silencio
~~~~~~~~~~~~~~~~~~~~~~~~

El silencio importa manifestación de voluntad cuando la ley o el convenio le atribuyen ese significado.

TITULO II Forma del Acto Jurídico
=================================

Capítulo único
##############

Artículo 143 Libertad de forma
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando la ley no designe una forma específica para un acto jurídico, los interesados pueden usar la que juzguen conveniente.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)

Artículo 144 Forma ad probationem y ad solemnitatem
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando la ley impone una forma y no sanciona con nulidad su inobservancia, constituye sólo un medio de prueba de la existencia del acto.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)

TITULO III Representación
=========================

Capítulo único
##############

Artículo 145 Origen de la representación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto jurídico puede ser realizado mediante representante, salvo disposición contraria de la ley.

La facultad de representación la otorga el interesado o la confiere la ley.

Artículo 146 Representación conyugal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se permite la representación entre cónyuges. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 147 Pluralidad de representantes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando son varios los representantes se presume que lo son indistintamente, salvo que expresamente se establezca que actuarán conjunta o sucesivamente o que estén específicamente designados para practicar actos diferentes.

Artículo 148 Responsabilidad solidaria de los representantes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si son dos o más los representantes, éstos quedan obligados solidariamente frente al representado, siempre que el poder se haya otorgado por acto único y para un objeto de interés común.

Artículo 149 Revocación del poder
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El poder puede ser revocado en cualquier momento.

Artículo 150 Pluralidad de representados
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La revocación del poder otorgado por varios representados para un objeto de interés común, produce efecto sólo si es realizada por todos.

Artículo 151 Designación de nuevo representante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La designación de nuevo representante para el mismo acto o la ejecución de éste por parte del representado, importa la revocación del poder anterior. Esta produce efecto desde que se le comunica al primer representante.

Artículo 152 Comunicación de la revocación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La revocación debe comunicarse también a cuantos intervengan o sean interesados en el acto jurídico.

La revocación comunicada sólo al representante no puede ser opuesta a terceros que han contratado ignorando esa revocación, a menos que ésta haya sido inscrita.

Quedan a salvo los derechos del representado contra el representante.

Artículo 153 Poder irrevocable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El poder es irrevocable siempre que se estipule para un acto especial o por tiempo limitado o cuando es otorgado en interés común del representado y del representante o de un tercero.

El plazo del poder irrevocable no puede ser mayor de un año.
Artículo 154 
~~~~~~~~~~~~~
Renuncia del representante
El representante puede renunciar a la representación comunicándolo al representado. El representante está obligado a continuar con la representación hasta su reemplazo, salvo impedimento grave o justa causa.

El representante puede apartarse de la representación si notificado el representado de su renuncia, transcurre el plazo de treinta días más el término de la distancia, sin haber sido reemplazado.

Artículo 155 Poder general y especial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El poder general sólo comprende los actos de administración.

El poder especial comprende los actos para los cuales ha sido conferido.

Artículo 156 Poder por escritura pública para actos de disposición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para disponer de la propiedad del representado o gravar sus bienes, se requiere que el encargo conste en forma indubitable y por escritura pública, bajo sanción de nulidad.

Artículo 157 Carácter personal de la representación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El representante debe desempeñar personalmente el encargo, a no ser que se le haya facultado expresamente la sustitución.

Artículo 158 Sustitución y responsabilidad del representante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El representante queda exento de toda responsabilidad cuando hace la sustitución en la persona que se le designó. Si no se señaló en el acto la persona del sustituto, pero se concedió al representante la facultad de nombrarlo, éste es responsable cuando incurre en culpa inexcusable en la elección. El representante responde de las instrucciones que imparte al sustituto.

El representado puede accionar directamente contra el sustituto.

Artículo 159 Revocación del sustituto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La sustitución puede ser revocada por el representante, reasumiendo el poder, salvo pacto distinto.

Artículo 160 Representación directa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto jurídico celebrado por el representante, dentro de los límites de las facultades que se le haya conferido, produce efecto directamente respecto del representado.

Artículo 161 Ineficacia del acto jurídico por exceso de facultades
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto jurídico celebrado por el representante excediendo los límites de las facultades que se le hubiere conferido, o violándolas, es ineficaz con relación al representado, sin perjuicio de las responsabilidades que resulten frente a éste y a terceros.

También es ineficaz ante el supuesto representado el acto jurídico celebrado por persona que no tiene la representación que se atribuye.

Artículo 162 Ratificación del acto jurídico por el representado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos previstos por el artículo 161, el acto jurídico puede ser ratificado por el representado observando la forma prescrita para su celebración.

La ratificación tiene efecto retroactivo, pero queda a salvo el derecho de tercero.

El tercero y el que hubiese celebrado el acto jurídico como representante podrán resolver el acto jurídico antes de la ratificación, sin perjuicio de la indemnización que corresponda.

La facultad de ratificar se trasmite a los herederos.

Artículo 163 Anulabilidad del acto jurídico por vicios de la voluntad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto jurídico es anulable si la voluntad del representante hubiere sido viciada. Pero cuando el contenido del acto jurídico fuese total o parcialmente determinado, de modo previo, por el representado, el acto es anulable solamente si la voluntad de éste fuere viciada respecto de dicho contenido.

Artículo 164 Manifestación de la calidad de representante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El representante está obligado a expresar en todos los actos que celebre que procede a nombre de su representado y, si fuere requerido, a acreditar sus facultades.

Artículo 165 Presunción legal de representación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se presume que el dependiente que actúa en establecimientos abiertos al público tiene poder de representación de su principal para los actos que ordinariamente se realizan en ellos.

Artículo 166 Anulabilidad de acto jurídico del representante consigo mismo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es anulable el acto jurídico que el representante concluya consigo mismo, en nombre propio o como representante de otro, a menos que la ley lo permita, que el representado lo hubiese autorizado específicamente, o que el contenido del acto jurídico hubiera sido determinado de modo que excluya la posibilidad de un conflicto de intereses.

El ejercicio de la acción le corresponde al representado.

Artículo 167 Poder especial para actos de disposición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los representantes legales requieren autorización expresa para realizar los siguientes actos sobre los bienes del representado:

1.- Disponer de ellos o gravarlos.

2.- Celebrar transacciones.

3.- Celebrar compromiso arbitral.

4.- Celebrar los demás actos para los que la ley o el acto jurídico exigen autorización especial.

TITULO IV Interpretación del Acto Jurídico
==========================================

Capítulo único
##############

Artículo 168 Interpretación objetiva
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto jurídico debe ser interpretado de acuerdo con lo que se haya expresado en él y según el principio de la buena fe.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)

Artículo 169 Interpretación sistemática
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las cláusulas de los actos jurídicos se interpretan las unas por medio de las otras, atribuyéndose a las dudosas el sentido que resulte del conjunto de todas.

Artículo 170 Interpretación integral
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las expresiones que tengan varios sentidos deben entenderse en el más adecuado a la naturaleza y al objeto del acto.

TITULO V Modalidades del Acto Jurídico
======================================

Capítulo único
##############

Artículo 171 Invalidación del acto por condiciones impropias
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La condición suspensiva ilícita y la física o jurídicamente imposible invalidan el acto.

La condición resolutoria ilícita y la física o jurídicamente imposible se consideran no puestas.

Artículo 172 Nulidad del acto jurídico sujeto a voluntad del deudor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nulo el acto jurídico cuyos efectos están subordinados a condición suspensiva que dependa de la exclusiva voluntad del deudor.

Artículo 173 Actos realizables del adquiriente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pendiente la condición suspensiva, el adquiriente puede realizar actos conservatorios.

El adquirente de un derecho bajo condición resolutoria puede ejercitarlo pendiente ésta, pero la otra parte puede realizar actos conservatorios.

El deudor puede repetir lo que hubiese pagado antes del cumplimiento de la condición suspensiva o resolutoria.

Artículo 174 Indivisibilidad de la condición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cumplimiento de la condición es indivisible, aunque consista en una prestación divisible.

Cumplida en parte la condición, no es exigible la obligación, salvo pacto en contrario.

Artículo 175 Condición negativa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la condición es que no se realice cierto acontecimiento dentro de un plazo, se entenderá cumplida desde que vence el plazo, o desde que llega a ser cierto que el acontecimiento no puede realizarse.

Artículo 176 Cumplimiento e incumplimiento de la condición por mala fe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se impidiese de mala fe el cumplimiento de la condición por la parte en cuyo detrimento habría de realizarse, se considerará cumplida.

Al contrario, se considerará no cumplida, si se ha llevado a efecto de mala fe por la parte a quien aproveche tal cumplimiento.

Artículo 177 Irretroactividad de la condición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La condición no opera retroactivamente, salvo pacto en contrario.

Artículo 178 Efectos de plazos suspensivo y resolutorio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el plazo es suspensivo, el acto no surte efecto mientras se encuentre pendiente. Cuando el plazo es resolutorio, los efectos del acto cesan a su vencimiento.

Antes del vencimiento del plazo, quien tenga derecho a recibir alguna prestación puede ejercitar las acciones conducentes a la cautela de su derecho.

Artículo 179 Beneficio del plazo suspensivo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El plazo suspensivo se presume establecido en beneficio del deudor, a no ser que del tenor del instrumento o de otras circunstancias, resultase haberse puesto en favor del acreedor o de ambos.

Artículo 180 Derecho de repetición por pago anticipado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El deudor que pagó antes del vencimiento del plazo suspensivo no puede repetir lo pagado. Pero, si pagó por ignorancia del plazo, tiene derecho a la repetición.

Artículo 181 Caducidad de plazo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El deudor pierde el derecho a utilizar el plazo:

1.- Cuando resulta insolvente después de contraída la obligación, salvo que garantice la deuda.

"Se presume la insolvencia del deudor si dentro de los quince días de su emplazamiento judicial, no garantiza la deuda o no señala bienes libres de gravamen por valor suficiente para el cumplimiento de su prestación." (*)

(*) Párrafo agregado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993.

2.- Cuando no otorgue al acreedor las garantías a que se hubiese comprometido.

3.- Cuando las garantías disminuyeren por acto propio del deudor, o desaparecieren por causa no imputable a éste, a menos que sean inmediatamente sustituídas por otras equivalentes, a satisfacción del acreedor.

"La pérdida del derecho al plazo por las causales indicadas en los incisos precedentes, se declara a petición del interesado y se tramita como proceso sumarísimo. Son especialmente procedentes las medidas cautelares destinadas a asegurar la satisfacción del crédito." (*)

(*) Párrafo agregado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993.

Artículo 182 Plazo judicial para cumplimiento del acto jurídico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el acto no señala plazo, pero de su naturaleza y circunstancias se dedujere que ha querido concederse al deudor, el juez fija su duración.

También fija el juez la duración del plazo cuya determinación haya quedado a voluntad del deudor o un tercero y éstos no lo señalaren.

El procedimiento es el de menor cuantía. (*)

(*) Último párrafo sustituido por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"La demanda se tramita como proceso sumarísimo."

Artículo 183 Reglas para cómputo del plazo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El plazo se computa de acuerdo al calendario gregoriano, conforme a las siguientes reglas:

1.- El plazo señalado por días se computa por días naturales, salvo que la ley o el acto jurídico establezcan que se haga por días hábiles.

2.- El plazo señalado por meses se cumple en el mes del vencimiento y en el día de éste correspondiente a la fecha del mes inicial. Si en el mes de vencimiento falta tal día, el plazo se cumple el último día de dicho mes.

3.- El plazo señalado por años se rige por las reglas que establece el inciso 2.

4.- El plazo excluye el día inicial e incluye el día del vencimiento.

5.- El plazo cuyo último día sea inhábil, vence el primer día hábil siguiente.

Artículo 184 Reglas extensivas al plazo legal o convencional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las reglas del artículo 183 son aplicables a todos los plazos legales o convencionales, salvo disposición o acuerdo diferente.

Artículo 185 Exigibilidad del cumplimiento del cargo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cumplimiento del cargo puede ser exigido por el imponente o por el beneficiario. Cuando el cumplimiento del cargo sea de interés social, su ejecución puede ser exigida por la entidad a la que concierna.

Artículo 186 Fijación judicial del plazo para cumplimiento del cargo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si no hubiese plazo para la ejecución del cargo, éste debe cumplirse en el que el juez señale.

El procedimiento es el de menor cuantía. (*)

(*) Último párrafo sustituido por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"La demanda se tramita como proceso sumarísimo."

Artículo 187 Inexigibilidad del cargo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El gravado con el cargo no está obligado a cumplirlo en la medida en que exceda el valor de la liberalidad.

Artículo 188 Transmisibilidad del cargo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La obligación de cumplir los cargos impuestos para la adquisición de un derecho pasa a los herederos del que fue gravado con ellos, a no ser que sólo pudiesen ser cumplidos por él, como inherentes a su persona.

En este caso, si el gravado muere sin cumplir los cargos, la adquisición del derecho queda sin efecto, volviendo los bienes al imponente de los cargos o a sus herederos.

Artículo 189 Imposibilidad e ilicitud del cargo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el hecho que constituye el cargo es ilícito o imposible, o llega a serlo, el acto jurídico subsiste sin cargo alguno.

TITULO VI Simulación del Acto Jurídico
======================================

Capítulo único
##############

Artículo 190 Simulación absoluta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Por la simulación absoluta se aparenta celebrar un acto jurídico cuando no existe realmente voluntad para celebrarlo.

Artículo 191 Simulación relativa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando las partes han querido concluir un acto distinto del aparente, tiene efecto entre ellas el acto ocultado, siempre que concurran los requisitos de sustancia y forma y no perjudique el derecho de tercero.

Artículo 192 Simulación parcial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La norma del artículo 191 es de aplicación cuando en el acto se hace referencia a datos inexactos o interviene interpósita persona.

Artículo 193 Acción de nulidad de acto simulado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción para solicitar la nulidad del acto simulado puede ser ejercitada por cualquiera de las partes o por el tercero perjudicado, según el caso.

Artículo 194 Inoponibilidad de la simulación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La simulación no puede ser opuesta por las partes ni por los terceros perjudicados a quien de buena fe y a título oneroso haya adquirido derechos del titular aparente.

TITULO VII Fraude del Acto Jurídico
===================================

Capítulo único
##############

Artículo 195 Accion pauliana
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acreedor, aun cuando el crédito esté sujeto a condición o a plazo, puede pedir que se declaren ineficaces respecto de él los actos de disposición del patrimonio por los cuales el deudor origine perjuicio a su derecho, cuando concurran las condiciones siguientes:

1. Que el deudor tenga conocimiento del perjuicio que el acto origina a los derechos del acreedor o, tratándose de acto anterior al nacimiento del crédito, que el acto esté dolosamente preordenado a fin de perjudicar la satisfacción del futuro crédito.

2. Que, además, tratándose de actos a título oneroso, el tercero tenga conocimiento del perjuicio causado a los derechos del acreedor, y, en el caso del acto anterior al nacimiento del crédito, que haya conocido la preordenación dolosa. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Acción pauliana

"Artículo 195.- El acreedor, aunque el crédito esté sujeto a condición o a plazo, puede pedir que se declaren ineficaces respecto de él los actos gratuitos del deudor por los que renuncie a derechos o con los que disminuya su patrimonio conocido y perjudiquen el cobro del crédito. Se presume la existencia de perjuicio cuando del acto del deudor resulta la imposibilidad de pagar íntegramente la prestación debida, o se dificulta la posibilidad de cobro.

Tratándose de acto a título oneroso deben concurrir, además, los siguientes requisitos:

1.- Si el crédito es anterior al acto de disminución patrimonial, que el tercero haya tenido conocimiento del perjuicio a los derechos del acreedor o que, según las circunstancias, haya estado en razonable situación de conocer o de no ignorarlos y el perjuicio eventual de los mismos.

2.- Si el acto cuya ineficacia se solicita fuera anterior al surgimiento del crédito, que el deudor y el tercero lo hubiesen celebrado con el propósito de perjudicar la satisfacción del crédito del futuro acreedor. Se presume dicha intención en el deudor cuando ha dispuesto de bienes de cuya existencia había informado por escrito al futuro acreedor. Se presume la intención del tercero cuando conocía o estaba en aptitud de conocer el futuro crédito y que el deudor carece de otros bienes registrados.

Incumbe al acreedor la prueba sobre la existencia del crédito y, en su caso, la concurrencia de los requisitos indicados en los incisos 1 y 2 de este artículo. Corresponde al deudor y al tercero la carga de la prueba sobre la inexistencia del perjuicio, o sobre la existencia de bienes libres suficientes para garantizar la satisfacción del crédito."

CONCORDANCIAS:     R. Nº 248-2008-SUNARP-SN, Art. 112 (Acción pauliana)
R.N° 097-2013-SUNARP-SN, Art. 145 (Acción pauliana)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)
PROCESOS CONSTITUCIONALES (link en SPIJ)

Artículo 196 Onerosidad de las garantías
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para los efectos del artículo 195, se considera que las garantías, aún por deudas ajenas, son actos a título oneroso si ellas son anteriores o simultáneas con el crédito garantizado.

Artículo 197 Protección al sub adquriente de buena fe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La declaración de ineficacia del acto no perjudica los derechos adquiridos a título oneroso por los terceros subadquirientes de buena fe.

Artículo 198 Improcedencia de la declaración de ineficacia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No procede la declaración de ineficacia cuando se trata del cumplimiento de una deuda vencida, si ésta consta en documento de fecha cierta.

Artículo 199 Acción oblicua
~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acreedor puede ejercitar frente a los terceros adquirentes las acciones que le correspondan sobre los bienes objeto del acto ineficaz.

El tercero adquirente que tenga frente al deudor derechos de crédito pendientes de la declaración de ineficacia, no puede concurrir sobre el producto de los bienes que han sido objeto del acto ineficaz, sino después que el acreedor haya sido satisfecho.

Artículo 200 
~~~~~~~~~~~~~

Quedan a salvo las disposiciones pertinentes en materia de quiebra. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Único Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Ineficacia de acto jurídico gratuito u oneroso

"Artículo 200.- La ineficacia de los actos gratuitos se tramita como proceso sumarísimo; la de los actos onerosos como proceso de conocimiento. Son especialmente procedentes las medidas cautelares destinadas a evitar que el perjuicio resulte irreparable.

Quedan a salvo las disposiciones pertinentes en materia de quiebra."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)

TITULO VIII Vicios de la Voluntad
=================================

Capítulo único
##############

CONCORDANCIAS:     D.S. N° 052-2008-PCM, Reglamento, Art. 8

Artículo 201 Requisitos de error
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El error es causa de anulación del acto jurídico cuando sea esencial y conocible por la otra parte.

Artículo 202 Error esencial
~~~~~~~~~~~~~~~~~~~~~~~~~~~

El error es esencial:

1.- Cuando recae sobre la propia esencia o una cualidad del objeto del acto que, de acuerdo con la apreciación general o en relación a las circunstancias, debe considerarse determinante de la voluntad.

2.- Cuando recae sobre las cualidades personales de la otra parte, siempre que aquéllas hayan sido determinantes de la voluntad.

3.- Cuando el error de derecho haya sido la razón única o determinante del acto.

Artículo 203 Error conocible
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El error se considera conocible cuando, en relación al contenido, a las circunstancias del acto o a la calidad de las partes, una persona de normal diligencia hubiese podido advertirlo.

Artículo 204 Rectificación del acto jurídico por error de cálculo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El error de cálculo no da lugar a la anulación del acto sino solamente a rectificación, salvo que consistiendo en un error sobre la cantidad haya sido determinante de la voluntad.

Artículo 205 Anulabilidad del acto jurídico por error en el motivo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El error en el motivo sólo vicia el acto cuando expresamente se manifiesta como su razón determinante y es aceptado por la otra parte.

Artículo 206 Improcedencia de la anulabilidad por error rectificado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La parte que incurre en error no puede pedir la anulación del acto si, antes de haber sufrido un perjuicio, la otra ofreciere cumplir conforme al contenido y a las modalidades del acto que aquélla quiso concluir.

Artículo 207 Improcedencia de indemnización por error
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La anulación del acto por error no da lugar a indemnización entre las partes.

Artículo 208 Casos en que el error en la declaración vicia el acto jurídico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las disposiciones de los artículos 201 a 207 también se aplican, en cuanto sean pertinentes, al caso en que el error en la declaración se refiera a la naturaleza del acto, al objeto principal de la declaración o a  la identidad de la persona cuando la consideración a ella hubiese sido el motivo determinante de la voluntad, así como al caso en que la declaración hubiese sido trasmitida inexactamente por quien estuviere encargado de hacerlo.

Artículo 209 Casos en que el error en la declaración no vicia el acto jurídico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El error en la declaración sobre la identidad o la denominación de la persona, del objeto o de la naturaleza del acto, no vicia el acto jurídico, cuando por su texto o las circunstancias se puede identificar a la persona, al objeto o al acto designado.

Artículo 210 Anulabilidad por dolo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El dolo es causa de anulación del acto jurídico cuando el engaño usado por una de las partes haya sido tal que sin él la otra parte no hubiera celebrado el acto.

Cuando el engaño sea empleado por un tercero, el acto es anulable si fue conocido por la parte que obtuvo beneficio de él.

Artículo 211 Dolo incidental
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el engaño no es de tal naturaleza que haya determinado la voluntad, el acto será válido, aunque sin él se hubiese concluido en condiciones distintas; pero la parte que actuó de mala fe responderá de la indemnización de daños y perjuicios.

Artículo 212 Omisión dolosa
~~~~~~~~~~~~~~~~~~~~~~~~~~~

La omisión dolosa produce los mismos efectos que la acción dolosa.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)

Artículo 213 Dolo recíproco
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para que el dolo sea causa de anulación del acto, no debe haber sido empleado por las dos partes.

Artículo 214 Anulabilidad por violencia o intimidación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La violencia o la intimidación son causas de anulación del acto jurídico, aunque hayan sido empleadas por un tercero que no intervenga en él.

Artículo 215 Intimidación
~~~~~~~~~~~~~~~~~~~~~~~~~

Hay intimidación cuando se inspira al agente el fundado temor de sufrir un mal inminente y grave en su persona, su cónyuge, o sus parientes dentro del cuarto grado de consanguinidad o segundo de afinidad o en los bienes de unos u otros.

Tratándose de otras personas o bienes, corresponderá al juez decidir sobre la anulación, según las circunstancias.

Artículo 216 Criterios para calificar la violencia o intimidación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para calificar la violencia o la intimidación debe atenderse a la edad, al sexo, a la condición de la persona y a las demás circunstancias que puedan influir sobre su gravedad.

Artículo 217 Supuestos de no intimidación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La amenaza del ejercicio regular de un derecho y el simple temor reverencial no anulan el acto.

Artículo 218 Nulidad de la renuncia de la acción por vicios de la voluntad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nula la renuncia anticipada a la acción que se funde en error, dolo, violencia o intimidación.

TITULO IX Nulidad del Acto Jurídico
===================================

Capítulo único
##############

PROCESOS CONSTITUCIONALES (link en SPIJ)
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)

Artículo 219 Causales de nulidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto jurídico es nulo:

1.- Cuando falta la manifestación de voluntad del agente.

2.- Cuando se haya practicado por persona absolutamente incapaz, salvo lo dispuesto en el artículo 1358.(*)

(*) Numeral derogado por el Literal a) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

3.- Cuando su objeto es física o jurídicamente imposible o cuando sea indeterminable.

4.- Cuando su fin sea ilícito.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)

5.- Cuando adolezca de simulación absoluta.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)

6.- Cuando no revista la forma prescrita bajo sanción de nulidad.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA (link en SPIJ)

7.- Cuando la ley lo declara nulo. (*) RECTIFICADO POR FE DE ERRATAS

8.- En el caso del artículo V del Título Preliminar, salvo que la ley establezca sanción diversa.

Artículo 220 Alegación de la nulidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La nulidad a que se refiere el artículo 219 puede ser alegada por quienes tengan interés o por el Ministerio Público.

Puede ser declarada de oficio por el juez cuando resulte manifiesta.

No puede subsanarse por la confirmación.

Artículo 221 Causales de anulabilidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto jurídico es anulable:

1.- Por incapacidad relativa del agente. (*)

(*) Numeral modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

"1.- Por capacidad de ejercicio restringida de la persona contemplada en los numerales 1 al 8 del artículo 44."

2.- Por vicio resultante de error, dolo, violencia o intimidación.

PROCESOS CONSTITUCIONALES (link en SPIJ)

3.- Por simulación, cuando el acto real que lo contiene perjudica el derecho de tercero.

4.- Cuando la ley lo declara anulable.

Artículo 222 Efectos de la nulidad por sentencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto jurídico anulable es nulo desde su celebración, por efecto de la sentencia que lo declare.

Esta nulidad se pronunciará a petición de parte y no puede ser alegada por otras personas que aquellas en cuyo beneficio la establece la ley.

Artículo 223 Nulidad de acto plurilateral
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos en que intervengan varios agentes y en los que las prestaciones de cada uno de ellos vayan dirigidas a la consecución de un fin común, la nulidad que afecte al vínculo de una sola de las partes no importará la nulidad del acto, salvo que la participación de ella deba considerarse como esencial, de acuerdo con las circunstancias.

Artículo 224 Nulidad parcial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La nulidad de una o más de las disposiciones de un acto jurídico no perjudica a las otras, siempre que sean separables.

La nulidad de disposiciones singulares no importa la nulidad del acto cuando éstas sean sustituidas por normas imperativas.

La nulidad de la obligación principal conlleva la de las obligaciones accesorias, pero la nulidad de éstas no origina la de la obligación principal.

PROCESOS CONSTITUCIONALES (link en SPIJ)

Artículo 225 Acto y documento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No debe confundirse el acto con el documento que sirve para probarlo. Puede subsistir el acto aunque el documento se declare nulo.

Artículo 226 Incapacidad en beneficio propio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La incapacidad de una de las partes no puede ser invocada por la otra en su propio beneficio, salvo cuando es indivisible el objeto del derecho de la obligación común. (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

“Artículo 226.- Capacidad de ejercicio restringida en beneficio propio

Cuando hubiere más de un sujeto que integre una misma parte, la capacidad de ejercicio restringida del artículo 44 de uno de ellos no puede ser invocada por la otra que integre la misma parte, salvo cuando es indivisible la prestación o su objeto.”

Artículo 227 Anulabilidad por incapacidad relativa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las obligaciones contraídas por los mayores de dieciséis años y menores de dieciocho son anulables, cuando resultan de actos practicados sin la autorización necesaria.

Artículo 228 Repetición del pago al incapaz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nadie puede repetir lo que pagó a un incapaz en virtud de una obligación anulada, sino en la parte que se hubiere convertido en su provecho. (*)

(*) Artículo derogado por el Literal b) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

Artículo 229 Mala fe del incapaz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el incapaz ha procedido de mala fe ocultando su incapacidad para inducir a la celebración del acto, ni él, ni sus herederos o cesionarios, pueden alegar la nulidad.(*)

(*) Artículo derogado por el Literal b) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

TITULO X Confirmación del Acto Jurídico
=======================================

Capítulo único
##############

Artículo 230 Confirmación explícita
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Salvo el derecho de tercero, el acto anulable puede ser confirmado por la parte a quien corresponda la acción de anulación, mediante instrumento que contenga la mención del acto que se quiere confirmar, la causal de anulabilidad y la manifestación expresa de confirmarlo.

Artículo 231 Confirmación por ejecución total o parcial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto queda también confirmado si la parte a quien correspondía la acción de anulación, conociendo la causal, lo hubiese ejecutado en forma total o parcial, o si existen hechos que inequívocamente pongan de manifiesto la intención de renunciar a la acción de anulabilidad.

Artículo 232 Formalidad de la confirmación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La forma del instrumento de confirmación debe tener iguales solemnidades a las establecidas para la validez del acto que se confirma.
