===========================================
SECCION TERCERA Derechos Reales Principales
===========================================

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

TITULO  I Posesión
==================

CAPITULO PRIMERO Disposiciones Generales
########################################

Artículo 896. Noción de posesión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La posesión es el ejercicio de hecho de uno o más poderes inherentes a la propiedad.

PROCESOS CONSTITUCIONALES

Artículo 897. Servidor de la posesión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No es poseedor quien, encontrándose en relación de dependencia respecto a otro, conserva la posesión en nombre de éste y en cumplimiento de órdenes e instrucciones suyas.

Artículo 898. Adición del plazo posesorio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El poseedor puede adicionar a su plazo posesorio el de aquel que le trasmitió válidamente el bien.

Artículo 899. Coposesión
~~~~~~~~~~~~~~~~~~~~~~~~

Existe coposesión cuando dos o más personas poseen un mismo bien conjuntamente.

Cada poseedor puede ejercer sobre el bien actos posesorios, con tal que no signifiquen la exclusión de los demás.

CAPITULO SEGUNDO Adquisición y Conservación de la Posesión
##########################################################

Artículo 900. Adquisición de la posesión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La posesión se adquiere por la tradición, salvo los casos de adquisición originaria que establece la ley.

Artículo 901. Tradición
~~~~~~~~~~~~~~~~~~~~~~~

La tradición se realiza mediante la entrega del bien a quien debe recibirlo o a la persona designada por él o por la ley y con las formalidades que ésta establece.

Artículo 902. Sucedáneos de la tradición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La tradición también se considera realizada:

1.- Cuando cambia el título posesorio de quien está poseyendo.

2.- Cuando se transfiere el bien que está en poder de un tercero. En este caso, la tradición produce efecto en cuanto al tercero sólo desde que es comunicada por escrito.

Artículo 903. Tradición documental
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tratándose de artículos en viaje o sujetos al régimen de almacenes generales, la tradición se realiza por la entrega de los documentos destinados a recogerlos.

Sin embargo, el adquirente de buena fe de objetos no identificables, a quien se hubiere hecho entrega de los mismos, tiene preferencia sobre el tenedor de los documentos, salvo prueba en contrario.

Artículo 904. Conservación de la posesión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se conserva la posesión aunque su ejercicio esté impedido por hechos de naturaleza pasajera.

CAPITULO TERCERO Clases de Posesión y sus Efectos
#################################################

Artículo 905. Posesión inmediata y mediata
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es poseedor inmediato el poseedor temporal en virtud de un título. Corresponde la posesión mediata a quien confirió el título.

CONCORDANCIAS:     D.S. N° 032-2008-VIVIENDA, Art. 40, num. 5
D.S. N° 011-2010-VIVIENDA, Art. 39 Num. 2  y Art. 42 Num. 1) del Reglamento

Artículo 906. Posesión ilegítima de buena fe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La posesión ilegítima es de buena fe cuando el poseedor cree en su legitimidad, por ignorancia o error de hecho o de derecho sobre el vicio que invalida su título.

Artículo 907. Duración de la buena fe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La buena fe dura mientras las circunstancias permitan al poseedor creer que posee legítimamente o, en todo caso, hasta que sea citado en juicio, si la demanda resulta fundada.

Artículo 908. Posesión de buena fe y los frutos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El poseedor de buena fe hace suyos los frutos.

Artículo 909. Responsabilidad del poseedor de mala fe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El poseedor de mala fe responde de la pérdida o detrimento del bien aun por caso fortuito o fuerza mayor, salvo que éste también se hubiese producido en caso de haber estado en poder de su titular.

Artículo 910. Obligación del poseedor de mala fe a restituir frutos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El poseedor de mala fe está obligado a entregar los frutos percibidos y, si no existen, a pagar su valor estimado al tiempo que los percibió o debió percibir.

Artículo 911. Posesión precaria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La posesión precaria es la que se ejerce sin título alguno o cuando el que se tenía ha fenecido.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

CAPITULO CUARTO Presunciones Legales
####################################

Artículo 912. Presunción de propiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El poseedor es reputado propietario, mientras no se pruebe lo contrario. Esta presunción no puede oponerla el poseedor inmediato al poseedor mediato. Tampoco puede oponerse al propietario con derecho inscrito.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 913. Presunción de posesión de accesorios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La posesión de un bien hace presumir la posesión de sus accesorios.

La posesión de un inmueble hace presumir la de los bienes muebles que se hallen en él.

Artículo 914. Presunción de buena fe del poseedor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se presume la buena fe del poseedor, salvo prueba en contrario.

La presunción a que se refiere este artículo no favorece al poseedor del bien inscrito a nombre de otra persona.

Artículo 915. Presunción de posesión continua
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el poseedor actual prueba haber poseído anteriormente, se presume que poseyó en el tiempo intermedio, salvo prueba en contrario.

CAPITULO QUINTO Mejoras
#######################

Artículo 916. Clases de mejoras
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las mejoras son necesarias, cuando tienen por objeto impedir la destrucción o el deterioro del bien.

Son útiles, las que sin pertenecer a la categoría de las necesarias aumentan el valor y la renta del bien .

Son de recreo, cuando sin ser necesarias ni útiles, sirven para ornato, lucimiento o mayor comodidad.

Artículo 917. Derecho al valor o al retiro de las mejoras
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El poseedor tiene derecho a valor actual de las mejoras necesarias y útiles que existan al tiempo de la restitución y a retirar las de recreo que puedan separarse sin daño, salvo que el dueño opte por pagar por su valor actual.

La regla del párrafo anterior no es aplicable a las mejoras hechas después de la citación judicial sino cuando se trata de las necesarias.

Artículo 918. Derecho de retención
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos en que el poseedor debe ser reembolsado de mejoras, tiene el derecho de retención.

Artículo 919. Prescripción de la acción de reembolso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Restituido el bien, se pierde el derecho de separación, y transcurridos dos meses prescribe la acción de reembolso.

CAPITULO SEXTO Defensa Posesoria
################################

Artículo 920. Defensa posesoria extrajudicial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El poseedor puede repeler la fuerza que se emplee contra él y recobrar el bien, sin intervalo de tiempo, si fuere desposeído, pero en ambos casos debe abstenerse de las vías de hecho no justificadas por las circunstancias. (*)

(*) Artículo modificado por el Artículo 67 de la Ley N° 30230, publicada el 12 julio 2014, cuyo texto es el siguiente:

“Defensa posesoria extrajudicial

Artículo 920.-

El poseedor puede repeler la fuerza que se emplee contra él o el bien y recobrarlo, si fuere desposeído. La acción se realiza dentro de los quince (15) días siguientes a que tome conocimiento de la desposesión. En cualquier caso, debe abstenerse de las vías de hecho no justificadas por las circunstancias.

El propietario de un inmueble que no tenga edificación o esta se encuentre en dicho proceso, puede invocar también la defensa señalada en el párrafo anterior en caso de que su inmueble fuera ocupado por un poseedor precario. En ningún caso procede la defensa posesoria si el poseedor precario ha usufructuado el bien como propietario por lo menos diez (10) años.

La Policía Nacional del Perú así como las Municipalidades respectivas, en el marco de sus competencias previstas en la Ley Orgánica de Municipalidades, deben prestar el apoyo necesario a efectos de garantizar el estricto cumplimiento del presente artículo, bajo responsabilidad.

En ningún caso procede la defensa posesoria contra el propietario de un inmueble, salvo que haya operado la prescripción, regulada en el artículo 950 de este Código.”

Artículo 921. Defensa posesoria judicial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Todo poseedor de muebles inscritos y de inmuebles puede utilizar las acciones posesorias y los interdictos. Si su posesión es de más de un año puede rechazar los interdictos que se promuevan contra él.

CAPITULO SEPTIMO Extinción de la Posesión
#########################################

Artículo 922. Causales de extinción de la posesión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La posesión se extingue por:

1.- Tradición

2.- Abandono

3.- Ejecución de resolución judicial

4.- Destrucción total o pérdida del bien.

TITULO  II Propiedad
====================

CAPITULO PRIMERO Disposiciones Generales
########################################

Artículo 923. Noción de propiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La propiedad es el poder jurídico que permite usar, disfrutar, disponer y reivindicar un bien. Debe ejercerse en armonía con el interés social y dentro de los límites de la ley. (*) RECTIFICADO POR FE DE ERRATAS

PROCESOS CONSTITUCIONALES

Artículo 924. Ejercicio abusivo del derecho de propiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Aquél que sufre o está amenazado de un daño porque otro se excede o abusa en el ejercicio de su derecho, puede exigir que se restituya al estado anterior o que se adopten las medidas del caso, sin perjuicio de la indemnización por los daños irrogados.

Artículo 925. Restricciones legales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las restricciones legales de la propiedad establecidas por causa de necesidad y utilidad públicas o de interés social no pueden modificarse ni suprimirse por acto jurídico.

Artículo 926. Restricciones convencionales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las restricciones de la propiedad establecidas por pacto para que surtan efecto respecto a terceros,  deben inscribirse en el registro respectivo.

Artículo 927. Acción reinvindicatoria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción reivindicatoria es imprescriptible. No procede contra aquél que adquirió el bien por prescripción. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 928. Régimen legal de la expropiación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La expropiación se rige por la legislación de la materia.

CONCORDANCIAS:     D.Leg. Nº 313 (Promulgan la Ley General de Expropiación)
D.S. Nº 047-85-PCM (Reglamento del Decreto Legislativo Nº 313)
Ley Nº 27117 (Ley General de Expropiaciones)

CAPITULO SEGUNDO Adquisición de la Propiedad
############################################

SUB-CAPITULO I

Apropiación

Artículo 929. Apropiación de cosas libres
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las cosas que no pertenecen a nadie, como las piedras, conchas u otras análogas que se hallen en el mar o en los ríos o en sus playas u orillas, se adquieren por la persona que las aprehenda, salvo las previsiones de las leyes y reglamentos.

Artículo 930. Apropiación por caza y pesca
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los animales de caza y peces se adquieren por quien los coge, pero basta que hayan caído en las trampas o redes, o que, heridos, sean perseguidos sin interrupción.

Artículo 931. Caza y pesca en propiedad ajena
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No está permitida la caza ni la pesca en predio ajeno, sin permiso del dueño o poseedor, según el caso, salvo que se trate de terrenos no cercados ni sembrados.

Los animales cazados o pescados en contravención a este artículo pertenecen a su titular o poseedor, según el caso, sin perjuicio de la indemnización que corresponda.

Artículo 932. Hallazgo de objetos perdidos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quien halle un objeto perdido está obligado a entregarlo a la autoridad municipal, la cual comunicará el hallazgo mediante anuncio público. Si transcurren tres meses y nadie lo reclama, se venderá en pública subasta y el producto se distribuirá por mitades entre la Municipalidad y quien lo encontró, previa deducción de los gastos.

Artículo 933. Gastos y gratificación por el hallazgo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El dueño que recobre lo perdido está obligado al pago de los gastos y a abonar a quien lo halló la recompensa ofrecida o, en su defecto, una adecuada a las circunstancias. Si se trata de dinero, esa recompensa no será menor a una tercera parte de lo recuperado.

Artículo 934. Búsqueda de tesoro en terreno ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No está permitido buscar tesoro en terreno ajeno cercado, sembrado o edificado, salvo autorización expresa del propietario. El tesoro hallado en contravención de este artículo pertenece íntegramente al dueño del suelo.

Quien buscare tesoro sin autorización expresa del propietario está obligado al pago de la indemnización de daños y perjuicios resultantes.

Artículo 935. División de tesoro encontrado en terreno ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El tesoro descubierto en terreno ajeno no cercado, sembrado o edificado, se divide por partes iguales entre el que lo halla y el propietario del terreno, salvo pacto distinto.

Artículo 936. Protección al Patrimonio Cultural de la Nación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los artículos 934 y 935 son aplicables sólo cuando no sean opuestos a las normas que regulan el patrimonio cultural de la Nación.

SUB-CAPITULO II

Especificación y Mezcla

Artículo 937. Adquisición por especificación y mezcla
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El objeto que se hace de buena fe con materia ajena pertenece al artífice, pagando el valor de la cosa empleada.

La especie que resulta de la unión o mezcla de otras de diferentes dueños, pertenece a éstos en proporción a sus valores respectivos.

SUB-CAPITULO III

Accesión

Artículo 938. Noción de accesión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario de un bien adquiere por accesión lo que se une o adhiere materialmente a él.

Artículo 939. Accesión por aluvión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las uniones de tierra y los incrementos que se forman sucesiva e imperceptiblemente en los fundos situados a lo largo de los ríos o torrentes, pertenecen al propietario del fundo.

Artículo 940. Accesión por avulsión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando la fuerza del río arranca una porción considerable y reconocible en un campo ribereño y lo lleva al de otro propietario ribereño, el primer propietario puede reclamar su propiedad, debiendo hacerlo dentro de dos años del acaecimiento. Vencido este plazo perderá su derecho de propiedad, salvo que el propietario del campo al que se unió la porción arrancada no haya tomado aún posesión de ella.

Artículo 941. Edificación de buena fe en terreno ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando se edifique de buena fe en terreno ajeno, el dueño del suelo puede optar entre hacer suyo lo edificado u obligar al invasor a que le pague el terreno. En el primer caso, el dueño del suelo debe pagar el valor de la edificación, cuyo monto será el promedio entre el costo y el valor actual de la obra. En el segundo caso, el invasor debe pagar el valor comercial actual del terreno.

Artículo 942. Mala fe del propietario del suelo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el propietario del suelo obra de mala fe, la opción de que trata el artículo 941 corresponde al invasor de buena fe, quien en tal caso puede exigir que se le pague el valor actual de la edificación o pagar el valor comercial actual del terreno.

Artículo 943. Edificación de mala fe en terreno ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando se edifique de mala fe en terreno ajeno, el dueño puede exigir la demolición de lo edificado si le causare perjuicio, más el pago de la indemnización correspondiente o hacer suyo lo edificado sin obligación de pagar su valor. En el primer caso la demolición es de cargo del invasor.

Artículo 944. Invasión del suelo colindante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando con una edificación se ha invadido parcialmente y de buena fe el suelo de la propiedad vecina sin que el dueño de ésta se haya opuesto, el propietario del edificio adquiere el terreno ocupado, pagando su valor, salvo que destruya lo construido.

Si la porción ocupada hiciere insuficiente el resto del terreno para utilizarlo en una construcción normal, puede exigirse al invasor que lo adquiera totalmente.

Cuando la invasión a que se refiere este artículo haya sido de mala fe, regirá lo dispuesto en el artículo 943.

Artículo 945. Edificación o siembra con materiales, plantas o semillas ajenas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que de buena fe edifica con materiales ajenos o siembra plantas o semillas ajenas adquiere lo construido o sembrado, pero debe pagar el valor de los materiales, plantas o semillas y la indemnización por los daños y perjuicios causados.

Si la edificación o siembra es hecha de mala fe se aplica el párrafo anterior, pero quien construye o siembra debe pagar el doble del valor de los materiales, plantas o semillas y la correspondiente indemnización de daños y perjuicios.

Artículo 946. Accesión natural
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario de animal hembra adquiere la cría, salvo pacto en contrario. (*) RECTIFICADO POR FE DE ERRATAS

Para que los animales se consideren frutos, basta que estén en el vientre de la madre, aunque no hayan nacido.

En los casos de inseminación artificial realizada con elementos reproductivos procedentes de animal ajeno, el propietario de la hembra adquiere la cría pagando el valor del elemento reproductor, si obra de buena fe, y el triple de dicho valor, si lo hace de mala fe.

SUB-CAPITULO IV

Trasmisión de la Propiedad

Artículo 947. Transferencia de propiedad de bien mueble
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La transferencia de propiedad de una cosa mueble determinada se efectúa con la tradición a su acreedor, salvo disposición legal diferente.

Artículo 948. Adquisición a “non dominus” de bien mueble
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quien de buena fe y como propietario recibe de otro la posesión de una cosa mueble, adquiere el dominio, aunque el enajenante de la posesión carezca de facultad para hacerlo. Se exceptúan de esta regla los bienes perdidos y los adquiridos con infracción de la ley penal.

Artículo 949. Transferencia de propiedad de bien inmueble
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La sola obligación de enajenar un inmueble determinado hace al acreedor propietario de él, salvo disposición legal diferente o pacto en contrario.

SUB-CAPITULO V

Prescripción Adquisitiva

Artículo 950. Prescripción adquisitiva
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La propiedad inmueble se adquiere por prescripción mediante la posesión continua, pacífica y pública como propietario durante diez años.

Se adquiere a los cinco años cuando median justo título y buena fe.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 951. Requisitos de la prescripción adquisitiva de bien mueble
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La adquisición por prescripción de un bien mueble requiere la posesión continua, pacífica y pública como propietario durante dos años si hay buena fe, y por cuatro si no la hay.

Artículo 952. Declaración judicial de prescripción adquisitiva
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quien adquiere un bien por prescripción puede entablar juicio para que se le declare propietario.

La sentencia que accede a la petición es título para la inscripción de la propiedad en el registro respectivo y para cancelar el asiento en favor del antiguo dueño.

Artículo 953. Interrupción de término prescriptorio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se interrumpe el término de la prescripción si el poseedor pierde la posesión o es privado de ella, pero cesa ese efecto si la recupera antes de un año o si por sentencia se le restituye.

CAPITULO TERCERO Propiedad Predial
##################################

SUB-CAPITULO I

Disposiciones generales

Artículo 954. Extensión del derecho de propiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La propiedad del predio se extiende al subsuelo y al sobresuelo, comprendidos dentro de los planos verticales del perímetro superficial y hasta donde sea útil al propietario el ejercicio de su derecho.

La propiedad del subsuelo no comprende los recursos naturales, los yacimientos y restos arqueológicos, ni otros bienes regidos por leyes especiales.

CONCORDANCIAS:     R. N° 340-2008-SUNARP-SN, Directiva, Num. I.8, Cuarto Párrafo

Artículo 955. Propiedad del suelo, subsuelo y sobresuelo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El subsuelo o el sobresuelo pueden pertenecer, total o parcialmente, a propietario distinto que el dueño del suelo. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 956. Acciones por obra que amenaza ruina
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si alguna obra amenaza ruina, quien tenga legítimo interés puede pedir la reparación, la demolición o la adopción de medidas preventivas.

Artículo 957. Régimen de la propiedad predial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La propiedad predial queda sujeta a la zonificación, a los procesos de habilitación y subdivisión y a los requisitos y limitaciones que establecen las disposiciones respectivas.

Artículo 958. Régimen de la propiedad horizontal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La propiedad horizontal se rige por la legislación de la materia.

SUB-CAPITULO II

Limitaciones por razón de Vecindad

Artículo 959. Actos para evitar peligro de propiedades vecinas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario no puede impedir que en su predio se ejecuten actos para servicios provisorios de las propiedades vecinas, que eviten o conjuren un peligro actual o inminente, pero se le indemnizará por los daños y perjuicios causados.

Artículo 960. Paso de materiales de construcción por predio ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si para construir o reparar un edificio es indispensable pasar materiales por predio ajeno o colocar en él andamios, el dueño de éste debe consentirlo, recibiendo indemnización por los daños y perjuicios que se le causen.

Artículo 961. Límites a la explotación industrial del predio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario, en ejercicio de su derecho y especialmente en su trabajo de explotación industrial, debe abstenerse de perjudicar las propiedades contiguas o vecinas, la seguridad, el sosiego y la salud de sus habitantes.

Están prohibidos los humos, hollines, emanaciones, ruidos, trepidaciones y molestias análogas que excedan de la tolerancia que mutuamente se deben los vecinos en atención a las circunstancias.

Artículo 962. Prohibición de abrir o cavar pozos que dañen propiedad vecina
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Al propietario de un inmueble no le está permitido abrir o cavar en su terreno pozos susceptibles de causar ruina o desmoronamiento en la propiedad vecina o de perjudicar las plantaciones en ella existentes y puede ser obligado a guardar las distancias necesarias para la seguridad de los predios afectados, además de la obligación de pagar la indemnización por los daños y perjuicios.

Artículo 963. Obras y depósitos nocivos y peligrosos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si cerca de un lindero se construye horno, chimenea, establo u otros similares o depósito para agua o materias húmedas, penetrantes, explosivas o radioactivas o se instala maquinaria o análogos, deben observarse las distancias y precauciones establecidas por los reglamentos respectivos y, a falta de éstos, las que sean necesarias para preservar la solidez o la salubridad de los predios vecinos. La inobservancia de esta disposición puede dar lugar al cierre o retiro de la obra y a la indemnización de daños y perjuicios.

Artículo 964. Paso de aguas por predio vecino
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario no puede hacer que las aguas correspondientes al predio discurran en los predios vecinos, salvo pacto distinto.

SUB-CAPITULO III

Derechos del Propietario

Artículo 965. Derecho a cercar un predio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario de un predio tiene derecho a cercarlo.

Artículo 966. Obligación de deslinde y amojonamiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario de un predio puede obligar a los vecinos, sean propietarios o poseedores, al deslinde y al amojonamiento.

Artículo 967. Derecho al corte de ramas y raíces invasoras del predio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Todo propietario puede cortar las ramas de los árboles que se extiendan sobre el predio y las raíces que lo invadan. Cuando sea necesario, podrá recurrir a la autoridad municipal o judicial para el ejercicio de estos derechos.

CAPITULO CUARTO Extinción de la Propiedad
#########################################

Artículo 968. Causales de extinción de la propiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La propiedad se extingue por:

1.- Adquisición del bien por otra persona.

2.- Destrucción o pérdida total o consumo del bien.

3.- Expropiación.

4.- Abandono del bien durante veinte años, en cuyo caso pasa el predio al dominio del Estado.

CONCORDANCIAS:     Ley Nº 29415, Arts. 11 y siguientes (Declaración administrativa de abandono)

CAPITULO QUINTO Copropiedad
###########################

SUB-CAPITULO I

Disposiciones Generales

Artículo 969. Noción de Copropiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Hay copropiedad cuando un bien pertenece por cuotas ideales a dos o más personas.

Artículo 970. Presunción de igualdad de cuotas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las cuotas de los propietarios se presumen iguales, salvo prueba en contrario.

El concurso de los copropietarios, tanto en los beneficios como en las cargas, está en proporción a sus cuotas respectivas.

Artículo 971. Decisiones sobre el bien común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las decisiones sobre el bien común se adoptarán por:

1.- Unanimidad, para disponer, gravar o arrendar el bien, darlo en comodato o introducir modificaciones en él.

2.- Mayoría absoluta, para los actos de administración ordinaria. Los votos se computan por el valor de las cuotas.

En caso de empate, decide el juez por la vía incidental.

Artículo 972. Régimen aplicable a la administración judicial de los bienes comunes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La administración judicial de los bienes comunes se rige por el Código de Procedimientos Civiles. (*)

(*)  La referencia al Código de Procedimientos Civiles debe entenderse efectuada al Código Procesal Civil.

Artículo 973. Administración del bien común por uno de los copropietarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cualquiera de los copropietarios puede asumir la administración y emprender los trabajos para la explotación normal del bien, si no está establecida la administración convencional o judicial y mientras no sea solicitada alguna de ellas.

En este caso las obligaciones del administrador serán las del administrador judicial. Sus servicios serán retribuidos con una parte de la utilidad, fijada por el juez y observando el trámite de los incidentes.

SUB-CAPITULO II

Derechos y Obligaciones de los Copropietarios

Artículo 974. Derecho de uso del bien común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cada copropietario tiene derecho a servirse del bien común, siempre que no altere su destino ni perjudique el interés de los demás.

El derecho de usar el bien común corresponde a cada copropietario. En caso de desavenencia el juez regulará el uso, observándose las reglas procesales sobre administración judicial de bienes comunes.

Artículo 975. Indemnización por uso total o parcial del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El copropietario que usa el bien parcial o totalmente con exclusión de los demás, debe indemnizarles en las proporciones que les corresponda, salvo lo dispuesto en el artículo 731.

Artículo 976. Derecho de disfrute
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de disfrutar corresponde a cada copropietario. Estos están obligados a reembolsarse proporcionalmente los provechos obtenidos del bien.

Artículo 977. Disposición de la cuota ideal y sus frutos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cada copropietario puede disponer de su cuota ideal y de los respectivos frutos. Puede también gravarlos.

Artículo 978. Condicionabilidad de la validez de actos de propiedad exclusiva
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si un copropietario practica sobre todo o parte de un bien, acto que importe el ejercicio de propiedad exclusiva, dicho acto sólo será válido desde el momento en que se adjudica el bien o la parte a quien practicó el acto.

Artículo 979. Reinvindicación y defensa del bien común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cualquier copropietario puede revindicar el bien común. Asimismo, puede promover las acciones posesorias, los interdictos, las acciones de desahucio, aviso de despedida y las demás que determine la ley.

CONCORDANCIAS:     R.SMV Nº 013-2013-SMV, Num. 3.2 (Herederos reconocidos)

Artículo 980. Mejoras necesarias y útiles en la copropiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las mejoras necesarias y útiles pertenecen a todos los copropietarios, con la obligación de responder proporcionalmente por los gastos.

Artículo 981. Gastos de conservación y cargas del bien común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Todos los copropietarios están obligados a concurrir, en proporción a su parte, a los gastos de conservación y al pago de los tributos, cargas y gravámenes que afecten al bien común.

Artículo 982. Saneamiento por evicción del bien común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los copropietarios están recíprocamente obligados al saneamiento en caso de evicción, en proporción a la parte de cada uno.

SUB-CAPITULO III

Partición

Artículo 983. Noción de partición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Por la partición permutan los copropietarios, cediendo cada uno el derecho que tiene sobre los bienes que no se le adjudiquen, a cambio del derecho que le ceden en los que se le adjudican.

Artículo 984. Obligatoriedad de la partición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los copropietarios están obligados a hacer partición cuando uno de ellos o el acreedor de cualquiera lo pida, salvo los casos de indivisión forzosa, de acto jurídico o de ley que fije plazo para la partición.

Artículo 985. Imprescriptibilidad de la acción de partición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La acción de partición es imprescriptible y ninguno de los copropietarios ni sus sucesores pueden adquirir por prescripción los bienes comunes.

Artículo 986. Partición convencional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los copropietarios pueden hacer partición por convenio unánime.
La partición convencional puede ser hecha también mediante sorteo.

Artículo 987. Partición convencional especial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si alguno de los copropietarios es incapaz o ausente, puede hacerse partición convencional o por medio de árbitros, pero será aprobada por el juez previa tasación, y dictamen del Ministerio Público, con audiencia del consejo de familia cuando lo haya y lo estime conveniente. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Partición convencional especial

"Artículo 987.- Si alguno de los copropietarios es incapaz o ha sido declarado ausente, la partición convencional se somete a aprobación judicial, acompañando a la solicitud tasación de los bienes por tercero, con firma legalizada notarialmente, así como el documento que contenga el convenio particional, firmado por todos los interesados y sus representantes legales. Puede prescindirse de tasación cuando los bienes tienen cotización en bolsa o mercado análogo, o valor determinado para efectos tributarios. (*)

(*) Extremo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

“Artículo 987.- Partición convencional especial

Si alguno de los copropietarios es una persona contemplada en el artículo 43 o 44 del Código Civil o ha sido declarado ausente, la partición convencional se somete a aprobación judicial, acompañando a la solicitud tasación de los bienes por tercero, con firma legalizada notarialmente, así como el documento que contenga el convenio particional, firmado por todos los interesados y sus representantes legales. Puede prescindirse de tasación cuando los bienes tienen cotización en bolsa o mercado análogo, o valor determinado para efectos tributarios."

La solicitud de aprobación se sujeta al trámite del proceso no contencioso, con citación del Ministerio Público y del consejo de familia, si ya estuviera constituído."

Artículo 988. Partición de bienes indivisibles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los bienes comunes que no son susceptibles de división material pueden ser adjudicados, en común, a dos o más copropietarios que convengan en ello, o se venderán por acuerdo de todos ellos y se dividirá el precio. Si los copropietarios no estuvieran de acuerdo con la adjudicación en común o en la venta contractual, se venderán en pública subasta.

Artículo 989. Derecho de preferencia del copropietario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los copropietarios tienen el derecho de preferencia para evitar la subasta de que trata el artículo 988 y adquirir su propiedad, pagando en dinero el precio de la tasación en las partes que correspondan a los demás copartícipes. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 990. Lesión en la partición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La lesión en la partición se rige por lo dispuesto en los artículos 1447 a 1456.

Artículo 991. Diferimiento o suspensión de la partición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puede diferirse o suspenderse la partición por acuerdo unánime de los copropietarios. Si hubiese copropietarios incapaces, se requerirá autorización judicial, observándose las reglas previstas en el artículo 987.

SUB-CAPITULO IV

Extinción de la Copropiedad

Artículo 992. Causales de extinción de la copropiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La copropiedad se extingue por:

1.- División y partición del bien común.

2.- Reunión de todas las cuotas partes en un solo propietario.

3.- Destrucción total o pérdida del bien.

4.- Enajenación del bien a un tercero.

5.- Pérdida del derecho de propiedad de los copropietarios.

SUB-CAPITULO V

Pacto de Indivisión

Artículo 993. Plazo y efectos del pacto de indivisión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los copropietarios pueden celebrar pacto de indivisión por un plazo no mayor de cuatro años y renovarlo todas las veces que lo juzguen conveniente.

El pacto de indivisión que no consigne plazo se presume que es por cuatro años.

Para que produzca efecto contra terceros, el pacto de indivisión debe inscribirse en el registro correspondiente.

Si median circunstancias graves el juez puede ordenar la partición antes del vencimiento del plazo.

SUB-CAPITULO VI

Medianería

Artículo 994. Presunción de medianería
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las paredes, cercos o zanjas situados entre dos predios se presumen comunes, mientras no se pruebe lo contrario.

Artículo 995. Obtención de medianería
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la pared que separa los predios se ha levantado en terreno de uno de ellos, el vecino puede obtener la medianería pagando la mitad del valor actual de la obra y del suelo ocupado.

En tal caso, puede pedir la supresión de todo lo que sea incompatible con el derecho que le da la medianería.

Artículo 996. Uso de pared medianera
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Todo colindante puede colocar tirantes y vigas en la pared medianera, y servirse de ésta sin deteriorarla, pero no puede abrir en ella ventanas o claraboyas.

Artículo 997. Construcción de pared medianera
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cualquier colindante puede levantar la pared medianera, siendo de su cargo los gastos de la reparación y cualesquiera otros que exigiera la mayor altura.

Artículo 998. Cargas de la medianería
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los colindantes deben contribuir a prorrata para la conservación, reparación o reconstrucción de la pared medianera, a no ser que renuncien a la medianería, hagan o no uso de ella.

TITULO  III Usufructo
=====================

CAPITULO PRIMERO Disposiciones Generales
########################################

Artículo 999. Noción de Usufructo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructo confiere las facultades de usar y disfrutar temporalmente de un bien ajeno.

Pueden excluirse del usufructo determinados provechos y utilidades.

El usufructo puede recaer sobre toda clase de bienes no consumibles, salvo lo dispuesto en los artículo 1018 a 1020.

Artículo 1000. Constitución del usufructo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructo se puede constituir por:

1.- Ley cuando expresamente lo determina.

2.- Contrato o acto jurídico unilateral.

3.- Testamento.

Artículo 1001. Plazo del usufructo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructo es temporal. El usufructo constituido en favor de una persona jurídica no puede exceder de treinta años y cualquier plazo mayor que se fije se reduce a éste.

Tratándose de bienes inmuebles de valor monumental de propiedad del Estado que sean materia de restauración con fondos de personas naturales o jurídicas, el usufructo que constituya el Estado en favor de éstas podrá tener un plazo máximo de noventinueve años.

Artículo 1002. Transferencia o gravamen del usufructo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructo, con excepción del legal, puede ser transferido a título oneroso o gratuito o ser gravado, respetándose su duración y siempre que no haya prohibición expresa.

Artículo 1003. Usufructo del bien expropiado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de expropiación del bien objeto del usufructo, éste recaerá sobre el valor de la expropiación.

Artículo 1004. Usufructo legal sobre productos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el usufructo legal recae sobre los productos a que se refiere el artículo 894, los padres restituirán la mitad de los ingresos netos obtenidos.

Artículo 1005. Régimen de los efectos del usufructo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los efectos del usufructo se rigen por el acto constitutivo y, no estando previstos en éste, por las disposiciones del presente título.

CAPITULO SEGUNDO Deberes y Derechos del Usufructuario
#####################################################

Artículo 1006. Inventario y tasación de bienes por el usufructuario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Al entrar en posesión, el usufructuario hará inventario y tasación de los bienes muebles, salvo que haya sido expresamente eximido de esa obligación por el propietario que no tenga heredero forzoso. El inventario y la tasación serán judiciales cuando se trata del usufructo legal y del testamentario.

Artículo 1007. Obligación del usufructuario de prestar garantía
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructuario está obligado a prestar la garantía señalada en el título constitutivo de su derecho o la que ordene el juez, cuando éste encuentre que puede peligrar el derecho del propietario.

Artículo 1008. Explotación del bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructuario debe explotar el bien en la forma normal y acostumbrada.

Artículo 1009. Prohibición de modificar el bien usufructuado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructuario no debe hacer ninguna modificación sustancial del bien o de su uso.

Artículo 1010. Obligación del usufructuario de pagar tributos y rentas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructuario debe pagar los tributos, las rentas vitalicias y las pensiones de alimentos que graven los bienes.

Artículo 1011. Derecho de subrogación del usufructuario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el usufructuario paga la deuda hipotecaria o el interés que ésta devenga, se subroga en el crédito pagado.

Artículo 1012. Desgaste del bien por disfrute ordinario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructuario no responde del desgaste por el disfrute ordinario.

Artículo 1013. Obligación de reparar el bien usufructuado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructuario está obligado a efectuar las reparaciones ordinarias y, si por su culpa se necesitan obras extraordinarias, debe hacerlas a su costo.

Artículo 1014. Reparaciones ordinarias
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se consideran reparaciones ordinarias las que exijan los desperfectos que procedan del uso normal de los bienes y sean indispensables para su conservación.

El propietario puede exigir judicialmente la ejecución de las reparaciones. El pedido se tramita como incidente.

Artículo 1015. Aplicación supletoria de las normas sobre mejoras
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las reglas sobre mejoras necesarias, útiles y de recreo establecidas para la posesión se aplican al usufructo.

Artículo 1016. Propiedad de frutos pendientes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pertenecen al usufructuario los frutos naturales y mixtos pendientes al comenzar el usufructo; y al propietario, los pendientes a su término.

Artículo 1017. Oposición por infracciones del propietario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario puede oponerse a todo acto del usufructuario que importe una infracción de los artículos 1008 y 1009 y pedir al juez que regule el uso o explotación. El pedido se tramita como incidente.

CAPITULO TERCERO Cuasiusufructo
###############################

Artículo 1018. Usufructo de dinero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructo de dinero sólo da derecho a percibir la renta.

Artículo 1019. Usufructo de un crédito
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructuario de un crédito tiene las acciones para el cobro de la renta y debe ejercitar las acciones necesarias para que el crédito no se extinga.

Artículo 1020. Cobro de capital
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el usufructuario cobra el capital, debe hacerlo conjuntamente con el propietario y en este caso el usufructo recaerá sobre el dinero cobrado.

CAPITULO CUARTO Extinción y modificación del Usufructo
######################################################

Artículo 1021. Causales de extinción del usufructo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructo se extingue por:

1.- Cumplimiento de los plazos máximos que prevé el artículo 1001 o del establecido en el acto constitutivo.

2.- Prescripción resultante del no uso del derecho durante cinco años.

3.- Consolidación.

4.- Muerte o renuncia del usufructuario.

5.- Destrucción o pérdida total del bien.

6.- Abuso que el usufructuario haga de su derecho, enajenando o deteriorando los bienes o dejándolos perecer por falta de reparaciones ordinarias. En este caso el juez declara la extinción.

Artículo 1022. Usufructo a favor de varias personas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructo constituido en favor de varias personas en forma sucesiva se extingue a la muerte de la última.

Si el usufructo fuera constituido en favor de varias personas en forma conjunta, la muerte de alguna de éstas determinará que las demás acrezcan su derecho. Este usufructo también se extingue con la muerte de la última persona. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 1023. Destrucción del bien usufructuado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si la destrucción del bien ocurre por dolo o culpa de un tercero, el usufructo se transfiere a la indemnización debida por el responsable del daño.

Si se destruye el bien dado en usufructo, estando asegurado por el constituyente o el usufructuario, el usufructo se transfiere a la indemnización pagada por el asegurador.

Artículo 1024. Destrucción o pérdida parcial del bien usufructuado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el bien sujeto al usufructo se destruye o pierde en parte, el usufructo se conserva sobre el resto.

Artículo 1025. Usufructo sobre fundo o edificio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el usufructo se establece sobre un fundo del cual forma parte un edificio que llega a destruirse por vetustez o accidente, el usufructuario tiene derecho a gozar del suelo y de los materiales.

Pero si el usufructo se encuentra establecido solamente sobre un edificio que llega a destruirse, el usufructuario no tiene derecho al suelo ni a los materiales, ni al edificio que el propietario reconstruya a su costa.

TITULO  IV Uso y Habitación
===========================

CAPITULO UNICO
##############

Artículo 1026. Régimen legal del derecho de uso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de usar o de servirse de un bien no consumible se rige por las disposiciones del título anterior, en cuanto sean aplicables.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1027. Derecho de habitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el derecho de uso recae sobre una casa o parte de ella para servir de morada, se estima constituido el derecho de habitación.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 1028. Extensión de los derechos de uso y habitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los derechos de uso y habitación se extienden a la familia del usuario, salvo disposición distinta.

Artículo 1029. Carácter personal de los derechos de uso y habitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los derechos de uso y habitación no pueden ser materia de ningún acto jurídico, salvo la consolidación.

TITULO  V Superficie
====================

CAPITULO UNICO
##############

Artículo 1030. Superficie: Noción y plazo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puede constituirse el derecho de superficie por el cual el superficiario goza de la facultad de tener temporalmente una construcción en propiedad separada sobre o bajo la superficie del suelo.

Este derecho no puede durar más de noventinueve años. A su vencimiento, el propietario del suelo adquiere la propiedad de lo construido reembolsando su valor, salvo pacto distinto.

Artículo 1031. Constitución o transmisibilidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de superficie puede constituirse por acto entre vivos o por testamento. Este derecho es trasmisible, salvo prohibición expresa.

Artículo 1032. Extensión del derecho de superficie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de superficie puede extenderse al aprovechamiento de una parte del suelo, no necesaria para la construcción, si dicha parte ofrece ventaja para su mejor utilización.

Artículo 1033. Pervivencia del derecho de superficie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de superficie no se extingue por la destrucción de lo construido.

Artículo 1034. Extinción del derecho de superficie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La extinción del derecho de superficie importa la terminación de los derechos concedidos por el superficiario en favor de tercero.

TITULO  VI Servidumbres
=======================

CAPITULO UNICO
##############

Artículo 1035. Servidumbre legal y convencional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La ley o el propietario de un predio puede imponerle gravámenes en beneficio de otro que den derecho al dueño del predio dominante para practicar ciertos actos de uso del predio sirviente o para impedir al dueño de éste el ejercicio de alguno de sus derechos.

Artículo 1036. Caracteristicas de la servidumbre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las servidumbres son inseparables de ambos predios. Sólo pueden trasmitirse con ellos y subsisten cualquiera sea su propietario.

Artículo 1037. Perpetuidad de la servidumbre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las servidumbres son perpetuas, salvo disposición legal o pacto contrario.

Artículo 1038. Indivisibilidad de la servidumbre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las servidumbres son indivisibles. Por consiguiente, la servidumbre se debe entera a cada uno de los dueños del predio dominante y por cada uno de los del sirviente.

Artículo 1039. División del predio dominante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el predio dominante se divide, la servidumbre subsiste en favor de los adjudicatarios que la necesiten, pero sin exceder el gravamen del predio sirviente.

Artículo 1040. Servidumbres aparentes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sólo las servidumbres aparentes pueden adquirirse por prescripción, mediante la posesión continua durante cinco años con justo título y buena fe o durante diez años sin estos requisitos.

Artículo 1041. Constitución de servidumbre por el usufructuario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usufructuario puede constituir servidumbres por el plazo del usufructo, con conocimiento del propietario. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 1042. Servidumbre de predio sujeto a copropiedad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El predio sujeto a copropiedad sólo puede ser gravado con servidumbres si prestan su asentimiento todos los copropietarios. Si hubiere copropietarios incapaces, se requerirá autorización judicial, observándose las reglas del artículo 987 en cuanto sean aplicables.

El copropietario puede adquirir servidumbres en beneficio del predio común, aunque lo ignoren los demás copropietarios.

Artículo 1043. Extensión y condiciones de la servidumbre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La extensión y demás condiciones de las servidumbres se rigen por el título de su constitución y, en su defecto, por las disposiciones de este Código.

Toda duda sobre la existencia de una servidumbre, sobre su extensión o modo de ejercerla, se interpreta en el sentido menos gravoso para el predio sirviente, pero sin imposibilitar o dificultar el uso de la servidumbre.

Artículo 1044. Obras para ejercicio de servidumbre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A falta de disposición legal o pacto en contrario, el propietario del predio dominante hará a su costo las obras requeridas para el ejercicio de la servidumbre, en el tiempo y forma que sean de menor incomodidad para el propietario del predio sirviente.

Artículo 1045. Conservación de la servidumbre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La servidumbre se conserva por el uso de una persona extraña, si lo hace en consideración al predio dominante.

Artículo 1046. Prohibición de aumentar gravamen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario del predio dominante no puede aumentar el gravamen del predio sirviente por hecho o acto propio.

Artículo 1047. Prohibición de impedir el uso de servidumbre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario del predio sirviente no puede impedir el ejercicio o menoscabar el uso de la servidumbre. Si por razón de lugar o modo la servidumbre le es incómoda, podrá ser variada si no perjudica su uso.

Artículo 1048. Servidumbre sobre bien propio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario de dos predios puede gravar uno con servidumbre en beneficio del otro.

Artículo 1049. Extinción por destrucción total
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las servidumbres se extinguen por destrucción total, voluntaria o involuntaria, de cualquiera de los edificios, dominante o sirviente, sin mengua de las relativas al suelo. Pero reviven por la reedificación, siempre que pueda hacerse uso de ellas.

Artículo 1050. Extinción por falta de uso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las servidumbres se extinguen en todos los casos por el no uso durante cinco años.

Artículo 1051. Servidumbre legal de paso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La servidumbre legal de paso se establece en beneficio de los predios que no tengan salida a los caminos públicos.

Esta servidumbre cesa cuando el propietario del predio dominante adquiere otro que le de salida o cuando se abre un camino que de acceso inmediato a dicho predio.

Artículo 1052. Onerosidad de la servidumbre legal de paso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La servidumbre del artículo 1051 es onerosa. Al valorizársela, deberán tenerse también en cuenta los daños y  perjuicios que resultaren al propietario del predio sirviente.

Artículo 1053. Servidumbre de paso gratuito
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que adquiere un predio enclavado en otro del enajenante adquiere gratuitamente el derecho al paso.

Artículo 1054. Amplitud del camino en el derecho de paso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La amplitud del camino se fijará según las circunstancias.
