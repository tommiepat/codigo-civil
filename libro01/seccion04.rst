===============================================
SECCION CUARTA Comunidades Campesinas y Nativas
===============================================

PROCESOS CONSTITUCIONALES (link en SPIJ)

TITULO UNICO Disposiciones Generales
====================================

Capítulo único
##############

Artículo 134 Noción y fines de las Comunidades Campesinas y Nativas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las comunidades campesinas y nativas son organizaciones tradicionales y estables de interés público, constituidas por personas naturales y cuyos fines se orientan al mejor aprovechamiento de su patrimonio, para beneficio general y equitativo de los comuneros, promoviendo su desarrollo integral.

Están reguladas por legislación especial.

CONCORDANCIAS:     R. Nº 126-2011-SUNARP-SA (Elecciones de la Junta Directiva y Redacción de Estatuto de Comunidades Nativas Inscritas)
R.N° 345-2013-SUNARP-SN (Aprueban la Guía General para la inscripción de los actos y derechos de las Comunidades Nativas)

PROCESOS CONSTITUCIONALES (link en SPIJ)

Artículo 135 Existencia jurídica de las comunidades
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para la existencia legal de las comunidades se requiere, además de la inscripción en el registro respectivo, su reconocimiento oficial.

Artículo 136 Carácter de las tierras de las comunidades
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las tierras de las comunidades son inalienables, imprescriptibles e inembargables, salvo las excepciones establecidas por la Constitución Política del Perú.

Se presume que son propiedad comunal las tierras poseídas de acuerdo al reconocimiento e inscripción de la comunidad.

Artículo 137 Estatuto de las comunidades
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Poder Ejecutivo regula el estatuto de las comunidades, el cual consagra su autonomía económica y administrativa, así como los derechos y obligaciones de sus miembros y las demás normas para su reconocimiento, inscripción, organización y funcionamiento.

Artículo 138 Asamblea General
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La asamblea general es el órgano supremo de las comunidades. Los directivos y representantes comunales son elegidos periódicamente, mediante voto personal, igual, libre, secreto y obligatorio.

Artículo 139 Padrón y catastro de las comunidades
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las comunidades tienen un padrón general actualizado con el nombre, actividad, domicilio y fecha de admisión de cada uno de sus miembros, con indicación de los que ejerzan cargos directivos o representación.

Las comunidades tienen, asimismo, un catastro en el que constan los bienes que integran su patrimonio.

En el padrón general y en el catastro constan también los demás datos que señale la legislación especial.
