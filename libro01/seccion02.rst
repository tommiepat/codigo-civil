==================================
SECCION SEGUNDA Personas Jurídicas
==================================

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

TITULO I Disposiciones Generales
================================

CAPITULO UNICO
##############

CONCORDANCIAS:     Ley N° 26789 (Representación procesal del administrador, representante legal o presidente del Consejo Directivo)

Ley N° 28094 (Ley de Organizaciones Políticas)
D.S. Nº 014-2008-JUS, Art. 13 (De la representación de las personas naturales y jurídicas)


Artículo 76.  Normas que rigen la persona jurídica
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La existencia, capacidad, régimen, derechos, obligaciones y fines de la persona jurídica, se determinan por las disposiciones del presente Código o de las leyes respectivas.

La persona jurídica de derecho público interno se rige por la ley de su creación.


Artículo 77.  Inicio de la persona jurídica
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La existencia de la persona jurídica de derecho privado comienza el día de su inscripción en el registro respectivo, salvo disposición distinta de la ley.

La eficacia de los actos celebrados en nombre de la persona jurídica antes de su inscripción queda subordinada a este requisito y a su ratificación dentro de los tres meses siguientes de haber sido inscrita.

Si la persona jurídica no se constituye o no se ratifican los actos realizados en nombre de ella, quienes los hubieran celebrado son ilimitada y solidariamente responsables frente a terceros.


Artículo 78.  Diferencia entre persona jurídica y sus miembros
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La persona jurídica tiene existencia distinta de sus miembros y ninguno de éstos ni todos ellos tienen derecho al patrimonio de ella ni están obligados a satisfacer sus deudas.


Artículo 79.  Representante de la persona jurídica miembro de otra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La persona jurídica miembro de otra debe indicar quién la representa ante ésta.

TITULO II Asociación
====================

CAPITULO UNICO
##############

CONCORDANCIAS:     R. N° 331-2001-SUNARP-SN  (Acreditación de convocatorias y cómputo de quórum)

Ley N° 28094 (Ley de Organizaciones Políticas)

R. Nº 015-2004-JNE (Reglamento del Registo de Organizaciones Políticas)
R.N° 038-2013-SUNARP-SN (Aprueban el “Reglamento de Inscripciones del Registro de Personas Jurídicas”)
R.N° 0208-2015-JNE (Reglamento del Registro de Organizaciones Políticas)
R.N° 0325-2019-JNE (Aprueban el Reglamento del Registro de Organizaciones Políticas)


Artículo 80.  Noción
~~~~~~~~~~~~~~~~~~~~

La asociación es una organización estable de personas naturales o jurídicas, o de ambas, que a través de una actividad común persigue un fin no lucrativo.

PROCESOS CONSTITUCIONALES


Artículo 81.  Estatuto de la asociación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El estatuto debe constar por escritura pública, salvo disposición distinta de la ley.

Si la asociación es religiosa, su régimen interno se regula de acuerdo con el estatuto aprobado por la correspondiente autoridad eclesiástica.

PROCESOS CONSTITUCIONALES


Artículo 82.  Contenido del estatuto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El estatuto de la asociación debe expresar:

1.- La denominación, duración y domicilio.

2.- Los fines.

3.- Los bienes que integran el patrimonio social.

4.- La constitución y funcionamiento de la asamblea general de asociados, consejo directivo y demás órganos de la asociación.

5.- Las condiciones para la admisión, renuncia y exclusión de sus miembros.

6.- Los derechos y deberes de los asociados.

7.- Los requisitos para su modificación.

8.- Las normas para la disolución y liquidación de la asociación y las relativas al destino final de sus bienes.

9.- Los demás pactos y condiciones que se establezcan.


Artículo 83.  Libros de la asociación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toda asociación debe tener un libro de registro actualizado en que consten el nombre, actividad, domicilio y fecha de admisión de cada uno de sus miembros, con indicación de los que ejerzan cargos de administración o representación.

La asociación debe contar, asimismo, con libros de actas de las sesiones de asamblea general y de consejo directivo en los que constarán los acuerdos adoptados.

Los libros a que se refiere el presente artículo se llevan con las formalidades de ley, bajo responsabilidad del presidente del consejo directivo de la asociación y de conformidad con los requisitos que fije el estatuto.


Artículo 84.  Asamblea General
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La asamblea general es el órgano supremo de la asociación.

PROCESOS CONSTITUCIONALES


Artículo 85.  Convocatoria
~~~~~~~~~~~~~~~~~~~~~~~~~~

La asamblea general es convocada por el presidente del consejo directivo de la asociación, en los casos previstos en el estatuto, cuando lo acuerde dicho consejo directivo o cuando lo soliciten no menos de la décima parte de los asociados.

Si la solicitud de éstos no es atendida dentro de los quince días de haber sido presentada, o es denegada, la convocatoria es hecha por el juez de primera instancia del domicilio de la asociación, a solicitud de los mismos asociados.

De la solicitud se corre traslado a la asociación por el plazo de tres días, y con la contestación o en rebeldía resuelve el juez en mérito del libro de registro. Procede el recurso de apelación en el efecto devolutivo. (*)

(*) Tercer párrafo sustituido por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"La solicitud se tramita como proceso sumarísimo."

El juez, si ampara la solicitud, ordena se haga la convocatoria de acuerdo al estatuto, señalando el lugar, día, hora de la reunión, su objeto, quien la presidirá y el notario que de fe de los acuerdos. (*) RECTIFICADO POR FE DE ERRATAS

CONCORDANCIAS:     R. N° 331-2001-SUNARP-SN  (Acreditación de convocatorias y cómputo de quórum)
R.N° 038-2013-SUNARP-SN (Aprueban el “Reglamento de Inscripciones del Registro de Personas Jurídicas”)


Artículo 86.  Facultades de la Asamblea General
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La asamblea general elige a las personas que integran el consejo directivo, aprueba las cuentas y balances, resuelve sobre la modificación del estatuto, la disolución de la asociación y los demás asuntos que no sean competencia de otros órganos.


Artículo 87.  Quórum para adopción de acuerdos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para la validez de las reuniones de asamblea general se requiere, en primera convocatoria, la concurrencia de más de la mitad de los asociados. En segunda convocatoria, basta la presencia de cualquier número de asociados. Los acuerdos se adoptan con el voto de más de la mitad de los miembros concurrentes.

Para modificar el estatuto o para disolver la asociación se requiere, en primera convocatoria, la asistencia de más de la mitad de los asociados. Los acuerdos se adoptan con el voto de más de la mitad de los miembros concurrentes. En segunda convocatoria, los acuerdos se adoptan con los asociados que asistan y que representen no menos de la décima parte.

Los asociados pueden ser representados en asamblea general, por otra persona. El estatuto puede disponer que el representante sea otro asociado.

La representación se otorga por escritura pública. También puede conferirse por otro medio escrito y sólo con carácter especial para cada asamblea.


Artículo 88.  Derecho de voto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ningún asociado tiene derecho por sí mismo a más de un voto.


Artículo 89.  Carácter personalísimo de la calidad del asociado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La calidad de asociado es inherente a la persona y no es trasmisible, salvo que lo permita el estatuto.


Artículo 90.  Renuncia de los asociados
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La renuncia de los asociados debe ser formulada por escrito.


Artículo 91.  Pago de cuotas adeudadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los asociados renunciantes, los excluidos y los sucesores de los asociados muertos quedan obligados al pago de las cuotas que hayan dejado de abonar, no pudiendo exigir el reembolso de sus aportaciones.


Artículo 92.  Impugnación judicial de acuerdos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Todo asociado tiene derecho a impugnar judicialmente los acuerdos que violen las disposiciones legales o estatutarias.

Las acciones impugnatorias deben ejercitarse en un plazo no mayor de sesenta días contados a partir de la fecha del acuerdo. Pueden ser interpuestas por los asistentes, si hubieran dejado constancia en acta de su oposición al acuerdo, por los asociados no concurrentes y por los que hayan sido privados ilegítimamente de emitir su voto.

Si el acuerdo es inscribible en el registro, la impugnación puede formularse dentro de los treinta días siguientes a la fecha en que la inscripción tuvo lugar.

Cualquier asociado puede intervenir en el juicio, a su costa para defender la validez del acuerdo.

La acción impugnatoria se interpone ante el juez de primera instancia del domicilio de la asociación y se sujeta al trámite del juicio de menor cuantía. (*)

(*) Último párrafo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"La impugnación se demanda ante el Juez Civil del domicilio de la asociación y se tramita como proceso abreviado."


Artículo 93.  Responsabilidad de los directivos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los asociados que desempeñen cargos directivos son responsables ante la asociación conforme a las reglas de la representación, excepto aquellos que no hayan participado del acto causante del daño o que dejen constancia de su oposición.


Artículo 94.  Disolución de pleno derecho
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La asociación se disuelve de pleno derecho cuando no pueda funcionar según su estatuto.


Artículo 95.  Disolución por liquidación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La Asociación se disuelve por la declaración de quiebra.

En caso de suspensión de pagos, el consejo directivo debe solicitar la declaración de quiebra de la asociación, conforme a la ley de la materia y bajo responsabilidad ante los acreedores por los daños y perjuicios que resultaren por la omisión. (*)

(*) Artículo modificado por la Quinta Disposición Final del Decreto Legislativo Nº 845, publicado el 21 septiembre 1996, cuyo texto es el siguiente:

"Artículo 95.- La Asociación se disuelve por liquidación, según lo acordado por su respectiva Junta de Acreedores de conformidad con la ley de la materia.

En caso de pérdidas superiores a dos terceras partes del patrimonio, el Consejo Directivo debe solicitar la declaración de insolvencia de la asociación, conforme a la ley de la materia y bajo responsabilidad ante los acreedores por los daños y perjuicios que resultaren por la omisión." (*)

(*) Artículo modificado por la Quinta Disposición Final del Decreto Supremo N° 014-99-ITINCI, publicado el 01 noviembre 1999, cuyo texto es el siguiente:

"Artículo 95.- La Asociación se disuelve por liquidación, según lo acordado por su respectiva Junta de Acreedores de conformidad con la ley de la materia.

En caso de pérdidas superiores a dos terceras partes del patrimonio, el Consejo Directivo debe solicitar la declaración de insolvencia de la asociación, conforme a la ley de la materia y bajo responsabilidad ante los acreedores por los daños y perjuicios que resultaren por la omisión." (*)

(*) Artículo modificado por la Primera Disposición Modificatoria de la Ley N° 27809, publicada el 08 agosto 2002, la misma que entró en vigencia a los sesenta (60) días siguientes de su publicación en el Diario Oficial El Peruano, cuyo texto es el siguiente:

Disolución por liquidación

“Artículo 95.- La Asociación se disuelve por liquidación, según lo acordado por su respectiva Junta de Acreedores de conformidad con la ley de la materia.

En caso de pérdidas acumuladas, deducidas las reservas superiores al tercio del capital social pagado, el Consejo Directivo debe solicitar el inicio del Procedimiento Concursal Ordinario de la asociación, conforme a la ley de la materia y bajo responsabilidad ante los acreedores por los daños y perjuicios que resultaren por la omisión."


Artículo 96.  Disolución por atentar contra orden público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Ministerio Público puede pedir la disolución de la asociación a la Sala Civil de la Corte Superior del distrito judicial respectivo, cuando sus fines o actividades sean contrarios al orden público o a las buenas costumbres. La Sala, oyendo a las partes, resuelve la disolución dentro del plazo de quince días.

A solicitud del Ministerio Público, la Sala puede ordenar la suspensión inmediata de las actividades de la asociación, mientras se resuelve acerca de su disolución.

La resolución, si no es apelable, se eleva en consulta a la Sala Civil de la Corte Suprema, la que oyendo a las partes, resuelve dentro de un plazo no mayor de quince días. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Disolución por atentar contra orden público

"Artículo 96.- El Ministerio Público puede solicitar judicialmente la disolución de la asociación cuyas actividades o fines sean o resulten contrarios al orden público o a las buenas costumbres.

La demanda se tramita como proceso abreviado, considerando como parte demandada a la asociación. Cualquier asociado está legitimado para intervenir en el proceso. La sentencia no apelada se eleva en consulta a la Corte Superior.

En cualquier estado del proceso puede el Juez dictar medidas cautelares suspendiendo total o parcialmente las actividades de la asociación, o designando un interventor de las mismas."


Artículo 97.  Disolución por falta de norma estatutaria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

De no haberse previsto en el estatuto de la asociación normas para el caso en que no pueda seguir funcionando o para su disolución, se procede de conformidad con lo dispuesto en el artículo 599, inciso 2.


Artículo 98.  Destino del patrimonio restante a la liquidación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Disuelta la asociación y concluída la liquidación, el haber neto resultante es entregado a las personas designadas en el estatuto, con exclusión de los asociados. De no ser posible, la Sala Civil de la Corte Superior respectiva ordena su aplicación a fines análogos en interés de la comunidad, dándose preferencia a la provincia donde tuvo su sede la asociación.

TITULO III Fundación
====================

CAPITULO UNICO
##############


Artículo 99.  Noción
~~~~~~~~~~~~~~~~~~~~

La fundación es una organización no lucrativa instituida mediante la afectación de uno o más bienes para la realización de objetivos de carácter religioso, asistencial, cultural u otros de interés social. (*) RECTIFICADO POR FE DE ERRATAS


Artículo 100.  Constitución de la Fundación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La fundación se constituye mediante escritura pública, por una o varias personas naturales o jurídicas, indistintamente, o por testamento.


Artículo 101.  Acto constitutivo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acto constitutivo de la fundación debe expresar necesariamente su finalidad y el bien o bienes que se afectan. El fundador puede también indicar el nombre y domicilio de la fundación, así como designar al administrador o a los administradores y señalar normas para su régimen económico, funcionamiento y extinción así como el destino final del patrimonio.

Puede nombrarse como administradores de la fundación a personas jurídicas o a quien o quienes desempeñen funciones específicas en ellas. En el primer caso, debe designarse a la persona natural que la represente.

El registrador de personas jurídicas debe enviar al Consejo de Supervigilancia de Fundaciones el título de constitución que careciere de alguno de los requisitos señalados en el primer párrafo del presente artículo. El Consejo procederá en un plazo no mayor de diez días, con arreglo al artículo 104, incisos 1 a 3, según el caso.


Artículo 102.  Revocación del fundador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La facultad de revocar no es transmisible. El acto de constitución de la fundación, una vez inscrito, es irrevocable.


Artículo 103.  Consejo de Supervigilancia de Fundaciones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Consejo de Supervigilancia de Fundaciones es la organización administrativa encargada del control y vigilancia de las fundaciones.

Su integración y estructura se determinan en la ley de la materia.


Artículo 104.  Funciones del Consejo de Supervigilancia de Fundaciones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Consejo de Supervigilancia de Fundaciones ejerce las siguientes funciones básicas:

1.- Indicar la denominación y domicilio de la fundación, cuando no consten del acto constitutivo.

2. Designar a los administradores cuando se hubiese omitido su nombramiento por el fundador o sustituirlos al cesar por cualquier causa en sus actividades, si no se hubiese previsto en el acto constitutivo la forma o modo de reemplazarlos. (*)

(*) Inciso modificado por el Artículo Único de la Ley Nº 26813, publicada el 20 junio 1997, cuyo texto es el siguiente:

"2. Designar a los administradores cuando se hubiese omitido su nombramiento por el fundador o sustituirlos al cesar por cualquier causa en sus actividades, siempre que no se hubiese previsto, para ambos casos, en el acto constitutivo la forma o modo de reemplazarlos.

En el caso previsto en el párrafo anterior, están impedidos de ser nombrados como administradores de las fundaciones, los beneficiarios o los representantes de las instituciones beneficiarias. Asimismo, en dicho supuesto, el cargo de administrador es indelegable."

3.- Determinar, de oficio y con audiencia de los administradores o a propuesta de éstos, el régimen económico y administrativo, si hubiere sido omitido por el fundador, o modificarlo cuando impidiese el normal funcionamiento o conviniere a los fines de la fundación.

4.- Tomar conocimiento de los planes y del correspondiente presupuesto anual de las fundaciones, para lo cual éstas elevan copia de los mismos al Consejo al menos treinta días antes de la fecha de iniciación del año económico.

5.- Autorizar los actos de disposición y gravamen de los bienes que no sean objeto de las operaciones ordinarias de la fundación y establecer el procedimiento a seguir, en cada caso.

6.- Promover la coordinación de las fundaciones de fines análogos cuando los bienes de éstas resulten insuficientes para el cumplimiento del fin fundacional, o cuando tal coordinación determinase una acción más eficiente.

7.- Vigilar que los bienes y rentas se empleen conforme a la finalidad propuesta.

8.- Disponer las auditorías necesarias.

9. Demandar ante el Poder Judicial la anulación de los acuerdos, actos o contratos que los administradores celebren con infracción de las leyes que interesen al orden público o a las buenas costumbres o que sean contrarios al acto constitutivo. (*)

(*) Inciso modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

"9.- Impugnar judicialmente los acuerdos de los administradores que sean contrarios a ley o al acto constitutivo o demandar la nulidad o anulación de los actos o contratos que celebren, en los casos previstos por la ley. La impugnación se tramita como proceso abreviado; la demanda de nulidad o de anulación como proceso de conocimiento."

10.- Intervenir como parte en los juicios en que se impugne la validez del acto constitutivo de la fundación.

11.- Designar al liquidador o a los liquidadores de la fundación a falta de disposición en el acto constitutivo.

12.- Llevar un registro administrativo de fundaciones.

CONCORDANCIAS:     Ley Nº 26918, Art. 5 (Funciones del INABIF)


Artículo 105.  Presentación de cuentas y balances
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los administradores están obligados a presentar al Consejo de Supervigilancia de Fundaciones, para su aprobación, las cuentas y el balance de la fundación, dentro de los cuatro primeros meses del año.

CONCORDANCIAS:     Ley Nº 26918, Art. 5 (Funciones del INABIF)


Artículo 106.  Acciones judicial contra los administradores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Consejo de Supervigilancia de Fundaciones puede iniciar acción judicial contra los administradores que no cumplan con presentar las cuentas y el balance anuales de la fundación o si éstos fueron desaprobados y en otros casos de incumplimiento de sus deberes.

A pedido de parte, el juez de primera instancia puede, por causa justificada, suspender a los administradores.

Declarada la responsabilidad, los administradores cesan automáticamente en el ejercicio de sus funciones, sin perjuicio de la acción penal a que hubiere lugar.

Los administradores suspendidos son reemplazados de acuerdo a lo dispuesto en el acto constitutivo o, en su defecto, por el Consejo de Supervigilancia de Fundaciones.

"La demanda de presentación de cuentas y balances y la de suspensión de los administradores en su cargo, se tramitan como proceso abreviado. La demanda de desaprobación de cuentas o balances y la de responsabilidad por incumplimiento de deberes, como proceso de conocimiento." (*)

(*) Último párrafo agregado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993.

CONCORDANCIAS:     Ley Nº 26918, Art. 5 (Funciones del INABIF)


Artículo 107.  Personas prohibidas para contratar con Fundaciones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El administrador o los administradores de la fundación, así como sus parientes hasta el cuarto grado de consanguinidad y segundo de afinidad, no pueden celebrar contratos con la fundación, salvo autorización expresa del Consejo de Supervigilancia de Fundaciones.

La prohibición se hace extensiva a las personas jurídicas de las cuales sean socios tanto el administrador o los administradores de la fundación, como sus parientes en los grados señalados en el párrafo anterior.

CONCORDANCIAS:     Ley Nº 26918, Art. 5 (Funciones del INABIF)

Artículo 108.- El Consejo de Supervigilancia de Fundaciones puede, respetando en lo posible la voluntad del fundador y con audiencia de los administradores, solicitar a la Sala Civil de la Corte Superior respectiva:

1. La ampliación de los fines de la fundación a otros análogos, cuando el patrimonio resulta notoriamente excesivo para la finalidad instituida por el fundador.

2. La modificación del fin fundacional, cuando por el transcurso del tiempo haya perdido el interés social a que se refiere el artículo 99. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:


Artículo 108.  Ampliación y modificación de los objetivos de la Fundación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Consejo de Supervigilancia de Fundaciones, respetando en lo posible la voluntad del fundador, puede solicitar al Juez Civil:

1.- La ampliación de los fines de la fundación a otros análogos, cuando el patrimonio resulta notoriamente excesivo para la finalidad instituída por el fundador.

2.- La modificación de los fines, cuando haya cesado el interés social a que se refiere el artículo 99.

La pretensión se tramita como proceso abreviado, con citación del Ministerio Público, considerando como emplazados a los administradores de la fundación".

CONCORDANCIAS:     Ley Nº 26918, Art. 5 (Funciones del INABIF)

Artículo 109.- El Consejo de Supervigilancia de Fundaciones solicita a la Sala Civil de la Corte Superior respectiva la disolución de la fundación cuyo fin se haya hecho imposible.

La Sala Civil aplica el haber neto resultante de la liquidación a la finalidad prevista en el acto constitutivo. Si ello no es posible, los bienes se destinan, a propuesta del Consejo, a incrementar el patrimonio de otra u otras fundaciones de fines análogos o, en su defecto, a su inversión en obras de similares propósitos, preferentemente en la localidad donde la fundación tuvo su sede.(*) (*) RECTIFICADO POR FE DE ERRATAS

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 93, cuyo texto es el siguiente:


Artículo 109.  Disolución de la Fundación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Consejo de Supervigilancia puede solicitar la disolución de la fundación cuya finalidad resulte de imposible cumplimiento.

La demanda se tramita como proceso abreviado ante el Juez Civil de la sede de la fundación, emplazando a los administradores. La demanda será publicada por tres veces en el diario encargado de los avisos judiciales y en otro de circulación nacional, mediando cinco días entre cada publicación.

La sentencia no apelada se eleva en consulta a la Corte Superior."

CONCORDANCIAS:     Ley Nº 26918, Art. 5 (Funciones del INABIF)

Artículo 110.- La Sala Civil de la Corte Superior respectiva resuelve las solicitudes a que se refieren los artículos 108 y 109 dentro del plazo de cuarenticinco días, oyendo a los administradores y a quienes tengan legítimo interés. La Sala dispone que el Consejo de Supervigilancia de Fundaciones publique un resumen de la solicitud en el diario encargado de los avisos judiciales y en uno de circulación nacional, por el término de tres días.

Entre cada aviso deben mediar cinco días naturales.

La resolución se eleva en consulta a la Sala Civil de la Corte Suprema, la que se pronuncia dentro del plazo de treinta días, oyendo a los administradores y a los que tuvieren legítimo interés. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:


Artículo 110.  Destino del patrimonio restante a la liquidación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El haber neto resultante de la liquidación de la fundación se aplica a la finalidad prevista en el acto constitutivo. Si ello no fuera posible, se destina, a propuesta del Consejo, a incrementar el patrimonio de otra u otras fundaciones de finalidad análoga o, en su defecto, a la Beneficencia Pública para obras de similares propósitos a los que tenía la fundación en la localidad donde tuvo su sede."

CONCORDANCIAS:     Ley Nº 26918, Art. 5 (Funciones del INABIF)

TITULO IV Comité
================

CAPITULO UNICO
##############

CONCORDANCIAS:     R. N° 331-2001-SUNARP-SN  (Acreditación de convocatorias y cómputo de quórum)
R.N° 038-2013-SUNARP-SN (Aprueban el “Reglamento de Inscripciones del Registro de Personas Jurídicas”)
Noción
Artículo 111.  
~~~~~~~~~~~~~~~

El comité es la organización de personas naturales o jurídicas, o de ambas, dedicada a la recaudación pública de aportes destinados a una finalidad altruísta.

El acto constitutivo y el estatuto del comité pueden constar, para su inscripción en el registro, en documento privado con legalización notarial de las firmas de los fundadores.


Artículo 112.  Registro de miembros
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comité debe tener un registro actualizado que contenga el nombre, domicilio, actividad y fecha de admisión de sus miembros, con indicación de los integrantes del consejo directivo o de las personas que ejerzan cualquier otra actividad administrativa.

El registro debe constar de un libro llevado con las formalidades de ley, bajo la responsabilidad de quien preside el consejo directivo.


Artículo 113.  Estatuto del Comité
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El estatuto del comité debe expresar:

1.- La denominación, duración y domicilio.

2.- La finalidad altruísta propuesta.

3.- El régimen administrativo.

4.- La constitución y funcionamiento de la asamblea general y del consejo directivo, así como de cualquier otro órgano administrativo.

5.- La designación del funcionario que ha de tener la representación legal del comité.

6.- Los demás pactos y condiciones que se establezcan.


Artículo 114.  Convocatoria del Consejo Directivo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El consejo directivo es el órgano de gestión del comité y es convocado por quien lo presida en los casos previstos en el estatuto o cuando lo solicite cualquiera de los miembros integrantes del consejo o la décima parte de los miembros del comité. Si su solicitud fuese denegada o transcurren siete días de presentada sin efectuarse la convocatoria, se procede de conformidad con lo establecido en el artículo 85.


Artículo 115.  Atribuciones de la Asamblea General
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La asamblea general elige a las personas que integran el consejo directivo. Puede modificar el estatuto, acordar la disolución del comité y adoptar cualquier otra decisión que no sea de competencia de otros órganos.


Artículo 116.  Quórum para reuniones y acuerdos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para la validez de las reuniones de la asamblea, para el cómputo del quórum y para las votaciones, se aplica lo dispuesto en los artículos 87, párrafo primero, y 88.


Artículo 117.  Denuncia de actos y acuerdos ilegales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cualquier miembro del comité o del consejo directivo tiene el derecho y el deber de denunciar ante el Ministerio Público los acuerdos o los actos que violen las disposiciones legales o estatutarias.


Artículo 118.  Responsabilidad del Consejo Directivo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los miembros del consejo directivo son responsables solidariamente de la conservación y debida aplicación de los aportes recaudados a la finalidad anunciada.


Artículo 119.  Control de los aportes por el Ministerio Público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Ministerio Público vigila, de oficio o a instancia de parte, que los aportes recaudados por el comité se conserven y se destinen a la finalidad propuesta y, llegado el caso, puede solicitar la rendición de cuentas, sin perjuicio de la acción civil o penal a que haya lugar.

Artículo 120.- El Ministerio Público puede pedir la disolución del comité al juez de primera instancia del lugar en que aquél tenga su domicilio, cuando sus actividades resulten contrarias al orden público o a las buenas costumbres. El juez resuelve en un plazo de quince días.

El juez puede ordenar, a solicitud del Ministerio Público, de oficio o a instancia de parte, la suspensión inmediata de las actividades del comité mientras se resuelve acerca de su disolución.

Dicha resolución se eleva en consulta a la Sala Civil de la Corte Superior respectiva, la que, oyendo a los interesados, resuelve en un plazo no mayor de quince días. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:


Artículo 120.  Disolución por atentar contra el orden público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es de aplicación al Comité lo dispuesto en el artículo 96."

Artículo 121.- Cumplida la finalidad propuesta, o si ella no se ha podido alcanzar, el consejo directivo procede, con conocimiento del Ministerio Público, a la disolución del comité y a la rendición judicial de cuentas. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:


Artículo 121.  Disolución y liquidación del Comité
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cumplida la finalidad propuesta, o si ella no se ha podido alcanzar, el consejo directivo procede a la disolución y liquidación del comité, presentando al Ministerio Público copia de los estados finales de cuentas."

Artículo 122.- Disuelto el comité, el haber neto resultante, una vez pagadas las obligaciones, se restituye a los erogantes, si ello fuese posible. En caso contrario, el juez de primera instancia de la sede del comité, a propuesta del consejo directivo y con conocimiento del Ministerio Público, lo aplica a fines análogos en interés de la comunidad, dentro de un plazo no mayor de treinta días.

La resolución se eleva en consulta a la Sala Civil de la respectiva Corte Superior, la que debe pronunciarse en un plazo no mayor de quince días. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:


Artículo 122.  Aplicación del haber neto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El consejo directivo adjudica a los erogantes el haber neto resultante de la liquidación, si las cuentas no hubieran sido objetadas por el Ministerio Público dentro de los treinta días de haberle sido presentadas. La desaprobación de las cuentas se tramita como proceso de conocimiento, estando legitimados para intervenir cualquiera de los miembros del comité.

Si la adjudicación a los erogantes no fuera posible, el consejo entregará el haber neto a la entidad de Beneficencia Pública del lugar, con conocimiento del Ministerio Público."


Artículo 123.  Aplicación supletoria de normas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comité se rige, además, por los artículos 81 a  98, en cuanto le fueren aplicables.
