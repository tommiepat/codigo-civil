===========================================================
SECCION TERCERA Asociación, Fundación y Comité No Inscritos
===========================================================

TITULO I Asociación
===================

Capítulo único
##############

Artículo 124 Régimen de la asociación de hecho
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El ordenamiento interno y la administración de la asociación que no se haya constituido mediante escritura pública inscrita, se regula por los acuerdos de sus miembros, aplicándose las reglas establecidas en los artículos 80 a 98, en lo que sean pertinentes.

Dicha asociación puede comparecer en juicio representada por el presidente del consejo directivo o por quien haga sus veces.

Artículo 125 Fondo común de la asociación de hecho
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los aportes y las cuotas de los asociados, así como los bienes que adquiera la asociación, constituyen su fondo común. Mientras está vigente la asociación no se puede pedir la división y partición de dicho fondo, ni el reembolso de las aportaciones de los asociados.

Artículo 126 Responsabilidad por obligaciones de los representantes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El fondo común responde de las obligaciones contraídas por los representantes de la asociación. De dichas obligaciones responden solidariamente quienes actúen en nombre de la asociación, aun cuando no sean sus representantes.

TITULO II Fundación
===================

Capítulo único
##############

Artículo 127 Inscripción de la fundación de hecho
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si por cualquier causa el acto constitutivo de la fundación no llega a inscribirse, corresponde al Consejo de Supervigilancia de Fundaciones, al Ministerio Público o a quien tenga legítimo interés, realizar las acciones para lograr dicha inscripción.

Artículo 128 Responsabilidad solidaria de los administradores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los administradores de la fundación, en tanto no esté inscrita, son solidariamente responsables de la conservación de los bienes afectados a la finalidad propuesta y de las obligaciones que hubieren contraído.

Artículo 129 Afectación del patrimonio a otra fundación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

De no ser posible la inscripción a que se refiere el artículo 127, la Sala Civil de la Corte Superior de la sede de la fundación, a solicitud del Consejo de Supervigilancia de Fundaciones, del Ministerio Público o de quien tenga legítimo interés, afectará los bienes a otras fundaciones de fines análogos o, si ello no es posible, a otra fundación preferentemente establecida en el mismo distrito judicial.

TITULO III Comité
=================

Capítulo único
##############

Artículo 130 Comité de hecho
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El comité que no se haya constituído mediante instrumento inscrito se rige por los acuerdos de sus miembros, aplicándose las reglas establecidas en los artículos 111 a 123, en lo que sean pertinentes.

El comité puede comparecer en juicio representado por el presidente del consejo directivo o por quien haga sus veces.

Artículo 131 Responsabilidad solidaria de los organizadores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quienes aparezcan como organizadores del comité y quienes asuman la gestión de los aportes recaudados, son responsables solidariamente de su conservación, de su aplicación a la finalidad anunciada y de las obligaciones contraídas.

Artículo 132 Disolución y rendición de cuentas a pedido del Ministerio Público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cumplida la finalidad propuesta, o si ella no se hubiera podido alcanzar, el Ministerio Público solicita de oficio o a instancia de parte, la disolución del comité y la rendición judicial de cuentas, proponiendo la afectación del haber neto resultante a fines análogos.

Artículo 133 Supervisión de lo recaudado por el Ministerio Público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Ministerio Público vigila, de oficio o a instancia de parte, que los aportes recaudados se conserven debidamente y se apliquen a la finalidad anunciada.
